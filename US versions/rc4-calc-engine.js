//v.9/214/2020

/* Outputs Numbers like 1400000 as 1.4M */
function nFormatter(num) {
    if (num >= 1000000000) {
        return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
    }
    if (num >= 1000000) {
        return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
    }
    if (num >= 1000) {
        return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
    }
    return num;
}

/* Outputs Numbers like 1400000 as 1,400,000 */
function toCommas(value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

//var injectCalc = document.getElementById('calcOutput');

//injectCalc.innerHTML = '<div class="container"> <div class="row wrapper"> <div class="col-lg-2 col-lg-offset-1 text-right inputs-wrapper"></div><div class="col-lg-12 data-wrapper"> <div class="row"> <div class="col-lg-12"> <canvas id="myChart"></canvas> </div><div class="col-lg-12 rates"> <div class="row"> <div class="col-lg-12 text-center"> <h3>Based on the details you provided:</h3> </div><div class="col-sm-10 col-sm-offset-1 rate"> <div class="row align-items-center"> <div class="col-lg-3 text-center rate-percent"> <span>At</span> <span>2%</span> <span>Rate of return</span> </div><div class="col-lg-8 text-breakdown"> <p>Your portfolio is estimated to last until you are: <span class="finalAge">X</span> years old or <span class="investmentDuration">X</span> years into retirement, <span class="estimatedText">with an estimated portfolio value at the time of retirement of</span> <span class="finalValue">$X,XXX,XXX</span></p></div></div></div><div class="col-sm-10 col-sm-offset-1 rate"> <div class="row align-items-center"> <div class="col-lg-3 text-center rate-percent"> <span>At</span> <span>5%</span> <span>Rate of return</span> </div><div class="col-lg-8 text-breakdown"> <p>Your portfolio is estimated to last until you are: <span class="finalAge">X</span> years old or <span class="investmentDuration">X</span> years into retirement, <span class="estimatedText">with an estimated portfolio value at the time of retirement of</span> <span class="finalValue">$X,XXX,XXX</span></p></div></div></div><div class="col-sm-10 col-sm-offset-1 rate"> <div class="row align-items-center"> <div class="col-lg-3 text-center rate-percent"> <span>At</span> <span>8%</span> <span>Rate of return</span> </div><div class="col-lg-8 text-breakdown"> <p>Your portfolio is estimated to last until you are: <span class="finalAge">X</span> years old or <span class="investmentDuration">X</span> years into retirement, <span class="estimatedText">with an estimated portfolio value at the time of retirement of</span> <span class="finalValue">$X,XXX,XXX</span></p></div></div></div></div></div></div></div></div></div>';



//Start Of Calculations

// Grab Cookie
// function getCookie(cname) {
//     var name = cname + "=";
//     var ca = document.cookie.split(';');
//     for(var i = 0; i < ca.length; i++) {
//       var c = ca[i];
//       while (c.charAt(0) == ' ') {
//         c = c.substring(1);
//       }
//       if (c.indexOf(name) == 0) {
//         return c.substring(name.length, c.length);
//       }
//     }
//     return "";
// }

// function setCookie(name,value,minutes) {
//     var expires = "";
//     if (minutes) {
//         var date = new Date();
//         var minutes = 30;
//         date.setTime(date.getTime() + (minutes * 60 * 1000));
//         expires = "; expires=" + date.toUTCString();
//     }
//     document.cookie = name + "=" + (value || "")  + expires + "; path=/";
// }

// //Set Cookie To Variable
// var cookie = getCookie('calculator_query_string');

// // Demo If Statement to split into array
// if(cookie) {
//     var cookieBase = cookie.split(/[ \( \&\=\,ica,r,f,p,s| \)]+/);
//     var cookieFinal = cookieBase.slice(1);
//     console.log(cookieFinal)
// } else {
//     console.log('no cookie found');
// }

//Variables need to be set to cookie values. Zaw
var age = 85;
var retirementAge = 75;
var initialBalance = 1000000;
var contributionValue = 25000;
var annualWithdrawal = 80000;
var wantedTotalAmountAtLastAge =250000.00;
var estimatedTaxRate;

var investmentRoR = .07;
var inflation = 0.016;
var lifeExpectancy = 95;

//...................//

console.log('My initial cookie age is '+age+' I plan to cookie retire at '+retirementAge+' My initial cookie invest will be '+initialBalance+' My annual cookie contribution will be '+contributionValue);

// //Global Elements/Values
// var myChart = document.getElementById('myChart').getContext('2d');
// var retirementHave = document.getElementsByClassName('retirement-have');
// var retirementNeed = document.getElementsByClassName('retirement-need');
// var updateChart = document.getElementById('update-chart');

//Investment Related Variables

// Value Of Adds Calculation Values
var baseAgeDifference = retirementAge - age;


if(baseAgeDifference <= 0) {
    baseAgeDifference = 0;
}

var baseAgeValue = (1 + age) - age;



function calculateSavings(rateOfReturn) {
    ageData = [];
    savingsData = [];
    valueAddsData = [];
    var paceData = [initialBalance];
    var yearsInvested = 0;
    var yearCounter = 1;
    //Daryl Change here V2
    var contributions = [contributionValue];
    var valueOfAdds_preRetire = []
    var finalInvestment
    var nominalIncomeTaken = [annualWithdrawal];
    var contributionsValueAtRetirement = 0;
    var maxInvestmentVal = 0;
    var riskContent = "Very Low"
    var ageBeforeUnderWantedAmount = 0;
    var neededMaxAmountAtRetirement  = 0;



    function calculateTaxRate(){
        //TODO:Check if Usewr has given tax rate
        if(!estimatedTaxRate){
            var totalEffectiveTax = 0;
            var currTaxVal;
            var totalEffectiveTaxRate;
            var brackets = [
                [9699,0,.1],
                [39474,9700,.12],
                [84199,39475,.22],
                [160724,84200,.24],
                [204099,160725,.32],
                [510299,204100,.35],
                [1000000,510300,.37]
            ]

            try{
                for(var p = 0; p < brackets.length;p++){
                    if(annualWithdrawal >= brackets[p][0]){
                        currTaxVal = (brackets[p][0] - brackets[p][1]) * brackets[p][2];
                        totalEffectiveTax += currTaxVal;
                    } else if(annualWithdrawal > brackets[p][1] && annualWithdrawal < brackets[p][0]){
                        currTaxVal = (annualWithdrawal - brackets[p][1]) * brackets[p][2]
                        totalEffectiveTax += currTaxVal
                    }
                }
            } catch(e){
                console.log("Calculate Tax Rate function", e)
            }
            totalEffectiveTaxRate = (totalEffectiveTax/annualWithdrawal)
            estimatedTaxRate = totalEffectiveTaxRate
        }
    }
    //calculate tax rate here
    calculateTaxRate()

    for(i = age; i <= lifeExpectancy; i++) {
        ageData.push(i);
        //Calculate Contribution
        var ageDifference = (i+1) - age;
        var inflationTotal = Math.pow(1+(investmentRoR - inflation),baseAgeDifference-(baseAgeDifference- yearCounter))
        var savingsIncrease = (contributions[0] * inflationTotal);
        savingsData.push(savingsIncrease);
        //Calculate Value Of Adds
        if(yearCounter <= baseAgeDifference) {
            //custom by daryl
            var currentContribution = contributions[0] * Math.pow((1+inflation),yearCounter)
            var current_preRetirementValueAdd;

            if(i === age){
                //For value add
                current_preRetirementValueAdd  = contributions[0] * inflationTotal
            } else {
                //For value add
                current_preRetirementValueAdd = contributions[contributions.length - 1] * inflationTotal
            }
            contributions.push(currentContribution)
            valueOfAdds_preRetire.push(current_preRetirementValueAdd)


            yearCounter++;
        }
        yearsInvested++;

        //for nominal income needed
        if(i <= lifeExpectancy - baseAgeDifference){
            currentNominalIncomeValue = nominalIncomeTaken[nominalIncomeTaken.length -1] * (1 + inflation)
            nominalIncomeTaken.push(currentNominalIncomeValue)
        }
    }
    nominalIncomeTaken = nominalIncomeTaken.map(function(val, index){
        return val = Math.round(val)
    })
    //remove unneeded last contribution
    contributions.pop()
    nominalIncomeTaken.pop()

    contributions.forEach(function(val, index){
        contributionsValueAtRetirement += val
    })
    recontributionValue = contributionValue

    // Calculate Pace


    function calculateActualRetirement() {
        var total = valueAddsData[0];

        var yearCounter = 1;
        var paceCalcArray = [];
        var yearsAfterRetirement = lifeExpectancy - retirementAge;
        var yearsInvestedPace = 0;
        var valueAddsTotal = 0;
        var currPacePreRet;


        //preretire
        for(var i = 0; i < baseAgeDifference; i++) {
            valueAddsTotal += valueOfAdds_preRetire[i]
            currPacePreRet = valueAddsTotal+initialBalance * Math.pow((1+investmentRoR - inflation), (i + 1));
            paceData.push(currPacePreRet)
        }
        maxInvestmentVal = paceData[paceData.length -1]

        // Caluclate Final Pace Values and Start Annual Withdrawal
        // (final investment - (withdrawl/1-estimated tax rate) *

        finalInvestment = paceData.slice(-1)[0];
        paceCalcArray.push(finalInvestment)
        //console.log("this is final investment:",paceCalcArray)
        for(var l = 1; l <= yearsAfterRetirement; l++) {
            //...............Used for final input in graph data.............//
            finalInvestment = (finalInvestment - ((annualWithdrawal/(1- estimatedTaxRate)) * Math.pow((1+inflation),((retirementAge + l) - Math.min(age, retirementAge))))) * (1 + investmentRoR -inflation)
            finalInvestmentFormatted = finalInvestment;

            paceCalcArray.push(finalInvestmentFormatted);
            paceData.push(finalInvestmentFormatted);

        }
        console.log(baseAgeDifference, yearsAfterRetirement)

        return paceData;
    }

    //Daryl change here

    //Calculate minimum needed retirement account
    function calculateNeedsPreAndPostRetire(){

      //calculating post retire account value
      var postValueAccount = [wantedTotalAmountAtLastAge];
      var retirementAccountNeeds = [initialBalance];
      var annualizedRate;
      var preretireVal;
      retirementAge = retirementAge >= age ? retirementAge : age;
      for(var l = lifeExpectancy; l > retirementAge;l--){
          var currVal = (((annualWithdrawal /(1 - estimatedTaxRate)) * Math.pow((1 + inflation), (l - Math.min(age, retirementAge)))) + (postValueAccount[postValueAccount.length -1]/(1+ investmentRoR - inflation)))
          postValueAccount.push(currVal)
      }

      postValueAccount = postValueAccount.map(function(val, index){
        return Math.round(val)
      }).reverse()

      neededMaxAmountAtRetirement = postValueAccount[0]
      // calculating annualized tax rate at 95
      annualizedRate = (Math.pow((postValueAccount[0]/initialBalance),(1/(retirementAge - age)))) -1;

      // calculating needs post retirement;
console.log("Post val account", postValueAccount)
      for(var o = age + 1; o < retirementAge;o++){
        preretireVal = retirementAccountNeeds[retirementAccountNeeds.length -1] * (1+annualizedRate)
        retirementAccountNeeds.push(preretireVal)
      }
      retirementAccountNeeds = retirementAccountNeeds.map(function(val, index){
        return Math.round(val)
      })
      retirementAccountNeeds = retirementAccountNeeds.concat(postValueAccount)

      return retirementAccountNeeds

    }


    var maxAgeRiskVal = ((ageData[ageData.length-1] - 95))
    console.log("maxAge",maxAgeRiskVal)
    if(maxAgeRiskVal >= 20){
        riskContent = "Very Low"
    } else if(maxAgeRiskVal < 20 && maxAgeRiskVal >= 5){
        riskContent = "Low"
    } else if(maxAgeRiskVal  >=0 && maxAgeRiskVal < 5){
        riskContent = "Medium"
    } else if(maxAgeRiskVal>= -5 && maxAgeRiskVal < 0){
        riskContent = "High"
    } else if (maxAgeRiskVal < -5){
        riskContent = "Very High"
    } else {
        riskContent = "Very, Very Low"
    }


    //final calculations to format data for graphs object
    var actualPace = calculateActualRetirement();
    var minimumNeededRetirementAccount = calculateNeedsPreAndPostRetire()
    var growth = maxInvestmentVal - initialBalance - contributionsValueAtRetirement
    minimumNeededRetirementAccount.shift();

    return {
        graph1:{
            "actualPace":actualPace,
            "minNeededAccount": minimumNeededRetirementAccount
        },
        graph2:{
            "initialBalance":[initialBalance, initialBalance/maxInvestmentVal],
             "contributions":[contributionsValueAtRetirement, contributionsValueAtRetirement/maxInvestmentVal],
             "growth":[growth, growth/maxInvestmentVal]
         },
        graph3:{
            "nominalIncomeTaken": nominalIncomeTaken,
            "realIncomeNeededEachYear":annualWithdrawal,
            "shortfall":nominalIncomeTaken[nominalIncomeTaken.length -1] - annualWithdrawal
        },
        "riskContent": riskContent,
        "AgeData": ageData,
        "retirementAge":retirementAge,
        "ActualInvestmentAtRetirement": maxInvestmentVal,
        "neededInvestmentAtRetirement": neededMaxAmountAtRetirement
    }

}

function calcPortfolioValAtRet(yearsIntoRetirement){
 if(yearsIntoRetirement < 0){
    yearsIntoRetirement = 0
 }
   return Math.pow((initialBalance + (1+ (investmentRoR - inflation))),yearsIntoRetirement)
}



//Main calculation function gets declared here @zaw
var paceMedium = calculateSavings();
console.log("data needed", paceMedium)


//Format Array Data To $ and , Versions For Chart
function formatArray(arrayName) {
    formattedArray = [];
    for(l = 0; l < arrayName.length; l++) {
        formattedArray.push('$'+toCommas(arrayName[l]));
    }

    return formattedArray;
}


// var paceMediumFormatted = formatArray(paceMedium);


// Run Array And Find Value 'f' to calculate years into retirement portfolio hits 0 or below
function calculateYearsIntoRetirement(paceArray) {

    paceArray = paceArray.graph1.actualPace
    console.log(paceArray.length)
    for(f=0; f <= paceArray.length; f++) {
        if(paceArray[f] < wantedTotalAmountAtLastAge) {
            var yearsIntoRetirement = (f - 1) - (retirementAge - age) ;
            return yearsIntoRetirement;
        } else {
            // Do nothing
        }
    }
}

// Run array to find value to calculate age at which portfolio hits 0
function calculateMaxAge(paceArray) {

    paceArray = paceArray.graph1.actualPace
    for(f=0; f <= paceArray.length; f++) {
        if(paceArray[f] < wantedTotalAmountAtLastAge) {
            //console.log('you are broke');
            var ageAtZero = retirementAge + (f -1) - (retirementAge - age);
            //console.log("retirement age",ageAtZero)
            return ageAtZero;
        } else {
            // Do nothing
        }
    }
}

//var finalAge = document.getElementsByClassName('finalAge');
//var investmentDuration = document.getElementsByClassName('investmentDuration');



function paceDurationMedium() {
    var yearsIntoRetirement = calculateYearsIntoRetirement(paceMedium);
    console.log("years into retirement:", yearsIntoRetirement)
    if(isNaN(retirementAge + yearsIntoRetirement)) {
        // finalAge[1].innerHTML = '>= 100';
        // investmentDuration[1].innerHTML = '>= 10';
        console.log("html will be set to OVER 100 years. Meaning value of portfolio doesn't reach 0 in array timeframe", `${calculateMaxAge(paceMedium)} or ${yearsIntoRetirement} years into retirement.`)
    } else {
        console.log(`The medium portfolio value will last until the person is ${calculateMaxAge(paceMedium)} or ${yearsIntoRetirement} years into retirement.`)
    }
}




paceDurationMedium();


//Grab Portfolio Value Based On Retirement Duration
var portfolioTarget = baseAgeDifference;
// var portfolioValueLow = '$' + toCommas(paceLow[portfolioTarget].toFixed(0)); // 2%
// var portfolioValueMedium = '$' + toCommas(paceMedium[portfolioTarget].toFixed(0)); //5%
//var portfolioValueHigh = '$' + toCommas(paceHigh[portfolioTarget].toFixed(0)); //8%

// var finalValue = document.getElementsByClassName('finalValue');
// var estimatedText = document.getElementsByClassName('estimatedText');

// function createDetails() {
//     if(age >= retirementAge) {
//         for(v = 0; v < estimatedText.length; v++) {
//             estimatedText[v].innerHTML = "based on the current portfolio value of ";
//         }

//         for(v = 0; v < finalValue.length; v++) {
//             finalValue[v].innerHTML = '$' + toCommas(initialBalance);
//         }
//     } else {
//         for(v = 0; v < finalValue.length; v++) {
//             if(v == 0) {
//                 finalValue[v].innerHTML = portfolioValueLow;
//                 console.log(portfolioValueLow);
//             } else if (v == 1) {
//                 finalValue[v].innerHTML = portfolioValueMedium;
//                 console.log(portfolioValueMedium);
//             } else {
//                 finalValue[v].innerHTML = portfolioValueHigh;
//                 console.log(portfolioValueHigh);
//             }

//             estimatedText[v].innerHTML = "with an estimated portfolio value at the time of retirement of ";
//         }
//     }
// }



// createDetails();

// function checkZero() {
//     if(initialBalance <= 0 && contributionValue <= 0 && annualWithdrawal <= 0) {
//         paceData = [];
//         savingsData = [];
//         valueAddsData = [];

//         for(v = 0; v < finalValue.length; v++) {
//             if(v == 0) {
//                 finalValue[v].innerHTML = '0';
//                 finalAge[v].innerHTML = '0';
//                 investmentDuration[v].innerHTML = '0';
//             } else if (v == 1) {
//                 finalValue[v].innerHTML = '0';
//                 finalAge[v].innerHTML = '0';
//                 investmentDuration[v].innerHTML = '0';
//             } else {
//                 finalValue[v].innerHTML = '0';
//                 finalAge[v].innerHTML = '0';
//                 investmentDuration[v].innerHTML = '0';
//             }
//         }
//     }
// }

// checkZero();

// Globl Options
// Chart.defaults.global.defaultFontFamily = 'Roboto';
// Chart.defaults.global.defaultFontSize = 18;
// Chart.defaults.global.defaultFontColor = '#4c4c4c';

// var demoChart = new Chart(myChart, {

//     type: 'line', // bar, horizontal bar, pie, line, doughnut, radar, polarArea
//     data: {
//         labels: ageData,
//         datasets:[{
//             data: paceLow,
//             label: '2%',
//             backgroundColor: '#B6CCC2',
//             pointHoverBackgroundColor: '#B6CCC2',
//             borderWidth: 0,
//             borderColor: 'rgba(0,0,0,0)',
//             hoverBorderWidth: 3,
//             hoverBorderColor: 'black',
//             pointBackgroundColor: 'rgba(0,0,0,0)',
//             pointRadius: 2
//         },{
//             data: paceMedium,
//             label: '5%',
//             backgroundColor: '#769D91',
//             pointHoverBackgroundColor: '#769D91',
//             borderWidth: 0,
//             borderColor: 'rgba(0,0,0,0)',
//             hoverBorderWidth: 3,
//             hoverBorderColor: 'black',
//             pointBackgroundColor: 'rgba(0,0,0,0)',
//             pointRadius: 2
//         },{
//             data: paceHigh,
//             label: '8%',
//             backgroundColor: '#4D7F71',
//             pointHoverBackgroundColor: '#4D7F71',
//             borderWidth: 0,
//             borderColor: 'rgba(0,0,0,0)',
//             hoverBorderWidth: 3,
//             hoverBorderColor: 'black',
//             pointBackgroundColor: 'rgba(0,0,0,0)',
//             pointRadius: 2
//         }]
//     },
//     options: {
//         responsive: true,
//         maintainAspectRatio: false,
//         hover: {
//             intersect: false
//         },
//         title: {
//             display: true,
//             text: ['ESTIMATED PORTFOLIO VALUE', 'Based on annual returns of:'],
//             fontSize: 20,
//             padding: 30
//         },
//         legend: {
//             display: true,
//             position: 'top',
//             labels: {
//                 fontColor: 'black'
//             }
//         },
//         layout: {
//             padding: {
//                 left: 0,
//                 right: 0,
//                 bottom: 0,
//                 top: 0
//             }
//         },
//         scales: {
//             yAxes: [{
//                 ticks: {
//                     callback: function(label, index, labels) {
//                         return '$' + nFormatter(label);
//                     },
//                     beginAtZero: true,
//                     min: 0
//                 },
//             }],
//             xAxes: [{
//             scaleLabel: {
//                 display: true,
//                 labelString: 'Age',
//             },
//             ticks: {
//                 autoSkip: true,
//                 maxTicksLimit: 6
//             }
//             }]
//         },
//         plugins: {
//             datalabels: {
//                 // hide datalabels for all datasets
//                 display: false
//             }
//         },
//         tooltips: {
//             mode: 'point',
//             displayColors: true,
//             titleFontSize: 0,
//             callbacks: {
//                 label: function(tooltipItem, data) {
//                     var beforeLabel = data.labels[tooltipItem.index];
//                     var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
//                     return 'Age: ' + beforeLabel + ' Portfolio Value: $' +  toCommas(value);
//                 }
//             }
//         }
//     }
// });

//Append Custom Button To Form Test For Fisher
// var formBlock = document.getElementsByClassName('form-builder')[0];
// formBlock.innerHTML += '<button type="submit" id="update-chart">Insivia Appended Recalculate</button>';
// var updateChart = document.getElementById('update-chart');


//for re updating chart
// updateChart.addEventListener('click', function(e) {
//     e.preventDefault();

//     checkZero();

//     // $('html').animate({
//     //     scrollTop: $("#myChart").offset().top
//     // }, 2000);

//     //Grab Initial Balance and values
//     // var valueAge = document.querySelector('input[data-calculator-key="ica"]');
//     // var valueRetire = document.querySelector('input[data-calculator-key="ira"]');
//     // var valueContribute = document.querySelector('input[data-calculator-key="ias"]');
//     // var valueInitialValue = document.querySelector('input[data-calculator-key="icp"]');
//     // var valueWithdrawal = document.querySelector('input[data-calculator-key="ircf"]');


//     function grabValues() {
//     var _age = parseInt(valueAge.value);
//     var _retirementAge = parseInt(valueRetire.value);
//     var _contributionValue = parseInt(valueContribute.value);
//     var _initialBalance = parseInt(valueInitialValue.value);
//     var valuesArray = [_age, _retirementAge, _contributionValue, _initialBalance];

//     return valuesArray;
//     }

//     var formValues = grabValues();

//     [valueAge, valueRetire, valueContribute, valueInitialValue].forEach(function(value) {
//         value.addEventListener("input", function() {
//             Need to add recalulate tax function  @zaw
//             grabValues();
//             formValues = grabValues();
//             console.log(formValues);
//             age = formValues[0];
//             retirementAge = formValues[1];
//             contributionValue = formValues[2];
//             initialBalance = formValues[3];
//         });
//     });

//     /* Overwirte Cookie Values */
//     age = parseInt(valueAge.value);
//     retirementAge = parseInt(valueRetire.value);
//     initialBalance = parseInt(valueInitialValue.value);
//     contributionValue = parseInt(valueContribute.value);
//     annualWithdrawal = parseInt(valueWithdrawal.value);
//     baseAgeDifference = retirementAge - age;

//     if(baseAgeDifference <= 0) {
//         baseAgeDifference = 0;
//     }

//     paceLow = calculateSavings(0.02);
//     paceMedium = calculateSavings(0.05);
//     paceHigh = calculateSavings(0.08);

//     portfolioTarget = baseAgeDifference; // - 1 since arrays start at 0;
//     portfolioValueLow = '$' + toCommas(paceLow[portfolioTarget].toFixed(0)); // 2%
//     portfolioValueMedium = '$' + toCommas(paceMedium[portfolioTarget].toFixed(0)); //5%
//     portfolioValueHigh = '$' + toCommas(paceHigh[portfolioTarget].toFixed(0)); //8%

//     paceDurationLow();
//     paceDurationMedium();
//     paceDurationHigh();

//     createDetails();

//     setCookie('calculator_query_string','ica='+age+'&ira='+retirementAge+'&icp='+initialBalance+'&ias='+contributionValue+'&ircf='+annualWithdrawal,30);

//     //demoChart.data.datasets[0].data = paceLow;
//     // demoChart.data.datasets[1].data = paceMedium;
//     // demoChart.data.datasets[2].data = paceHigh;
//     // demoChart.data.labels = ageData;
//     // demoChart.update();
//     console.log("Everything has been recalculated");
// });
