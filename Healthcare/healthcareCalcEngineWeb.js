/**
 * 10/4/2021: TB remix of DL original. 
 * Contains test inputs at bottom of file.
 * See minified version for prod ready file.
 */
//Stores functions in global window
window.fiHcCalc = {};
//Stores the results output in global window
window.fiHcCalcData = {};
/**
 * Core engine as a global function.
 * This version has a lot of repetative code, but does the job for now.
 * Contains 3 primary functions: calculatePrimary, calculateSpouse, and calculateCombined.
 * It runs all three so there's always an output. When there is no spouse - the spouse inputs are set to 0.
 * @param {object} primaryInputs the primary user's inputs drive the entire engine which assumes no spouse. Look at primaryResults when there is no spouse.
 * @param {boolean} hasSpouse if this is true then we grab the spouse inputs using getSpouseInputs and should look at the combinedResults
 */
window.fiHcCalc.hcEngine = function calculateEverything(inputs) {
    var primaryInputs = inputs.primary;
    var spouseInputs = inputs.spouse;
    //The return object for primary, spouse, and combined calculations
    var returnObj = {
        "coveredByHsa": [],
        "oop": [],
        "years": [],
        "hcPortfolioRatio": [],
        "baseYearHcCosts": [],
        "inflationAdjustedHcCosts": [],
        "baseYearCostBreakdown": [],
        "inflationAdjustedCostBreakdown": [],
        "hsaValueEoy": [],
        "portfolioValue": [],
        "outOfPocketYears": [],
        "outOfPocketExHsa": [],
        "lifetimeHcCosts": 0,
        "lifetimeHsa": 0,
        "lifetimeOop": 0,
        "lifetimePercentCovered": 0,
        "hasHsaFlag": false
    };
    /**
     * CONSTANTS THAT WON'T CHANGE THROUGHOUT THE CALCULATIONS
     */
    //The source dataset for cost breakdowns
    const SOURCETABLE = {
        //arrayvalue[NumberofBeneficiaries	,Medianshareofindividualincomespentonout-ofpockethealthcarecosts,AverateTotalOOPSpending,AverageOOPSpendingonPremiums,AverageOOPspendingonServices,	LongTermCareFacility	SkilledNursingFacility,	PerscriptionDrugs,	Providers/Suppliers	Dental,	OutpatientServices	,InpatientServices,	Other]
        "M": {
            "Under_65": [2979000, 0.08, 2876, 982, 1894, 607, 60, 389, 426, 269, 81, 23, 40],
            "Between_65_and_74": [6688000, 0.1, 4799, 2594, 2204, 706, 70, 453, 496, 313, 94, 26, 47],
            "Between_75_and_84": [3560000, 0.13, 5737, 2661, 3076, 985, 97, 632, 692, 436, 131, 37, 65],
            "Greater_than_85": [1366000, 0.15, 9802, 2493, 7309, 2341, 231, 1503, 1644, 1037, 312, 88, 155]
        },
        "F": {
            "Under_65": [2892000, 0.07, 2790, 819, 1971, 631, 62, 405, 443, 280, 84, 24, 42],
            "Between_65_and_74": [8103000, 0.13, 5205, 2705, 2501, 801, 79, 514, 562, 355, 107, 30, 53],
            "Between_75_and_84": [4603000, 0.14, 5900, 2703, 3197, 1024, 101, 657, 719, 453, 136, 38, 68],
            "Greater_than_85": [2535000, 0.17, 10579, 2052, 8527, 2731, 269, 1753, 1918, 1209, 364, 102, 180]
        },
        "U": {
            "Under_65": [5871000, 0.07, 2834, 901, 1932, 619, 61, 397, 434, 274, 82, 23, 41],
            "Between_65_and_74": [14791000, 0.12, 5021, 2655, 2367, 758, 75, 487, 532, 336, 101, 28, 50],
            "Between_75_and_84": [8163000, 0.14, 5829, 2685, 3144, 1007, 99, 646, 707, 446, 134, 38, 67],
            "Greater_than_85": [3901000, 0.16, 10307, 2206, 8101, 2595, 256, 1666, 1822, 1149, 345, 97, 171]
        }
    };
    //Max lifespan. Constant that we use in added years to support running caculations past someone's death
    const MAXLIFESPAN = 130;
    //The assumed healthcare inflation rate
    const HEALTHCAREINFLATION = 0.0199;
    const HEALTHCAREINFLATIONRATE = 1 + HEALTHCAREINFLATION;
    //The assumed HSA return rate (negative?)
    const HSARETURN = -0.0201;
    const HSARETURNRATE = 1 + HSARETURN;
    //The assumed rate of return for portfolio
    const PORTFOLIORETURN = 0.0199;
    const PORTFOLIORETURNRATE = 1 + PORTFOLIORETURN;
    //The assumed rate of portfolio value growth @ 5% EOY growth
    const EOYPORTFOLIOGROWTH = 0.95;
    //Get the years since 2016 to use in inflation adjustments
    const DATE = new Date();
    const BASEYEARADJUSTMENT = DATE.getFullYear() - 2016;
    /**
     * Future Value Calculation like Excel's version
     * @param {*} rate is the interest rate per period.
     * @param {*} nper is the total number of payment periods in an annuity.
     * @param {*} pmt is the payment made each period; it cannot change over the life of the annuity.Pmt must be entered as a negative number.
     * @param {*} pv is the present value, or the lump - sum amount that a series of future payments is worth right now.If pv is omitted, it is assumed to be 0(zero).PV must be entered as a negative number.
     * @param {*} type is the number 0 or 1 and indicates when payments are due.If type is omitted, it is assumed to be 0 which represents at the end of the period.If payments are due at the beginning of the period, type should be 1.
     * @returns 
     */
    var FV = function futureValue(rate, nper, pmt, pv, type) {
        var pow = Math.pow(1 + rate, nper);
        var fv;
        pv = pv || 0;
        type = type || 0;
        if (rate) {
            fv = (pmt * (1 + rate * type) * (1 - pow) / rate) - pv * pow;
        } else {
            fv = -1 * (pv + pmt * nper);
        }
        return fv;
    };
    /**
     * 
     * @param {number} currentAge is the spouse's current age
     * @returns the secondary age to use in spouse calculations
     */
    var getSecondaryAge = function (currentAge) {
        var secondaryAge;
        //If the primary retired  
        if (fiHcCalcData.primaryInputs.isRetired) {
            secondaryAge = currentAge;
        } else {
            //If the primary isn't retired then use the difference between the primary's retirement age and current age and add it to the spouses current age
            secondaryAge = currentAge + (fiHcCalcData.primaryInputs.retirementAge - fiHcCalcData.primaryInputs.currentAge);
        }
        return secondaryAge;
    };

    /**
    * PRIMARY USERS CALCULATION
    * @param {object} inputs for primary user
    * @returns {object} data object for primary
    */
    var calculatePrimary = function (inputs) {
        //Copy the return object. Can't use Object.assign. See https://www.samanthaming.com/tidbits/70-3-ways-to-clone-objects/
        let primaryObj = JSON.parse(JSON.stringify(returnObj));
        //let primaryObj = Object.assign({}, returnObj);
        //Add to the test inputs to data object for debugging
        fiHcCalcData.primaryInputs = inputs;
        //Additional years to calculate for (used as a buffer when calculating values after someones death). Based on 130 year old max lifespan.
        let ADDEDYEARS = MAXLIFESPAN - inputs.lifeExpectancy;
        //Add the ADDYEARS flag to the return object
        fiHcCalcData.primaryInputs.addedYears = ADDEDYEARS;
        //Track if the person is retired
        let isRetired = inputs.retirementAge < inputs.currentAge ? true : false;
        //If the person is retired use the current age, If the person is working use the retirement age
        let startAge = Math.max(inputs.currentAge, inputs.retirementAge);
        //Figure out the number of years we need to loop through
        let yearsInRetirement = inputs.lifeExpectancy - startAge;
        //Add the primary inputs to the data object so we can reference later
        fiHcCalcData.primaryInputs.isRetired = isRetired;
        fiHcCalcData.primaryInputs.startAge = startAge;
        fiHcCalcData.primaryInputs.yearsLived = yearsInRetirement;
        //Calculate years from today for inflations adjustments in cost breakdown
        let YEARSFROMTODAY = fiHcCalcData.primaryInputs.startAge - inputs.currentAge === 0 ? 0 : inputs.retirementAge - inputs.currentAge;
        fiHcCalcData.primaryInputs.YEARSFROMTODAY = YEARSFROMTODAY;
        //Find the correct source reference for gender
        let COSTTABLE = SOURCETABLE[inputs.gender];
        //Store the current HSA value
        let startYearHsaValue;
        //Store the current portfolio value
        let startYearRetirementSavings;
        //Init a counter to track the numbers of years that are out of pocket (used in step 6 block)
        let outOfPocketYearsCounter = 0;
        //Start at a base year 0 then add a year to our age until we die
        for (let i = 0; i <= yearsInRetirement; i++) {
            //Adjust for inflation: 1 + RATE ^ ((Retirement Age + i) - Current Age) + years since 2016
            let inflationRate = Math.pow(HEALTHCAREINFLATIONRATE, (startAge - inputs.currentAge) + BASEYEARADJUSTMENT);
            //Step 1: Populate the years array. Year 0 is either their currrent age (if retired) OR their retirment age (if still working)
            primaryObj.years.push(i);
            //Step 2: Find the total heathcare costs for the start age from the source table and push to array to fill the base year healthcare costs. Base year is 2016 prices.
            if (startAge < 65) {
                primaryObj.baseYearHcCosts.push(COSTTABLE["Under_65"][2]);
                //Step 2.1: Get the entire base year cost breakdown to display in the breakdown section. We adjust for inflation later.
                if (i === 0) {
                    primaryObj.baseYearCostBreakdown = COSTTABLE["Under_65"];
                }
            } else if (startAge >= 65 && startAge <= 74) {
                primaryObj.baseYearHcCosts.push(COSTTABLE["Between_65_and_74"][2]);
                //Step 2.1: Get the entire base year cost breakdown to display in the breakdown section. We adjust for inflation later.
                if (i === 0) {
                    primaryObj.baseYearCostBreakdown = COSTTABLE["Between_65_and_74"];
                }
            } else if (startAge >= 75 && startAge < 85) {
                primaryObj.baseYearHcCosts.push(COSTTABLE["Between_75_and_84"][2]);
                //Step 2.1: Get the entire base year cost breakdown to display in the breakdown section. We adjust for inflation later.
                if (i === 0) {
                    primaryObj.baseYearCostBreakdown = COSTTABLE["Between_75_and_84"];
                }
            } else {
                primaryObj.baseYearHcCosts.push(COSTTABLE["Greater_than_85"][2]);
                //Step 2.1: Get the entire base year cost breakdown to display in the breakdown section. We adjust for inflation later.
                if (i === 0) {
                    primaryObj.baseYearCostBreakdown = COSTTABLE["Greater_than_85"];
                }
            }
            //Step 3: Adust for inflation. Use baseYearHcCosts to get inflationAdjustedHcCosts 
            primaryObj.inflationAdjustedHcCosts.push(primaryObj.baseYearHcCosts[i] * inflationRate);
            //Add one year and get one year closer to death
            startAge++;
        }
        /** Calculations that we can handle outside of for the base loop above. Looping, reducing, or mapping the existing array */
        //Step 4: Get the HSA value for retired or currently working adjusted for inflation. First year is calculated differently so do that first and then enter the for loop block for the remaining years in step 3.5
        //Step 5: Get the portfolio value for retired or currently working adjusted for inflation. First year is calculated differently like HSA value is.
        if (isRetired) {
            //Step 4: If retired then the base HSA is the current HSA value minus first year cost
            startYearHsaValue = inputs.currentHealthSavings - primaryObj.inflationAdjustedHcCosts[0];
            //Step 4: Push the initial EOY HSA value to the array
            primaryObj.hsaValueEoy.push(startYearHsaValue);
            //Step 5: If retired then the starting retirement savings account value is the current value adjusted for growth
            startYearRetirementSavings = inputs.currentInvestibleAssets * EOYPORTFOLIOGROWTH;
            //Step 5: Push the initial retirement savings value to the array
            primaryObj.portfolioValue.push(startYearRetirementSavings);
        } else {
            //Step 4: If still working then calculate the current HSA value they but adjusted for inflation 
            let inflationAdjustedHsaSavings = inputs.currentHealthSavings * Math.pow(HSARETURNRATE, inputs.startAge - inputs.currentAge);
            //Step 4: Calculate the future value of all additional contributions
            let futureValueHsaContributions = FV(HSARETURN, inputs.retirementAge - inputs.currentAge, -inputs.hsaAdditionsTillRetirement);
            //Step 4: Add the inflation adjusted HSA start value + the value of any contributions then subtract inflation adjusted first year costs
            startYearHsaValue = (inflationAdjustedHsaSavings + futureValueHsaContributions) - primaryObj.inflationAdjustedHcCosts[0];
            //Step 4: Push the initial EOY HSA value to the array
            primaryObj.hsaValueEoy.push(startYearHsaValue);
            //Step 5: If still working then current portfolio value equals rate of return plus future value
            let startYearAdjustedSavings = inputs.currentInvestibleAssets * Math.pow(PORTFOLIORETURNRATE, inputs.retirementAge - inputs.currentAge);
            //Step 5:Add the future value of contributions
            let futureValuePortfolioContributions = FV(PORTFOLIORETURN, inputs.retirementAge - inputs.currentAge, -inputs.additionsTillRetirement, 0);
            //Step 5: Add the adjusted start year retirement savings plus the future value of contributions to get the inital value
            startYearRetirementSavings = (startYearAdjustedSavings + futureValuePortfolioContributions) * EOYPORTFOLIOGROWTH;
            //Step 5: Push the initial portfolio value to the array
            primaryObj.portfolioValue.push(startYearRetirementSavings);
        }
        //Step 4.1: Loop through adjusted healthcare costs to get the remaining end of year HSA values using ADDEDYEARS to buffer calculations to account for someones death
        //Step 5.1: Loop through to get the remaining portfolio value aka annual retirement savings using ADDEDYEARS to buffer calculations to account for someones death
        for (let i = 0; i < primaryObj.inflationAdjustedHcCosts.length + ADDEDYEARS; i++) {
            //Set empty or null values to 0 when we go past the primary users' death
            let hcCosts = primaryObj.inflationAdjustedHcCosts[i] || 0;
            //Skip the first index becuase start years are already handled in the if/else block above
            if (i > 0) {
                //Step 4.1 Stuff
                var previousYearHsaValueEoy = primaryObj.hsaValueEoy[i - 1];
                var currentYearHsa = (previousYearHsaValueEoy * HSARETURNRATE) - hcCosts;
                primaryObj.hsaValueEoy.push(currentYearHsa);
                //Step 5.1 Stuff
                var previousYearPortfolioValue = primaryObj.portfolioValue[i - 1];
                var currentYearPortfolioValue = (previousYearPortfolioValue * EOYPORTFOLIOGROWTH) * PORTFOLIORETURNRATE;
                primaryObj.portfolioValue.push(currentYearPortfolioValue);
            }
        }
        //Step 6: Get the out of pocket totals for retired or currently working adjusted for inflation by looping through HSA value. When there's no HSA just push the inflation adjusted cost
        //Step 7: Get the portion covered by the HSA. We can do this at the same time because we look at EOY of HSA value in this loop
        //Step 8: Fill the Fully Out of Pocket Years Array. If the inflation adjusted healthcare costs the out of pocket exluding HSA cost add 1
        for (let i = 0; i < primaryObj.hsaValueEoy.length; i++) {
            //Get the number of living years we're calculating for
            let livingYearsToFill = primaryObj.years.length;
            //Set empty or null array indexes to 0
            primaryObj.hsaValueEoy[i] = primaryObj.hsaValueEoy[i] || 0;
            //HSA Value is above 0 so no out of pocket costs i.e. everything is covered by the HSA
            if (primaryObj.hsaValueEoy[i] > 0) {
                //Update the out of pocket array with $0 for the year
                primaryObj.outOfPocketExHsa.push(0);
                //The entire year's healthcare costs are covered by the HSA. Only update if still alive
                if (i < livingYearsToFill) {
                    primaryObj.coveredByHsa.push(primaryObj.inflationAdjustedHcCosts[i]);
                }
            }//Current HSA is less than 0 but it's the first year OR current HSA is less than 0 but there's a little bit leftover from previous year to use
            else if (primaryObj.hsaValueEoy[i] <= 0 && i == 0 || primaryObj.hsaValueEoy[i] <= 0 && primaryObj.hsaValueEoy[i - 1] > 0) {
                //Push the HSA value to both the arrays but flip the value to a positive number
                primaryObj.outOfPocketExHsa.push(Math.abs(primaryObj.hsaValueEoy[i]));
                //Only update if still alive and the hsaValueEoy is equal to the outOfPocketExHsa costs
                if (i < livingYearsToFill && (Math.abs(primaryObj.hsaValueEoy[i]) === primaryObj.outOfPocketExHsa[i])) {
                    //This should equal 0 so I'm not sure why we don't hardcode 0
                    let aLittleBitofHsa = primaryObj.inflationAdjustedHcCosts[i] - primaryObj.outOfPocketExHsa[i];
                    primaryObj.coveredByHsa.push(aLittleBitofHsa);
                }
            }//No HSA so push the inflation adjusted healthcare cost as the total out of pocket
            else {
                //Push the healthcare cost to the out of pocket array
                primaryObj.outOfPocketExHsa.push(primaryObj.inflationAdjustedHcCosts[i]);
                //Update the covered by HSA array with $0 for the year. Only update if still alive
                if (i < livingYearsToFill) {
                    primaryObj.coveredByHsa.push(0);
                }
            }
            //Step 5.2: Calculate healthcare costs as a percentage of total retirement savings/portfolio value. Need to complete Step 5 first before we can do this
            //Step 7.1: Go back and fill out of pocket array for the graph 1 while the person is living. Need to complete Step 7 first above before we handle this
            if (i < livingYearsToFill) {
                //Step 5.2 Stuff
                let hcCostsAsPercentOfPortfolio = primaryObj.inflationAdjustedHcCosts[i] / (primaryObj.portfolioValue[i] * (1 - EOYPORTFOLIOGROWTH));
                //Make sure the number is inifity
                if (hcCostsAsPercentOfPortfolio !== Infinity) {
                    primaryObj.hcPortfolioRatio.push(hcCostsAsPercentOfPortfolio * 100);
                } else {
                    primaryObj.hcPortfolioRatio.push(0);
                }
                //Step 7.1 Stuff
                let oopDiff = primaryObj.inflationAdjustedHcCosts[i] - primaryObj.coveredByHsa[i];
                primaryObj.oop.push(oopDiff);
            }
            //Step 9: Update the fully out of pocket years array
            //Set empty or null values to 0 when we go past the primary user's death
            primaryObj.inflationAdjustedHcCosts[i] = primaryObj.inflationAdjustedHcCosts[i] || 0;
            primaryObj.outOfPocketExHsa[i] = primaryObj.outOfPocketExHsa[i] || 0;
            //Update the fully out of pocket years counter by 1 when inflation adjusted cost equals out of pocket total excluding the HSA
            if (primaryObj.inflationAdjustedHcCosts[i] === primaryObj.outOfPocketExHsa[i]) {
                outOfPocketYearsCounter += 1;
            } else {
                outOfPocketYearsCounter = 0;
            }
            //Populate the fully out of pocket years counter array
            primaryObj.outOfPocketYears.push(outOfPocketYearsCounter);
        }
        //Step 10: Adjust the first year cost breakdown for inflation (base table is based on 2016 data)
        let inflationAdjustedCostBreakdown = primaryObj.baseYearCostBreakdown.map(function (el, i) {
            //Add 1 to baseyearadjustment to get current year
            return el * Math.pow(HEALTHCAREINFLATIONRATE, YEARSFROMTODAY + BASEYEARADJUSTMENT);
        });
        primaryObj.inflationAdjustedCostBreakdown = inflationAdjustedCostBreakdown;
        //Step 11: Lifetime Totals
        //Lifetime Total HC Cost
        primaryObj.lifetimeHcCosts = primaryObj.inflationAdjustedHcCosts.reduce(function (a, b) {
            return a + b;
        });
        //Lifetime HSA Amount
        primaryObj.lifetimeHsa = primaryObj.coveredByHsa.reduce(function (a, b) {
            return a + b;
        });
        //Lifetime OOP Pocket
        //lifetimeOop = Math.round(OOP.reduce(function (a, b) { return a + b; }));
        primaryObj.lifetimeOop = primaryObj.lifetimeHcCosts - primaryObj.lifetimeHsa;
        //Lifetime Covered by HSA
        primaryObj.lifetimePercentCovered = Math.round((primaryObj.lifetimeHsa / primaryObj.lifetimeHcCosts) * 100);
        //Do they have an HSA?
        primaryObj.hasHsaFlag = primaryObj.lifetimeHsa === 0 ? false : true;
        //Finally: the results
        //console.log('primary ', primaryObj);
        return primaryObj;
    };
    /**
     * Spouse calculations copied from primary with tweaks to support 
     * @param {object} inputs for primary user's spouse
     * @returns {object} data object for spouse
     */
    var calculateSpouse = function (inputs) {
        //Copy the return object. Can't use object assign. See https://www.samanthaming.com/tidbits/70-3-ways-to-clone-objects/
        let spouseObj = JSON.parse(JSON.stringify(returnObj));
        //let spouseObj = Object.assign({}, returnObj);
        //Add to the test inputs to window object for debugging
        fiHcCalcData.spouseInputs = inputs;
        //INPUT MODIFICATION: the spouse should use retirement age to primary's current or retirement age
        let adjustedRetirementAge = fiHcCalcData.primaryInputs.retirementAge;
        fiHcCalcData.spouseInputs.primaryRetirementAge = adjustedRetirementAge;
        //INPUT MODIFICATION: the spouse needs to track a secondary age for use in calculations this is the start age
        let startAge = getSecondaryAge(inputs.currentAge);
        fiHcCalcData.spouseInputs.startAge = startAge;
        //Additional years to calculate for (used as a buffer when calculating values after someones death). Based on 130 year old max lifespan.
        const ADDEDYEARS = MAXLIFESPAN - inputs.lifeExpectancy;
        //Add the ADDYEARS flag to the return object
        fiHcCalcData.spouseInputs.addedYears = ADDEDYEARS;
        //Track if the person is retired
        let isRetired = inputs.retirementAge < inputs.currentAge ? true : false;
        //Figure out the number of years we need to loop through - for spouse this is life expectancy plus the difference between the primary's age and the spouses current age. 
        let spouseYearsInRetirement = ((inputs.lifeExpectancy + (adjustedRetirementAge - inputs.currentAge))) - startAge;
        let spouseYearsLived = inputs.lifeExpectancy - fiHcCalcData.spouseInputs.startAge;
        //Add the primary inputs to the data object so we can reference later
        fiHcCalcData.spouseInputs.isRetired = isRetired;
        fiHcCalcData.spouseInputs.yearsLived = spouseYearsInRetirement;
        //Calculate years from today in terms of primary for inflations adjustments in cost breakdown
        let YEARSFROMTODAY = fiHcCalcData.primaryInputs.YEARSFROMTODAY;
        fiHcCalcData.spouseInputs.YEARSFROMTODAY = YEARSFROMTODAY;
        //Find the correct source reference for gender
        let COSTTABLE = SOURCETABLE[inputs.gender];
        //Store the current HSA value
        let startYearHsaValue;
        //Store the current portfolio value
        let startYearRetirementSavings;
        //Init a counter to track the numbers of years that are out of pocket (used in step 6 block)
        let outOfPocketYearsCounter = 0;
        //Start at a base year 0 then add a year to our age until we die
        for (let i = 0; i <= spouseYearsInRetirement + ADDEDYEARS; i++) {
            //Adjust for inflation: 1 + RATE ^ ((Retirement Age + i) - Current Age) + years since 2016
            let inflationRate = Math.pow(HEALTHCAREINFLATIONRATE, (startAge - inputs.currentAge) + BASEYEARADJUSTMENT);
            //Step 1: Populate the years array. Year 0 is either their currrent age (if retired) OR their retirment age (if still working)
            spouseObj.years.push(i);
            //Step 2: Find the total heathcare costs for the start age from the source table and push to array to fill the base year healthcare costs. Base year is 2016 prices.
            if (startAge < 65) {
                spouseObj.baseYearHcCosts.push(COSTTABLE["Under_65"][2]);
                //Step 2.1: Get the entire base year cost breakdown to display in the breakdown section. We adjust for inflation later.
                if (i === 0) {
                    spouseObj.baseYearCostBreakdown = COSTTABLE["Under_65"];
                }
            } else if (startAge >= 65 && startAge <= 74) {
                spouseObj.baseYearHcCosts.push(COSTTABLE["Between_65_and_74"][2]);
                //Step 2.1: Get the entire base year cost breakdown to display in the breakdown section. We adjust for inflation later.
                if (i === 0) {
                    spouseObj.baseYearCostBreakdown = COSTTABLE["Between_65_and_74"];
                }
            } else if (startAge >= 75 && startAge < 85) {
                spouseObj.baseYearHcCosts.push(COSTTABLE["Between_75_and_84"][2]);
                //Step 2.1: Get the entire base year cost breakdown to display in the breakdown section. We adjust for inflation later.
                if (i === 0) {
                    spouseObj.baseYearCostBreakdown = COSTTABLE["Between_75_and_84"];
                }
            } else {
                spouseObj.baseYearHcCosts.push(COSTTABLE["Greater_than_85"][2]);
                //Step 2.1: Get the entire base year cost breakdown to display in the breakdown section. We adjust for inflation later.
                if (i === 0) {
                    spouseObj.baseYearCostBreakdown = COSTTABLE["Greater_than_85"];
                }
            }
            //Step 3: Adust for inflation. Use baseYearHcCosts to get inflationAdjustedHcCosts 
            spouseObj.inflationAdjustedHcCosts.push(spouseObj.baseYearHcCosts[i] * inflationRate);
            //Add one year and get one year closer to death
            startAge++;
        }
        /** Calculations that we can handle outside of for the base loop above. Looping, reducing, or mapping the existing array */
        //SPOUSE MODIFICATION: check if the secondary age is greater than the current age
        //Step 4: Get the HSA value for retired or currently working adjusted for inflation. First year is calculated differently so do that first and then enter the for loop block for the remaining years in step 3.5
        //Step 5: Get the portfolio value for retired or currently working adjusted for inflation. First year is calculated differently like HSA value is.
        if (fiHcCalcData.spouseInputs.startAge > inputs.currentAge) {
            //Step 4: If still working then calculate the current HSA value they but adjusted for inflation 
            let inflationAdjustedHsaSavings = inputs.currentHealthSavings * Math.pow(HSARETURNRATE, inputs.startAge - inputs.currentAge);
            //Step 4: USE PRIMARY'S RETIREMENT AGE: Calculate the future value of all additional contributions
            let futureValueHsaContributions = FV(HSARETURN, inputs.primaryRetirementAge - inputs.currentAge, -inputs.hsaAdditionsTillRetirement);
            //Step 4: Add the inflation adjusted HSA start value + the value of any contributions then subtract inflation adjusted first year costs
            startYearHsaValue = (inflationAdjustedHsaSavings + futureValueHsaContributions) - spouseObj.inflationAdjustedHcCosts[0];
            //Step 4: Push the initial EOY HSA value to the array
            spouseObj.hsaValueEoy.push(startYearHsaValue);
            //Step 5: USE PRIMARY'S RETIREMENT AGE: If still working then current portfolio value equals rate of return plus future value
            let startYearAdjustedSavings = inputs.currentInvestibleAssets * Math.pow(PORTFOLIORETURNRATE, fiHcCalcData.spouseInputs.startAge - inputs.currentAge);
            //Step 5: USE PRIMARY'S RETIREMENT AGE: Add the future value of contributions
            let futureValuePortfolioContributions = FV(PORTFOLIORETURN, fiHcCalcData.spouseInputs.startAge - inputs.currentAge, -inputs.additionsTillRetirement, 0);
            //Step 5: Add the adjusted start year retirement savings plus the future value of contributions to get the inital value
            startYearRetirementSavings = (startYearAdjustedSavings + futureValuePortfolioContributions) * EOYPORTFOLIOGROWTH;
            //Step 5: Push the initial portfolio value to the array
            spouseObj.portfolioValue.push(startYearRetirementSavings);
        } else {
            //Step 4: If retired then the base HSA is the current HSA value minus first year cost
            startYearHsaValue = inputs.currentHealthSavings - spouseObj.inflationAdjustedHcCosts[0];
            //Step 4: Push the initial EOY HSA value to the array
            spouseObj.hsaValueEoy.push(startYearHsaValue);
            //Step 5: If retired then the starting retirement savings account value is the current value adjusted for growth
            startYearRetirementSavings = inputs.currentInvestibleAssets * EOYPORTFOLIOGROWTH;
            //Step 5: Push the initial retirement savings value to the array
            spouseObj.portfolioValue.push(startYearRetirementSavings);
        }
        //Step 4.1: Loop through adjusted healthcare costs to get the remaining end of year HSA values
        //Step 5.1: Loop through to get the remaining portfolio value aka annual retirement savings
        for (let i = 0; i < spouseObj.inflationAdjustedHcCosts.length; i++) {
            //Set empty or null values to 0 when we go past the primary users' death
            let hcCosts = spouseObj.inflationAdjustedHcCosts[i] || 0;
            //Skip the first index becuase start years are already handled in the if/else block above
            if (i > 0) {
                //Step 4.1 Stuff
                let previousYearHsaValueEoy = spouseObj.hsaValueEoy[i - 1];
                let currentYearHsa = (previousYearHsaValueEoy * HSARETURNRATE) - hcCosts;
                spouseObj.hsaValueEoy.push(currentYearHsa);
                //Step 5.1 Stuff
                let previousYearPortfolioValue = spouseObj.portfolioValue[i - 1];
                let currentYearPortfolioValue = (previousYearPortfolioValue * EOYPORTFOLIOGROWTH) * PORTFOLIORETURNRATE;
                spouseObj.portfolioValue.push(currentYearPortfolioValue);
            }
        }
        //Step 6: Get the out of pocket totals for retired or currently working adjusted for inflation by looping through HSA value. When there's no HSA just push the inflation adjusted cost
        //Step 7: Get the portion covered by the HSA. We can do this at the same time because we look at EOY of HSA value in this loop
        //Step 8: Fill the Fully Out of Pocket Years Array. If the inflation adjusted healthcare costs the out of pocket exluding HSA cost add 1
        for (let i = 0; i < spouseObj.hsaValueEoy.length; i++) {
            //Get the number of living years we're calculating for
            let livingYearsToFill = inputs.lifeExpectancy - fiHcCalcData.spouseInputs.startAge;
            //Set empty or null array indexes to 0
            spouseObj.hsaValueEoy[i] = spouseObj.hsaValueEoy[i] || 0;
            //HSA Value is above 0 so no out of pocket costs i.e. everything is covered by the HSA
            if (spouseObj.hsaValueEoy[i] > 0) {
                //Update the out of pocket array with $0 for the year
                spouseObj.outOfPocketExHsa.push(0);
                //The entire year's healthcare costs are covered by the HSA. Only update if still alive
                if (i < livingYearsToFill) {
                    spouseObj.coveredByHsa.push(spouseObj.inflationAdjustedHcCosts[i]);
                }
            }//Current HSA is less than 0 but it's the first year OR current HSA is less than 0 but there's a little bit leftover from previous year to use
            else if (spouseObj.hsaValueEoy[i] <= 0 && i == 0 || spouseObj.hsaValueEoy[i] <= 0 && spouseObj.hsaValueEoy[i - 1] > 0) {
                //Push the HSA value to both the arrays but flip the value to a positive number
                spouseObj.outOfPocketExHsa.push(Math.abs(spouseObj.hsaValueEoy[i]));
                //Only update if still alive and the hsaValueEoy is equal to the outOfPocketExHsa costs
                if (i < livingYearsToFill && (Math.abs(spouseObj.hsaValueEoy[i]) === spouseObj.outOfPocketExHsa[i])) {
                    //This should equal 0 so I'm not sure why we don't hardcode 0
                    let aLittleBitofHsa = spouseObj.inflationAdjustedHcCosts[i] - spouseObj.outOfPocketExHsa[i];
                    spouseObj.coveredByHsa.push(aLittleBitofHsa);
                }
            }//No HSA so push the inflation adjusted healthcare cost as the total out of pocket
            else {
                //Push the healthcare cost to the out of pocket array
                spouseObj.outOfPocketExHsa.push(spouseObj.inflationAdjustedHcCosts[i]);
                //Update the covered by HSA array with $0 for the year. Only update if still alive
                if (i < livingYearsToFill) {
                    spouseObj.coveredByHsa.push(0);
                }
            }
            //Step 5.2: Calculate healthcare costs as a percentage of total retirement savings/portfolio value. Need to complete Step 5 first before we can do this
            //Step 7.1: Go back and fill out of pocket array for the graph 1 while the person is living. Need to complete Step 7 first above before we handle this
            if (i < livingYearsToFill) {
                //Step 5.2 Stuff
                let hcCostsAsPercentOfPortfolio = spouseObj.inflationAdjustedHcCosts[i] / (spouseObj.portfolioValue[i] * (1 - EOYPORTFOLIOGROWTH));
                //Make sure the number is inifity
                if (hcCostsAsPercentOfPortfolio !== Infinity) {
                    spouseObj.hcPortfolioRatio.push(hcCostsAsPercentOfPortfolio * 100);
                } else {
                    spouseObj.hcPortfolioRatio.push(0);
                }
                //Step 7.1 Stuff
                let oopDiff = spouseObj.inflationAdjustedHcCosts[i] - spouseObj.coveredByHsa[i];
                spouseObj.oop.push(oopDiff);
            }
            //Step 9: Update the fully out of pocket years array
            //Set empty or null values to 0 when we go past the primary user's death
            spouseObj.inflationAdjustedHcCosts[i] = spouseObj.inflationAdjustedHcCosts[i] || 0;
            spouseObj.outOfPocketExHsa[i] = spouseObj.outOfPocketExHsa[i] || 0;
            //Update the fully out of pocket years counter by 1 when inflation adjusted cost equals out of pocket total excluding the HSA
            if (spouseObj.inflationAdjustedHcCosts[i] === spouseObj.outOfPocketExHsa[i]) {
                outOfPocketYearsCounter += 1;
            } else {
                outOfPocketYearsCounter = 0;
            }
            //Populate the fully out of pocket years counter array
            spouseObj.outOfPocketYears.push(outOfPocketYearsCounter);
        }
        //Step 10: Adjust the first year cost breakdown for inflation (base table is based on 2016 data)
        let inflationAdjustedCostBreakdown = spouseObj.baseYearCostBreakdown.map(function (el, i) {
            //Add 1 to baseyearadjustment to get current year
            return el * Math.pow(HEALTHCAREINFLATIONRATE, YEARSFROMTODAY + BASEYEARADJUSTMENT);
        });
        spouseObj.inflationAdjustedCostBreakdown = inflationAdjustedCostBreakdown;
        //Step 11: Lifetime Totals
        //Lifetime Total HC Cost - only count years the spouse is alive. 
        let hcCostsWhileAlive = spouseObj.inflationAdjustedHcCosts.slice(0, spouseYearsLived + 1);
        //Only reduce if array has values
        spouseObj.hcCostsWhileAlive = hcCostsWhileAlive;
        if (hcCostsWhileAlive.length > 0) {
            spouseObj.lifetimeHcCosts = hcCostsWhileAlive.reduce(function (a, b) {
                return a + b;
            });
        }
        //Lifetime HSA Amount - only reduce if array has values
        if (spouseObj.coveredByHsa.length > 0) {
            spouseObj.lifetimeHsa = spouseObj.coveredByHsa.reduce(function (a, b) {
                return a + b;
            });
        }
        //Lifetime OOP Pocket
        //lifetimeOop = Math.round(OOP.reduce(function (a, b) { return a + b; }));
        spouseObj.lifetimeOop = spouseObj.lifetimeHcCosts - spouseObj.lifetimeHsa;
        //Lifetime Covered by HSA 
        spouseObj.lifetimePercentCovered = Math.round((spouseObj.lifetimeHsa / spouseObj.lifetimeHcCosts) * 100);
        //Do they have an HSA?
        spouseObj.hasHsaFlag = spouseObj.lifetimeHsa === 0 ? false : true;
        //Finally: the results
        //console.log('spouse ', spouseObj);
        return spouseObj;
    };
    /**
     * 
     * @param {object} primaryData 
     * @param {object} spouseData 
     * @returns {object} combined output of both 
     */
    var combineData = function (primaryData, spouseData) {
        //Copy the return object. Can't use object assign. See https://www.samanthaming.com/tidbits/70-3-ways-to-clone-objects/
        let combinedObj = {};
        //Add the rest of the arrarys 
        combinedObj.outOfPocketExHsa = [];
        combinedObj.coveredByHsa = [];
        combinedObj.hcPortfolioRatio = [];
        combinedObj.oop = [];
        combinedObj.outOfPocketYears = [];
        combinedObj.years = [];
        combinedObj.inflationAdjustedCostBreakdown = [];
        //Spouse life expectancy expressed in primary's years
        let secondaryLifeExpectancy = (fiHcCalcData.primaryInputs.currentAge - fiHcCalcData.spouseInputs.currentAge) + fiHcCalcData.spouseInputs.lifeExpectancy;
        //Who lives longest
        let maxLifeExpectancy = Math.max(fiHcCalcData.primaryInputs.lifeExpectancy, secondaryLifeExpectancy);
        //How many years we should combine (used to slice arrays)
        let yearsToCombine = maxLifeExpectancy - fiHcCalcData.primaryInputs.startAge;
        //The number of living years we're calculating for
        var livingYearsToFill = yearsToCombine + 1;
        //Init a counter to track the numbers of years that are out of pocket (used in step 6 block)
        let outOfPocketYearsCounter = 0;
        //Combine the arrays...
        let combinedHcCosts = primaryData.inflationAdjustedHcCosts.map(function (el, i) {
            let spouseHcCosts = spouseData.hcCostsWhileAlive[i];
            //Set empty, null, or undefined indexes to 0
            el = el || 0;
            spouseHcCosts = spouseHcCosts || 0;
            return el + spouseHcCosts;
        });
        let combinedHsaValueEoy = primaryData.hsaValueEoy.map(function (el, i) {
            let spouseHsaValueEoy = spouseData.hsaValueEoy[i];
            //Set empty, null, or undefined indexes to 0
            el = el || 0;
            spouseHsaValueEoy = spouseHsaValueEoy || 0;
            return el + spouseHsaValueEoy;
        });
        let combinedPortfolioValue = primaryData.portfolioValue.map(function (el, i) {
            let spousePortfolioValue = spouseData.portfolioValue[i];
            //Set empty, null, or undefined indexes to 0
            el = el || 0;
            spousePortfolioValue = spousePortfolioValue || 0;
            return el + spousePortfolioValue;
        });
        let combinedYears = function () {
            let years = [];
            for (let i = 0; i < livingYearsToFill; i++) {
                years.push(i);
            }
            return years;
        };
        let combinedBreakdown = primaryData.inflationAdjustedCostBreakdown.map(function (el, i) {
            let spouseBreakdown = spouseData.inflationAdjustedCostBreakdown[i];
            //Don't need to support null or empty because breakdowns arrays are set at matching lengths
            return el + spouseBreakdown;
        });
        //...but only add the living bits
        combinedObj.inflationAdjustedHcCosts = combinedHcCosts.slice(0, livingYearsToFill);
        combinedObj.hsaValueEoy = combinedHsaValueEoy.slice(0, livingYearsToFill);
        combinedObj.portfolioValue = combinedPortfolioValue.slice(0, livingYearsToFill);
        combinedObj.years = combinedYears();
        combinedObj.inflationAdjustedCostBreakdown = combinedBreakdown;
        //Step 6: Get the out of pocket totals for retired or currently working adjusted for inflation by looping through HSA value. When there's no HSA just push the inflation adjusted cost
        //Step 7: Get the portion covered by the HSA. We can do this at the same time because we look at EOY of HSA value in this loop
        //Step 8: Fill the Fully Out of Pocket Years Array. If the inflation adjusted healthcare costs the out of pocket exluding HSA cost add 1
        for (let i = 0; i < combinedObj.hsaValueEoy.length; i++) {
            //Set empty or null array indexes to 0
            combinedObj.hsaValueEoy[i] = combinedObj.hsaValueEoy[i] || 0;
            //HSA Value is above 0 so no out of pocket costs i.e. everything is covered by the HSA
            if (combinedObj.hsaValueEoy[i] > 0) {
                //Update the out of pocket array with $0 for the year
                combinedObj.outOfPocketExHsa.push(0);
                //The entire year's healthcare costs are covered by the HSA. Only update if still alive
                if (i < livingYearsToFill) {
                    combinedObj.coveredByHsa.push(combinedObj.inflationAdjustedHcCosts[i]);
                }
            }//Current HSA is less than 0 but it's the first year OR current HSA is less than 0 but there's a little bit leftover from previous year to use
            else if (combinedObj.hsaValueEoy[i] <= 0 && i == 0 || combinedObj.hsaValueEoy[i] <= 0 && combinedObj.hsaValueEoy[i - 1] > 0) {
                //Push the HSA value to both the arrays but flip the value to a positive number
                combinedObj.outOfPocketExHsa.push(Math.abs(combinedObj.hsaValueEoy[i]));
                //Only update if still alive and the hsaValueEoy is equal to the outOfPocketExHsa costs
                if (i < livingYearsToFill && (Math.abs(combinedObj.hsaValueEoy[i]) === combinedObj.outOfPocketExHsa[i])) {
                    //This should equal 0 so I'm not sure why we don't hardcode 0
                    let aLittleBitofHsa = combinedObj.inflationAdjustedHcCosts[i] - combinedObj.outOfPocketExHsa[i];
                    combinedObj.coveredByHsa.push(aLittleBitofHsa);
                }
            }//No HSA so push the inflation adjusted healthcare cost as the total out of pocket
            else {
                //Push the healthcare cost to the out of pocket array
                combinedObj.outOfPocketExHsa.push(combinedObj.inflationAdjustedHcCosts[i]);
                //Update the covered by HSA array with $0 for the year. Only update if still alive
                if (i < livingYearsToFill) {
                    combinedObj.coveredByHsa.push(0);
                }
            }
            //Step 5.2: Calculate healthcare costs as a percentage of total retirement savings/portfolio value. Need to complete Step 5 first before we can do this
            //Step 7.1: Go back and fill out of pocket array for the graph 1 while the person is living. Need to complete Step 7 first above before we handle this
            if (i < livingYearsToFill) {
                //Step 5.2 Stuff
                let hcCostsAsPercentOfPortfolio = combinedObj.inflationAdjustedHcCosts[i] / (combinedObj.portfolioValue[i] * (1 - EOYPORTFOLIOGROWTH));
                //Make sure the number is inifity
                if (hcCostsAsPercentOfPortfolio !== Infinity) {
                    combinedObj.hcPortfolioRatio.push(hcCostsAsPercentOfPortfolio * 100);
                } else {
                    combinedObj.hcPortfolioRatio.push(0);
                }
                //Step 7.1 Stuff
                let oopDiff = combinedObj.inflationAdjustedHcCosts[i] - combinedObj.coveredByHsa[i];
                combinedObj.oop.push(oopDiff);
            }
            //Step 9: Update the fully out of pocket years array
            //Set empty or null values to 0 when we go past the primary user's death
            combinedObj.inflationAdjustedHcCosts[i] = combinedObj.inflationAdjustedHcCosts[i] || 0;
            combinedObj.outOfPocketExHsa[i] = combinedObj.outOfPocketExHsa[i] || 0;
            //Update the fully out of pocket years counter by 1 when inflation adjusted cost equals out of pocket total excluding the HSA
            if (combinedObj.inflationAdjustedHcCosts[i] === combinedObj.outOfPocketExHsa[i]) {
                outOfPocketYearsCounter += 1;
            } else {
                outOfPocketYearsCounter = 0;
            }
            //Populate the fully out of pocket years counter array
            combinedObj.outOfPocketYears.push(outOfPocketYearsCounter);
        }
        //Lifetime Total HC Cost
        combinedObj.lifetimeHcCosts = primaryResults.lifetimeHcCosts + spouseResults.lifetimeHcCosts;
        //Lifetime HSA Amount uses the higher of the inherited HSA (inherited when a spouse dies) or combined HSA
        //Only run if coveredByHsa array has values
        if (combinedObj.coveredByHsa.length > 0) {
            //Find the inherited HSA value. This reduces the covered by HSA to account for death and HSA inheritance
            combinedObj.inheritedHsa = combinedObj.coveredByHsa.reduce(function (a, b) {
                return a + b;
            });
        }
        //Find the pure combined Hsa (the living HSA amount for each individual)
        combinedObj.combinedHsa = primaryResults.lifetimeHsa + spouseResults.lifetimeHsa;
        // Use the bigger of the two values above for lifetimeHsa
        combinedObj.lifetimeHsa = combinedObj.inheritedHsa > combinedObj.combinedHsa ? combinedObj.inheritedHsa : combinedObj.combinedHsa;
        //Lifetime OOP Pocket
        //lifetimeOop = Math.round(OOP.reduce(function (a, b) { return a + b; }));
        combinedObj.lifetimeOop = combinedObj.lifetimeHcCosts - combinedObj.lifetimeHsa;
        //Lifetime Covered by HSA
        combinedObj.lifetimePercentCovered = Math.round((combinedObj.lifetimeHsa / combinedObj.lifetimeHcCosts) * 100);
        //Do they have an HSA?
        combinedObj.hasHsaFlag = combinedObj.lifetimeHsa === 0 ? false : true;
        //console.log('combined ', combinedObj);
        return combinedObj;
    };
    //Store the primary results
    let primaryResults = calculatePrimary(primaryInputs);
    window.fiHcCalcData.primaryResults = primaryResults;
    //Store the spouse results
    let spouseResults = calculateSpouse(spouseInputs);
    window.fiHcCalcData.spouseResults = spouseResults;
    //Store the combined results
    let combinedResults = combineData(primaryResults, spouseResults);
    window.fiHcCalcData.combinedResults = combinedResults;
    //Return everything to the global window
    return fiHcCalcData;
};

/**
* Test inputs
*/
// var primaryInputs = {
//     "gender": "F",
//     "currentAge": 65,
//     "retirementAge": 75,
//     "lifeExpectancy": 95,
//     "currentInvestibleAssets": 10000000,
//     "additionsTillRetirement": 0,
//     "currentHealthSavings": 30000000,
//     "hsaAdditionsTillRetirement": 0
// };
// var spouseInputs = {
//     "gender": "F",
//     "currentAge": 58,
//     "retirementAge": 50,
//     "lifeExpectancy": 93,
//     "currentInvestibleAssets": 250000,
//     "additionsTillRetirement": 10000,
//     "currentHealthSavings": 1000,
//     "hsaAdditionsTillRetirement": 0
// };
// //Run everthing here
// fiHcCalc.hcEngine(primaryInputs, true);