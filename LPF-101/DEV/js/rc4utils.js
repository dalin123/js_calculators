"use strict";
function sanitizeStringOfDigits(x) {
  if (!x) {
    x = "0";  // default to 0
  }
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/[^0-9\-]/g, '');
  try {
    return parseInt(parts[0]);
  } catch (ex) {
    // ignore errors
  }
  return 0; // if this gets here, return zero
}
function sanitizeStringOfDigitsAndCommas(x) {
  if (!x) {
    return 0;
  }
  if (typeof (x) == "number") {
    return x;
  }
  x.replace(",", "");
  x.replace("$", "");
  return sanitizeStringOfDigits(x);
}
function convertTextToWishesCode(wishesText) {
  if (wishesText === "would like to") {
    return "707";
  } else if (wishesText === "would not like to") {
    return "627";
  }
  return "";
}
function sanitizeStringOfDigitsAsString(x) {
  if (!x) {
    return "";
  }
  if (typeof (x) === "number") {
    return x.toString();
  }
  // replace anything that isn't a digit
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/[^0-9\-]/g, '');
  return parts[0];
}
function addCommas(x) {
  if (!x) {
    x = '0';  // default to 0
  }
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts[0]; // ignore the decimals
}
function isRetired() {
  return $("#Retired:checked").length > 0;
}
function notRetired() {
  return !isRetired();
}
