/************************************************************************************************/
/************************************* Formatting Functions *************************************/
/************************************************************************************************/
function ReasonableRound(n) {
  var divCount = 0;
  while (n > 100) {
      n = n / 10;
      divCount++;
  }
  n = Math.round(n); // round to nearest integer
  while (divCount > 0) {
      n *= 10;
      divCount--;
  }
  return n;
}

function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

function createRounder(decimals) {
  return (function roundFn(value) {
    round(value, decimals);
  });
}

function FormatCurrencyKM(val, rounder) {
  var suffix = "";
  var sign = "";
  if (val < 0) {
      sign = "-";
      val = -val;
  }
  if (val < 1000) { // do nothing
  } else if (val < 1000000) {
      val = Math.round(val / 1000); // if less than 1M, get rid of decimals
      suffix = "K";
  } else if (val < 1000000000) {
      val = val / 1000000;
      suffix = "M";
  }

  if (rounder) {
    val = rounder(val);
  } else {
    val = round(val, 2);
  }

  return "$" + sign + val + suffix;
}

/************************************************************************************************/
/************************************* Calculation Functions ************************************/
/************************************************************************************************/
function getTaxRate(withdrawal) {
  //TODO:Check if Usewr has given tax rate
  var effectiveTaxAmount = 0;
  var currTaxVal;
  var effectiveTaxRate;
  var brackets = [
      [9699, 0, .1],
      [39474, 9700, .12],
      [84199, 39475, .22],
      [160724, 84200, .24],
      [204099, 160725, .32],
      [510299, 204100, .35],
      [1000000, 510300, .37]
  ]

  try {
      for (var p = 0; p < brackets.length; p++) {
          if (withdrawal >= brackets[p][0]) {
              currTaxVal = (brackets[p][0] - brackets[p][1]) * brackets[p][2];
              effectiveTaxAmount += currTaxVal;
          } else if (withdrawal > brackets[p][1] && withdrawal < brackets[p][0]) {
              currTaxVal = (withdrawal - brackets[p][1]) * brackets[p][2]
              effectiveTaxAmount += currTaxVal
          }
      }
  } catch (e) {
      console.log("Calculate Tax Rate function", e)
  }
  effectiveTaxRate = (effectiveTaxAmount / withdrawal)
  return effectiveTaxRate
}

function getContribution(inflationRate, returnRate, annualContribution, startAge, retireAge) {
  var PreRetireYears = ((retireAge - startAge) <= 0) ? 0 : (retireAge - startAge);
  var ageArr = [];
  var inflationAdjustedByAgeArr = [];
  var sumInflationAdjustedByAgeArr = [];
  var currentSumInflationAdjusted = 0;
  var returnAdjustedByAgeArr = [];
  var sumReturnAdjustedByAgeArr = [];
  var currentSumReturnAdjusted = 0;
  var i;
  for (i = 1; i <= PreRetireYears; i++) {
      //********** Calculate Year-Adjusted Inflation and Net Gain Rates **********//
      // Year-adjusted Inflation and Net-Return Rates
      var netInflationRate = Math.pow((1 + inflationRate), i - 1);
      var netGainRate = Math.pow(1 + (returnRate - inflationRate), i);
      // Inflation-adjusted contribution
      var contribution = (annualContribution * netInflationRate);
      inflationAdjustedByAgeArr.push(contribution);
      currentSumInflationAdjusted += contribution;
      sumInflationAdjustedByAgeArr.push(currentSumInflationAdjusted);
      // Net-Return-Adjusted  Contribution
      var returnAdjustedContribution = (contribution * netGainRate);
      returnAdjustedByAgeArr.push((returnAdjustedContribution));
      currentSumReturnAdjusted += returnAdjustedContribution;
      sumReturnAdjustedByAgeArr.push(Math.round(currentSumReturnAdjusted));
      // age array
      ageArr.push(startAge + i);
  }
  return ({
      "age": ageArr,
      "baseContribute": annualContribution,
      "inflationAdjustedByYears": inflationAdjustedByAgeArr,
      "sumInflationAdjustedByYears": sumInflationAdjustedByAgeArr,
      "sumInflationAdjustedAtRetire": sumInflationAdjustedByAgeArr[PreRetireYears - 1],
      "sumAdjustedByYear": returnAdjustedByAgeArr,
      "sumReturnAdjustedByYears": sumReturnAdjustedByAgeArr,
      "sumReturnAdjustedAtRetire": sumReturnAdjustedByAgeArr[PreRetireYears],
      "length": PreRetireYears
  });
}

// Withdraw Yearly Nominal Income From Portfolio
function getNominalIncome(inflationRate, taxRate, annualWithdraw, startAge, retireAge, endAge) {
  var yearsBeforeRetire = ((retireAge - startAge) <= 0) ? 0 : (retireAge - startAge);
  var yearsAfterRetire = ((endAge - retireAge) <= 0) ? 0 : (endAge - retireAge);
  var ageArr = [];
  var nominalIncomeTakenArr = [];
  for (i = 1; i <= yearsAfterRetire; i++) {
      var withdraw = adjustWithdraw(inflationRate, taxRate, annualWithdraw, i + yearsBeforeRetire)
      nominalIncomeTakenArr.push(withdraw);
      ageArr.push(retireAge + i);
  }

  return ({
      "age": ageArr,
      "baseWithdraw": annualWithdraw,
      "taxInflationAdjustedByYears": nominalIncomeTakenArr,
      "length": yearsAfterRetire
  });
}

// Adjust Withdraw for Inflation and Tax for certain year period n.
function adjustWithdraw(inflationRate, taxRate, withdraw, n) {
  var preTaxWithdrawal = withdraw / (1 - taxRate);
  // Period-Adjusted Inflation Rates
  var netInflationRate = Math.pow((1 + inflationRate), n);
  // Curreny n period Pre-tax withdrawal
  preTaxWithdrawal = preTaxWithdrawal * netInflationRate;
  return preTaxWithdrawal;
}

// Calculate Yearly Portfolio values from Current Age to Expected Life Age
// Adjusted with contribution, withdrawl, inflation rate, and return rate data
// Note:  Contribution array should already be inflation and return adjusted.
// Note:  Each Contribution Element should be a sum of contribution returns of
//          current and previous years. (See function "getContribution")
// Note: Withdrawal array should alaready be inflation and tax adjusted
function getActualPace(
  inflationRate, returnRate, startBalance, endBalance, annualWithdraw,
  startAge, retireAge, endAge, sumContributeArr, withdrawArr
) {
  var paceArr = [];
  var ageArr = [];
  var currentPace;
  var i;
  var retireBalance;
  var riskText;
  var riskImage;

  // Begin Portfoliolio Pace with a starting balance
  paceArr.push(startBalance);
  ageArr.push(startAge);

  // Pre-Retirement Ages
  // Add Aggregated Contributions (now+past years) to Pre-retirement Portfoliolio Pace
  // Adjust starting balance with return and inflation rates per year i
  for (i = 0; i < sumContributeArr.length; i++) {
      currentPace = startBalance * Math.pow((1 + returnRate - inflationRate), i + 1);
      currentPace += sumContributeArr[i];
      paceArr.push(currentPace);
      ageArr.push(ageArr.length + startAge)
  }

  // Portfolio Balance At Retirement Age
  retireBalance = paceArr[paceArr.length - 1];

  // Post-Retirement to End Age
  // Deduct withdrawals to Post-Retirement Portfoliolio Pace
  // Adjust remaining balance with return and inflation rates per year i
  for (i = 0; i < withdrawArr.length; i++) {
      currentPace = (currentPace - withdrawArr[i]) * (1 + returnRate - inflationRate);
      paceArr.push(currentPace);
      ageArr.push(ageArr.length + startAge)
  }

  // Continue Withdrawing Until Negative Balance
  for (i = (paceArr.length + 1);
      (((i + startAge) - retireAge) < 20) && (currentPace > 0); i++) {
      var withdraw = adjustWithdraw(inflationRate, taxRate, annualWithdraw, i);
      currentPace = (currentPace - withdraw) * (1 + returnRate - inflationRate);
  }

  // calculate risk content
  // get age when pace turns to negative balance
  // insert file path root here
  i = (i + startAge) - retireAge
  console.log("count here",i)
  if (i >= 20) {
      riskText = "Very Low";
      riskImage = "images/VeryLowProb2x.png";
  } else if (i < 20 && i >= 5) {
      riskText = "Low";
      riskImage = "images/LowProb2x.png";
  } else if (i >= 0 && i < 5) {
      riskText = "Medium";
      riskImage = "images/MediumProb2x.png";
  } else if (i >= -5 && i < 0) {
      riskText = "High"
      riskImage = "images/HighProb2x.png";
  } else {
      riskText = "Very High"
      riskImage = "images/VeryHighProb2x.png";
  }

  return ({
      "age": ageArr,
      "pace": paceArr,
      "paceAtRetire": retireBalance,
      "riskLevelText": riskText,
      "riskLevelImg": riskImage
  });
}

// Get Minimum-Needed Pace to Reach Desired Portfolio At Death
function getMinimumPace(
  inflationRate, returnRate, nominalIncomeArr, startBalance,
  endAgeBalance, startAge, retireAge, endAge
) {
  var paceArr = [];
  var ageArr = [];
  var currPace;
  var retireBalance;
  var annualizedRate;
  var i;

  // Begin Portfoliolio Pace with a starting balance
  paceArr.push(endAgeBalance);
  ageArr.push(endAge);

  // POST-Retirement Needs
  for (i = endAge; i > retireAge; i--) {
      // Traverse Back Starting with Desired End Balance At Death
      currPace = nominalIncomeArr[endAge - i];
      currPace += paceArr[paceArr.length - 1] / (1 + returnRate - inflationRate);
      paceArr.push(currPace)
      ageArr.push(i);
  }

  // Minimum-Needed Balance at Retirement Age
  retireBalance = paceArr[paceArr.length - 1];

  // Required Annual Net Return Rate to reach retirement balance from initial balance
  annualizedRate = Math.pow((retireBalance / startBalance), (1 / (retireAge - startAge))) - 1;

  // PRE-Retirement Needs
  for (i = retireAge; i > startAge; i--) {
      // Traverse Back with Annualized Balance At Retirement
      currPace = paceArr[paceArr.length - 1] / (1 + annualizedRate)
      paceArr.push(currPace);
      ageArr.push(i);
  }

  paceArr = paceArr.map(function(val, index) {
      return Math.round(val)
  }).reverse();
  ageArr = ageArr.reverse();

  return ({
      "age": ageArr,
      "pace": paceArr,
      "paceAtRetire": retireBalance,
      "length": endAge - startAge + 1
  });
}

function calculateSavings(
  inflationRate, returnRate, startBalance, desiredEndBalance,
  annualContribute, annualWithdraw, startAge, retireAge, endAge
) {
  // Calculate federal tax rate here
  var taxRate = getTaxRate(annualWithdraw)

  // Calculate return and inflation adjusted contribution data
  var contributeData = getContribution(
      inflationRate, returnRate, annualContribute, startAge, retireAge
  );
  var sumContribution = contributeData["sumInflationAdjustedAtRetire"];

  // Calculate Nominal Income Taken (i.e. inflation/tax adjusted withdrawl per year)
  var withdrawData = getNominalIncome(
      inflationRate, taxRate, annualWithdraw, startAge, retireAge, endAge
  );

  // Calculate Actual Pace Data with Contribution, Withdrawl, and Net Return
  var actualPaceData = getActualPace(
      inflationRate, returnRate, startBalance, desiredEndBalance,
      annualWithdraw, startAge, retireAge, endAge,
      contributeData["sumReturnAdjustedByYears"],
      withdrawData["taxInflationAdjustedByYears"]
  );
  var realPortfolioAtRetire = actualPaceData["paceAtRetire"];

  // Calculate Minimum Pace Data with Desired End Balance, Withdraw, and Annualized Rate
  var minPaceData = getMinimumPace(
      inflationRate, returnRate, withdrawData["taxInflationAdjustedByYears"],
      startBalance, desiredEndBalance, startAge, retireAge, endAge
  );
  var neededPortfolioAtRetire = minPaceData["paceAtRetire"];

  //final calculations to format data for graphs object
  var growth = realPortfolioAtRetire - startBalance - sumContribution;
  return {
    "graph1": {
      "actualPace": actualPaceData["pace"],
      "minNeededPace": minPaceData["pace"]
    },
    "graph2": {
      "initialBalance": [startBalance, startBalance / realPortfolioAtRetire],
      "contributions": [sumContribution, sumContribution / realPortfolioAtRetire],
      "growth": [growth, growth / realPortfolioAtRetire]
    },
    "graph3": {
      "nominalIncomeTaken": withdrawData["taxInflationAdjustedByYears"],
      "realIncomeNeededEachYear": annualWithdraw,
    },
    "ageData": actualPaceData["age"],
    "riskLevelText": actualPaceData["riskLevelText"],
    "riskLevelImg": actualPaceData["riskLevelImg"],
    "retirementAge": retireAge,
    "actualAtRetire": realPortfolioAtRetire,
    "needAtRetire": neededPortfolioAtRetire,
    "actualDiffNeedAtRetire": Math.abs(realPortfolioAtRetire - neededPortfolioAtRetire),
    "isSurplus": ((realPortfolioAtRetire - neededPortfolioAtRetire) > 0 ? true : false)
  }
}


/****************************************************************************************************/
/*************************** Rendering Block *******************************/
/****************************************************************************************************/
$(document).ready(function() {

  /****************************************************************************************************/
  /*************************** Front End Get Inputs and Calculate *******************************/
  /****************************************************************************************************/
  // Set Cookie To Variable
  // var cookie = getCookie('calculator_query_string');

  // // Split cookie into array
  // if(cookie) {
  //     cookie = cookie.split(/[ \( \&\=\,ica,r,f,p,s| \)]+/);
  //     cookie = cookieBase.slice(1);
  //     console.log(cookie)
  // } else {
  //     console.log('no cookie found');
  // }

  // Assumptions
  var returnRate = .07;
  var inflationRate = 0.016;
  var startAge = 85;
  var retireAge = 75;
  var endAge = 95;
  var startBalance = 1000000;
  var annualContribute = 25000;
  var annualWithdraw = 80000;
  var desiredEndBalance = 250000;

  // Get User Retirement Inputs From Cookie
  /*
  var age = parseInt(cookie[0]);
  var retirementAge = parseInt(cookie[1]);
  var currentBalance = parseInt(cookie[2]);
  var annualWithdraw = parseInt(cookie[3]);
  var annualContribute = parseInt(cookie[4]);
  */

  // Main Retirement Calculation Output
  var retData = calculateSavings(
    inflationRate, returnRate, startBalance, desiredEndBalance,
    annualContribute, annualWithdraw, startAge, retireAge, endAge
  );
  console.log('My initial age is ' + startAge + ' I plan to cookie retire at ' + retireAge + ' My initial invest will be ' + startBalance + ' My annual cookie contribution will be ' + annualContribute);
  console.log(retData)
  // Create Money Format with wNumb library
  var MONEYFormat = wNumb({
    prefix: "$",
    negativeBefore: "-",
    thousand: ','
  });

  // Assign, Format, and Round Numbers
  var formatActual = FormatCurrencyKM(retData["actualAtRetire"]);
  var formatNeed = FormatCurrencyKM(retData["needAtRetire"]);
  var formatdDiff = FormatCurrencyKM(retData["actualDiffNeedAtRetire"]);

  var roundBalance = Math.round(retData["graph2"]["initialBalance"][0]);
  var roundBalancePercent = Math.round(retData["graph2"]["initialBalance"][1] * 100);
  var roundContribute = Math.round(retData["graph2"]["contributions"][0]);
  var roundContributePercent = Math.round(retData["graph2"]["contributions"][1] * 100);
  var roundGrowth = Math.round(retData["graph2"]["growth"][0]);
  var roundGrowthPercent = Math.round(retData["graph2"]["growth"][1] * 100);

  var ageArray = retData["ageData"];
  var actualPaceArray = retData["graph1"]["actualPace"];
  var minNeedPaceArray = retData["graph1"]["minNeededPace"];
  var actualPortfolioAtRetire = FormatCurrencyKM(parseFloat(retData["actualAtRetire"]));

  var needLineOrder = (retData["actualAtRetire"] < retData["needAtRetire"]) ? 1 : 0;
  var actualLineOrder = (needLineOrder == 1) ? 0 : 1;

/****************************************************************************************************/
/************************************* Front-End Rendering Texts ************************************/
/****************************************************************************************************/

  // Inject Risk Level and Gauge Meter Image for Desktop + Mobile
  document.getElementById("RiskLevelDesktop").innerHTML = retData["riskLevelText"];
  document.getElementById("RiskLevelMobile").innerHTML = retData["riskLevelText"];
  document.getElementById("RiskGaugeDesktop").src = retData["riskLevelImg"];
  document.getElementById("RiskGaugeMobile").src = retData["riskLevelImg"];

  // Inject Dollar Values for "What you have" and "what you need" at retirement
  // for desktop and mobile elements
  document.getElementById("portfolioAtX").innerHTML = formatActual;
  document.getElementById("portfolioAtXM").innerHTML = formatActual;
  document.getElementById("portfolioNeededAtRetirement").innerHTML = formatNeed;
  document.getElementById("portfolioNeededAtRetirementM").innerHTML = formatNeed;

  // Inject Have, Need, and Shortfall/Surplus Values
  document.getElementById("HaveValue").innerHTML = formatActual;
  document.getElementById("NeedValue").innerHTML = formatNeed;
  document.getElementById("DiffValue").innerHTML = formatdDiff;
  document.getElementById("DiffText").innerHTML = retData["isSurplus"] ? "Surplus" : "Shortfall";

  // Inject Initial Balance, Contributions, and Growth for doughnut values
  document.getElementById("doughnutBalance").innerHTML = MONEYFormat.to(roundBalance);
  document.getElementById("doughnutGrowth").innerHTML = MONEYFormat.to(roundGrowth);
  document.getElementById("doughnutContribution").innerHTML = MONEYFormat.to(roundContribute);roundGrowth

/****************************************************************************************************/
/********************************** Front-End Rendering Line Chart **********************************/
/****************************************************************************************************/
  // Inject Line Chart
  var lineContext = document.getElementById('TimelineChart').getContext('2d');
  window.lineChart = new Chart(lineContext, {
    type: 'line',
    data: {
      labels: ageArray,
      datasets: [{
        label: "What you'll need",
        data: minNeedPaceArray,
        backgroundColor: "rgba(77,127,113,100)",
        pointRadius: 0,
        fill: 'origin',
        showLine: true,
        order: needLineOrder,
      }, {
        label: "What you'll have",
        data: actualPaceArray,
        backgroundColor: "rgba(255,176,67,100)",
        pointRadius: 0,
        fill: 'origin',
        showLine: true,
        order: actualLineOrder,
      }]
    },
    options: {
      plugins: {
        datalabels: {
          display: false
        }
      },
      responsive: true,
      maintainAspectRatio: true,
      legend: {
        display: false
      },
      animation: {
          duration: 0
      },
      scales: {
        yAxes: [{
          ticks: {
            min: 0,
            //max: 2500000,
            beginAtZero: true,
            maxTicksLimit: 5,
            padding: 5,
            callback: function(value, index, values) {
              return FormatCurrencyKM(value);
            }
          },
          gridLines: {
            display: true,
            borderDashOffset: {
              display: true
            },
            drawTicks: false
          },
        }],
        xAxes: [{
          gridLines: {
            display: false
          },
          ticks: {
            min: 0,
            max: endAge,
            //beginAtZero: false,
            startValue: 45,
            step: 5,
            maxTicksLimit: 9
          },
          scaleLabel: {
            display: true,
            labelString: 'Age',
            fontSize: 16,
            fontStyle: 'bold'
          }
        }]
      },
      tooltips: {
        enabled: true,
        backgroundColor: '#fff',
        borderColor: '#000',
        borderWidth: '1',
        titleFontColor: '#000',
        bodyFontColor: '#000',
        mode: 'nearest',
        intersect: false,
        callbacks: {
          title: function(tooltipItems, data) {
            var tooltipItem = tooltipItems[0];
            return data.datasets[tooltipItem.datasetIndex].label + ": " + FormatCurrencyKM(tooltipItem.yLabel);
          },
          label: function(tooltipItem, data) {
            return "At Age " + tooltipItem.xLabel;
          }
        }
      }
    },
    plugins: [{
      afterDatasetsDraw: function (myChart) {
        if (myChart.tooltip._active && myChart.tooltip._active.length) {
          var activePoint = myChart.tooltip._active[0],
            ctx = myChart.ctx,
            y_axis = myChart.scales['y-axis-0'],
            x = activePoint.tooltipPosition().x,
            topY,
            bottomY;

          if (y_axis) {
            topY = y_axis.top,
            bottomY = y_axis.bottom;
          }

          // draw line
          ctx.save();
          ctx.beginPath();
          ctx.moveTo(x, topY);
          ctx.lineTo(x, bottomY);
          ctx.lineWidth = 3;
          ctx.strokeStyle = '#000000';
          ctx.pointBackgroundColor = '#000000';
          ctx.pointRadius = 20;
          ctx.pointHighlightFill = '#000000';
          ctx.pointStrokeColor = '#000000';
          ctx.stroke();
          ctx.restore();
        }
      },
    }],
  });

/****************************************************************************************************/
/******************************** Front-End Rendering Doughnut Chart ********************************/
/****************************************************************************************************/
  // Inject Doughnut Chart
  var doughnutContext = document.getElementById("chartDoughnut").getContext("2d");
  window.doughnutConfig = new Chart(doughnutContext, {
    type: 'doughnut',
    data: {
      labels: ['Initial Balance', 'Contributions', 'Growth'],
      datasets: [{
        data: [
          roundBalancePercent,
          roundContributePercent,
          roundGrowthPercent
        ],
        backgroundColor: [
          '#6C3B6F',
          '#4D7F71',
          '#FFA426'
        ],
        borderWidth: [
          1,
          1,
          1
        ],
        borderAlign: "inner",
        borderColor: [
          '#6C3B6F',
          '#4D7F71',
          '#FFA426'
        ],
      }],
    },
    options: {
      responsive: true,
      aspectRatio: 1,
      maintainAspectRatio: true,
      legend: {
        display: false
      },
      title: {
        display: false
      },
      tooltips: {
        enabled: false
      },
      animation: {
        animateScale: false,
        animateRotate: false
      },
      elements: {
        customCutout: true
      },
      layout: {
        padding: {
          left: 0,
          right: 0,
          top: 0,
          bottom: 0
        }
      },
      rotation: 5,
      plugins: {
        datalabels: {
          // Requires 3rd party plugin chartjs-plugin-datalabels
          color: '#fff',
          textAlign: 'center',
          font: {
            lineHeight: 1.6,
            weight: 'bold'
          },
          formatter: function(value, ctx) {
            return value + '%';
          }
        },
      }
    },
    plugins: [{
      // MR - adding center text on doughnut
      beforeDraw: function(doughnutConfig) {
        var width = doughnutConfig.chart.width;
        var height = doughnutConfig.chart.height;
        var ctx = doughnutConfig.chart.ctx;

        ctx.restore();
        var fontSize = (height / 150).toFixed(2);
        ctx.font = fontSize + "em sans-serif";
        ctx.textBaseline = "middle";
        ctx.fillStyle = "#333";
        var text = actualPortfolioAtRetire;
        var textX = Math.round((width - ctx.measureText(text).width) / 2);
        var textY = height / 2;

        ctx.fillText(text, textX, textY);
        ctx.save();
      }
    }, {
      //doughnut custom cutout percentage for contributions
      //https://github.com/chartjs/Chart.js/issues/6195
      // Change thickness of line for the data section of the
      // doughnut chart that displays the amount that is left to be raised. Accessing data[1] gets us the
      // correct data section of the doughnut we want to manipulate.
      // Bill Ahlbrandt - make the radius of data[0] 0.93*data[1] so it scales properly
      beforeDraw: function (doughnutConfig) {
        if (doughnutConfig.config.options.elements.customCutout !== undefined) {
          //doughnutConfig.getDatasetMeta(0).data[1]._view.innerRadius = 94;
          doughnutConfig.getDatasetMeta(0).data[0]._view.outerRadius = doughnutConfig.getDatasetMeta(0).data[1]._view.outerRadius * 0.93;
          doughnutConfig.getDatasetMeta(0).data[2]._view.outerRadius = doughnutConfig.getDatasetMeta(0).data[1]._view.outerRadius * 0.93;
          //doughnutConfig.getDatasetMeta(0).data[0]._view.outerRadius = 60;
          //doughnutConfig.getDatasetMeta(0).data[2]._view.outerRadius = 70;
          doughnutConfig.update();
        }
      }
    }],
  });

/****************************************************************************************************/
/*************************** Front-End Callbacks for Animation/Transition ***************************/
/****************************************************************************************************/
// Accordion Expand behavior for sections: Disclosures, About Us, More Resources
  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
      }
    });
  }

// Popover features for question-mark tooltips
// Dependency: jquery-2.2.4, bootstrap, popover
 $('[data-toggle="popover"]').popover({
        placement: 'bottom',
        boundary: 'viewport',
        container: 'body',
        html: true,
        content: function () {
            let content = $(this).attr("data-popover-content");
            return $(content).html();
        }
    });

  $('body').on('click', function(e) {
    $('[data-toggle="popover"]').each(function() {
      //$("div.overlay").toggleClass("on");
      if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
        $(this).popover('hide');
      //$("div.overlay").removeClass("on");
      }
    });
  });
});


