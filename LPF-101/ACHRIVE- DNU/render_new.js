"use strict";
var myChart;
var ReasonableRound = function (n) {
  var divCount = 0;
  while (n > 100) {
    n = n / 10;
    divCount++;
  }
  n = Math.round(n); // round to nearest integer
  while (divCount > 0) {
    n *= 10;
    divCount--;
  }
  return n;
}
function FormatCurrencyKM(val) {
  val = ReasonableRound(val);
  let suffix = "";
  let sign = "";
  if (val < 0) {
    sign = "-";
    val = -val;
  }
  if (val < 1000) { // do nothing   
  } else if (val < 1000000) {
    val = Math.round(val / 1000); // if less than 1M, get rid of decimals
    suffix = "K";
  } else if (val < 1000000000) {
    val = val / 1000000;
    suffix = "M";
  }
  return "$" + sign + val + suffix;
}
var GetMax = function (seriesData) {
  var maxVal = 0;
  $.each(seriesData, function (index, value) {
    if (value > maxVal) {
      maxVal = value;
    }
  });
  return maxVal;
}  

Chart.plugins.register({
  afterDatasetsDraw: function (myChart) {
    if (myChart.tooltip._active && myChart.tooltip._active.length) {
      var activePoint = myChart.tooltip._active[0],
          ctx = myChart.ctx,
          y_axis = myChart.scales['y-axis-0'],
          x = activePoint.tooltipPosition().x,
          topY,
          bottomY;

      if (y_axis) {
        topY = y_axis.top,
        bottomY = y_axis.bottom;
      }

      // draw line
      ctx.save();
      ctx.beginPath();
      ctx.moveTo(x, topY);
      ctx.lineTo(x, bottomY);
      ctx.lineWidth = 3;
      ctx.strokeStyle = '#000000';
      ctx.pointBackgroundColor = '#000000';
      ctx.pointRadius = 20;
      ctx.pointHighlightFill = '#000000';
      ctx.pointStrokeColor = '#000000';
      ctx.stroke();
      ctx.restore();
    }
  }
}); 

let TriggerMouseMove = function (c) {
var meta = c.getDatasetMeta(0);
var rect = c.canvas.getBoundingClientRect();
var retage = document.getElementById('retirementAge').value
c.data.labels.forEach(function (labelVal, idx) {
  if (labelVal == retage) {
    var point = meta.data[idx].getCenterPoint();
    var evt = new MouseEvent('mousemove', {
      clientX: rect.left + point.x,
      clientY: rect.top + point.y
    });
    var node = c.canvas;
    node.dispatchEvent(evt);
  }
});
};
$(document).ready(function () {
let retired = false;
let lifeExpectancy = document.getElementById('LifeExpectancy').value;      
if (retired) { // draw the inflation adjusted retirement income needed
  $(".factsToConsider").html("Here Are Some Facts to Consider");
  $(".projectedAssetsWorking").addClass("hidden"); // hide the Working Projected Assets
  $(".projectedAssetsRetired").removeClass("hidden");  // show the Retired Projected Assets
  $(".legendTextRetired").html("What you have"); // update text for Retired copy under main chart 
  $("#doughnutDiv").addClass("hidden"); // hide the doughnut
  $("#RetiredTimelineDiv").removeClass("hidden");  // show the chart
  var ctxx = document.getElementById("chartCanvasRetired").getContext("2d");
  window.myChart = new Chart(ctxx, {
    type: 'line',
    data: {
      labels: [40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95],
      datasets: [{
        label: 'Nominal Income Taken',
        data: [],
        backgroundColor: "#6C3B6F",
        borderColor: "#6C3B6F",
        pointRadius: 0,
        fill: 'none',
      },
      ],
    },
    options: {
      plugins: { datalabels: { display: false } },
      responsive: true,
      maintainAspectRatio: true,
      legend: { display: true, position: 'bottom', boxWidth: 20, },
      animation: { duration: 0 },


      scales: {
        yAxes: [{
          ticks: {
            min: 0,
            beginAtZero: true,
            maxTicksLimit: 5,
            padding: 5,
            callback: function (value, index, values) {
              return FormatCurrencyKM(value);
            }
          },
          gridLines: {
            display: true,
            borderDashOffset: { display: true },
            drawTicks: false
          },
        }],
        xAxes: [{
          gridLines: { display: false },
          ticks: {
            min: 0,
            max: lifeExpectancy,
            //beginAtZero: false,
            startValue: 45,
            step: 5,
            maxTicksLimit: 9
          },
          scaleLabel: {
            display: true,
            labelString: 'Age',
            fontSize: 16,
            fontStyle: 'bold'
          }
        }]
      },
      tooltips: {
        enabled: true,
        backgroundColor: '#fff',
        borderColor: '#000',
        borderWidth: '1',
        borderRadius: '1',
        titleFontColor: '#000',
        bodyFontColor: '#000',
        mode: 'nearest',
        axis: 'x',
        intersect: false,
        callbacks: {
          title: function (tooltipItems, data) {
            let tooltipItem = tooltipItems[0];
            return "Nominal Income: " + FormatCurrencyKM(tooltipItem.yLabel);;
          },
          label: function (tooltipItem, data) {
            return " At Age " + tooltipItem.xLabel;
          }
        }
      }
    }
  });
}  // end of if (!retired)
let whatYoullHaveLabelText = "What you'll have";
if (retired) {
  whatYoullHaveLabelText = "What you have";
}
var ctx = document.getElementById("chartCanvasWorking").getContext("2d");
window.myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: [40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95],
    datasets: [{
      label: "What you'll need",
      data: [500000,630219.85,794354.11,1001235.45,1261996.91,1590671.00,2004944.87,2527112.10,3185272.41,4014843.78,5060468.47,5098064.44,5133919.88,5167880.47,5199782.59,5229452.82,5256707.35,5281351.45,5303178.84,5321971.06,5337496.79,5349511.13,5357754.89,5361953.73,5361817.39,5357038.77,5347293.04,5332236.64,5311506.22,5284717.61,5251464.67,5211318.02,5163823.85,5108502.50,5044847.11,4972322.10,4890361.59,4798367.76,4695709.09,4581718.52,4455691.52,4316884.03,4164510.29,3997740.60,3815698.90,3617460.23,3402048.09,3168431.62,2915522.62,2642172.47,2347168.80,2029232.03,1687011.73,1319082.76,923941.18,500000],
      backgroundColor: "rgba(77,127,113,100)",
      pointRadius: 0,
      showLine: false,
    }, {
      label: whatYoullHaveLabelText,
      data: [500000,579700.00,664592.53,755020.97,851351.70,953975.67,1063310.03,1179800.00,1303920.65,1436179.00,1577116.21,1426611.15,1264208.11,1089204.27,900857.88,698386.17,480963.10,247717.01,-2271.86,-269973.98,-556413.28,-862670.08,-1189884.11,-1539257.77,-1912059.53,-2309627.50,-2733373.21,-3184785.60,-3665435.22,-4176978.65,-4721163.20,-5299831.75,-5914928.01,-6568501.96,-7262715.59,-7999848.99,-8782306.73,-9612624.65,-10493476.91,-11427683.53,-12418218.24,-13468216.86,-14580986.05,-15760012.53,-17008972.90,-18331743.85,-19732413.01,-21215290.38,-22784920.37,-24446094.44,-26203864.53,-28063557.10,-30030788.01,-32111478.17,-34311870.04,-36638545.02],
      backgroundColor: "rgba(255,176,67,100)",
      pointRadius: 0,
      fill: 'origin'
    }, {
      label: "What you'll need",
      data: [500000,630219.85,794354.11,1001235.45,1261996.91,1590671.00,2004944.87,2527112.10,3185272.41,4014843.78,5060468.47,5098064.44,5133919.88,5167880.47,5199782.59,5229452.82,5256707.35,5281351.45,5303178.84,5321971.06,5337496.79,5349511.13,5357754.89,5361953.73,5361817.39,5357038.77,5347293.04,5332236.64,5311506.22,5284717.61,5251464.67,5211318.02,5163823.85,5108502.50,5044847.11,4972322.10,4890361.59,4798367.76,4695709.09,4581718.52,4455691.52,4316884.03,4164510.29,3997740.60,3815698.90,3617460.23,3402048.09,3168431.62,2915522.62,2642172.47,2347168.80,2029232.03,1687011.73,1319082.76,923941.18,500000],
      backgroundColor: "rgba(77,127,113,100)",
      pointRadius: 0,
      fill: 'origin',
      showLine: true,
    },
    ],
  },
  options: {
    plugins: { datalabels: { display: false } },
    responsive: true,
    maintainAspectRatio: true,
    legend: { display: false },
    animation: { duration: 0 },


    scales: {
      yAxes: [{
        ticks: {
          min: 0,
          //max: 2500000,
          beginAtZero: true,
          maxTicksLimit: 5,
          padding: 5,
          callback: function (value, index, values) {
            return FormatCurrencyKM(value);
          }
        },
        gridLines: {
          display: true,
          borderDashOffset: { display: true },
          drawTicks: false
        },
      }],
      xAxes: [{
        gridLines: { display: false },
        ticks: {
          min: 0,
          max: lifeExpectancy,
          //beginAtZero: false,
          startValue: 45,
          step: 5,
          maxTicksLimit: 9
        },
        scaleLabel: {
          display: true,
          labelString: 'Age',
          fontSize: 16,
          fontStyle: 'bold'
        }
      }]
    },
    tooltips: {
      enabled: true,
      backgroundColor: '#fff',
      borderColor: '#000',
      borderWidth: '1',
      titleFontColor: '#000',
      bodyFontColor: '#000',
      mode: 'nearest',
      intersect: false,
      callbacks: {
        title: function (tooltipItems, data) {
          let tooltipItem = tooltipItems[0];
          return data.datasets[tooltipItem.datasetIndex].label + ": "+FormatCurrencyKM(tooltipItem.yLabel);
        },
        label: function (tooltipItem, data) {
          return "At Age " + tooltipItem.xLabel;
        }
      }
    }
  }
});
TriggerMouseMove(window.myChart);
});
  
$(document).ready(function () {
var ctx = document.getElementById("chartDoughnut").getContext("2d");
window.doughnutConfig = new Chart(ctx, {
  type: 'doughnut',
  plugins: 
    [{
      // MR - adding center text on doughnut
      beforeDraw: function (doughnutConfig) {
        let width = doughnutConfig.chart.width;
        let height = doughnutConfig.chart.height;
        let ctx = doughnutConfig.chart.ctx;

        ctx.restore();
          let fontSize = (height / 150).toFixed(2);
          ctx.font = fontSize + "em sans-serif";
          ctx.textBaseline = "middle";
          ctx.fillStyle = "#333";
          let text = FormatCurrencyKM(parseFloat("1577116.21"));
          let textX = Math.round((width - ctx.measureText(text).width) / 2);
          let textY = height / 2;

        ctx.fillText(text, textX, textY);
        ctx.save();
      }
    },
      {
      //doughnut custom cutout percentage for contributions
      //https://github.com/chartjs/Chart.js/issues/6195
      // Change thickness of line for the data section of the
      // doughnut chart that displays the amount that is left to be raised. Accessing data[1] gets us the
      // correct data section of the doughnut we want to manipulate.
      // Bill Ahlbrandt - make the radius of data[0] 0.93*data[1] so it scales properly
      beforeDraw: function (doughnutConfig) {
        if (doughnutConfig.config.options.elements.customCutout !== undefined) {
          //doughnutConfig.getDatasetMeta(0).data[1]._view.innerRadius = 94;
          doughnutConfig.getDatasetMeta(0).data[0]._view.outerRadius = doughnutConfig.getDatasetMeta(0).data[1]._view.outerRadius * 0.93;
          doughnutConfig.getDatasetMeta(0).data[2]._view.outerRadius = doughnutConfig.getDatasetMeta(0).data[1]._view.outerRadius * 0.93;
          //doughnutConfig.getDatasetMeta(0).data[0]._view.outerRadius = 60;
          //doughnutConfig.getDatasetMeta(0).data[2]._view.outerRadius = 70;
          doughnutConfig.update();
        }
      }
    }],
  data: {
    labels: ['Initial Balance', 'Contributions', 'Growth'],
    datasets: [{
      data: [
                      34,
                      34,
                      32
      ],
      backgroundColor: [
        '#FFA426',
        '#4D7F71',
        '#6C3B6F'
      ],
      borderWidth: [
        1,
        1,
        1
      ],
      borderAlign: "inner",
      borderColor: [
        '#FFA426',
        '#4D7F71',
        '#6C3B6F'
      ],
      weight: [
        '1',
        '1',
        '1'
      ],
    }]
  },
  options: {
    responsive: true,
    aspectRatio: 1,
    maintainAspectRatio: true,
    legend: {
      display: false
    },
    title: {
      display: false
    },
    tooltips: {
      enabled: false
    },
    animation: {
      animateScale: false,
      animateRotate: false
    },
    elements: {
      customCutout: true
    },
    //outerRadius: [80,100,80],
    //cutoutPercentage: 50,
    layout: {
      padding: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
      }
    },
    rotation: 5,
    plugins: {
      datalabels: {
        color: '#fff',
        textAlign: 'center',
        font: {
          lineHeight: 1.6,
          weight: 'bold'
        },
        formatter: function (value, ctx) {
          return value + '%';
        }
      },

    }
  }
});      
});
let showPopup = true;
function PopupForm1() {
if (showPopup) {
  $("#ContactFormModal").modal('show');
}
}

  function addCommas(x) {
      if (!x) {
          x = 0;  // default to 0
      }
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts[0]; // ignore the decimals
  }
//Portfolio Return Slider
  $(document).ready(function () {
  let sliderwNumb = wNumb({
    decimals: 2,
    suffix: '%'
  });
    var ror = document.getElementById('RateOfReturnPercent').value;
      // set the global slider field
      let ReturnRateValue = document.getElementById('ReturnRateValue');
      // set up the slider
      noUiSlider.create(
          ReturnRateValue, {
              start: 7,
              step: 0.25,
              connect: [true, false],
      tooltips: [true],
              range: {
                  min: 0,
                  max: 12
              },
              format: sliderwNumb,				  
          }
      );
      let valueDisplayReturnRate = document.getElementById("ReturnRateValue");
      ReturnRateValue.noUiSlider.on('update', function (values, handle) {
        valueDisplayReturnRate.value = values[handle];
        document.getElementById("RateOfReturnPercent").value = valueDisplayReturnRate.value;            
      });
      valueDisplayReturnRate.addEventListener('change', function () {
        ReturnRateValue.noUiSlider.set(this.value);
        document.getElementById("RateOfReturnPercent").value = valueDisplayReturnRate.value;            
      });       
      ReturnRateValue.noUiSlider.set(ror);  // set it to the value in the hidden field     
  });
//Inflation Return Slider
$(document).ready(function () {
    let sliderwNumb = wNumb({
        decimals: 2,
        suffix: '%'
    });
    var inflationToSet = document.getElementById('InflationRatePercent').value;
    // set the global slider field
    let InflationRateValue = document.getElementById('InflationRateValue');
    // set up the slider
    noUiSlider.create(
        InflationRateValue, {
        start: 2.0,
        step: 0.02,
        connect: [true, false],
        tooltips: [true],
        range: {
            min: 0,
            max: 6
        },
        format: sliderwNumb,
    }
    );
    let valueDisplayInflationRate = document.getElementById("InflationRateValue");
    InflationRateValue.noUiSlider.on('update', function (values, handle) {
      valueDisplayInflationRate.value = values[handle];
      document.getElementById("InflationRatePercent").value = valueDisplayInflationRate.value;
    });
    valueDisplayInflationRate.addEventListener('change', function () {
      InflationRateValue.noUiSlider.set(this.value);
      document.getElementById("InflationRatePercent").value = valueDisplayInflationRate.value;
    });
    InflationRateValue.noUiSlider.set(inflationToSet);  // set it to the value in the hidden field
});
    // Set up the Life Expectancy Slider
$(document).ready(function () {
    //var lifeExpectancyToSetHidden = document.getElementById('LifeExpectancy');
  if (showPopup) {
    var tenSeconds = 10000;
    window.setTimeout(PopupForm1, tenSeconds);
  }
    let lifeExpectancy = document.getElementById('LifeExpectancy').value;
    // set the global slider field
    let lifeExpectancyValue = document.getElementById('LifeExpectancyValue');  // LifeExpectancyValue is the div holding the slider
    // set up the slider
    noUiSlider.create(
      lifeExpectancyValue, {
        start: lifeExpectancy,
        step: 1,
        connect: [true, false],
        tooltips: [true],
        range: {
          min: 65,
          max: 120
        },
        format: wNumb({
          decimals: 0,
          thousand: ','
        })
      }
    );        
      let valueDisplayLifeExpectancy = document.getElementById("LifeExpectancyValue");
      LifeExpectancyValue.noUiSlider.on('update', function (values, handle) {
        valueDisplayLifeExpectancy.value = values[handle];
        document.getElementById("LifeExpectancy").value = valueDisplayLifeExpectancy.value;
      });
      valueDisplayLifeExpectancy.addEventListener('change', function () {
        LifeExpectancyValue.noUiSlider.set(this.value);
        document.getElementById("LifeExpectancy").value = valueDisplayLifeExpectancy.value;
      });
      LifeExpectancyValue.noUiSlider.set(lifeExpectancy);  // set it to the value in the hidden field
  });