"use strict";
var formatCurrencyENUS = function (inputString) {
  if (typeof inputString === 'number' && isNaN(inputString)) {
    return '';
  }
  if (!inputString) {
    return '';
  }
  let input = inputString;
  if (typeof input !== 'number') {
    input = input.replace(/[\D\s\._\-]+/g, "");
    if (input === '') {
      return '';
    }
    input = input ? parseInt(input, 10) : 0;
  }
  return "$" + input.toLocaleString("en-US");
};
var unformatCurrencyENUS = function (inputString) {
  if (typeof inputString === 'number') {
    return inputString;
  }
  if (!inputString) {
    return inputString;
  }
  let input = inputString;
  if (typeof input !== 'number') {
    input = input.toUpperCase();
    let multiplier = 1;
    if (input.endsWith('K')) {
      multiplier = 1000;
      input = input.substring(0, input.length - 1);
    } else if (input.endsWith('M')) {
      multiplier = 1000000;
      input = input.substring(0, input.length - 1);
    }
    input = input.replace(/[\D\s\._\-]+/g, "");
    if (input === '') {
      return '';
    }
    input = input ? parseInt(input, 10) : 0;
    input = input * multiplier;
    return input;
  }
};
var formatCurrencyField = function (event) {
  // When user select text in the document, also abort.
  var selection = window.getSelection().toString();
  if (selection !== '') {
    return;
  }

  // When the arrow keys are pressed, abort.
  if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
    return;
  }

  var $this = $(this);

  // Get the value.
  var input = $this.val();
  if (input === '') {
    // leave it alone.  Get out of here
    return;
  }
  let formattedAsCurrency = formatCurrencyENUS(input);

  $this.val(formattedAsCurrency);
};
var unformatCurrencyField = function (event) {
  // When user tabs into a field, unformat it
  var $this = $(this);

  // Get the value.
  var input = $this.val();
  if (input === '') {
    // leave it alone.  Get out of here
    return;
  }
  let unformattedCurrency = unformatCurrencyENUS(input);

  $this.val(unformattedCurrency);
};
var formatPercentageField = function (event) {
  // When user select text in the document, also abort.
  var selection = window.getSelection().toString();
  if (selection !== '') {
    return;
  }

  // When the arrow keys are pressed, abort.
  if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
    return;
  }

  var $this = $(this);

  // Get the value.
  let inputValue = $this.val();

  let input = inputValue.replace(/[\s_\%\-]+/g, "");
  if (input === "") {
    return input;
  }
  input = input ? parseFloat(input) : 0;

  $this.val(function () {
    return input.toString() + "%";
  });
};
var sanitizePercentageField = function (f) {
  var clean = [];
  for (var i = 0; i < f.length; i++) {
    var c = f[i];
    if ((c >= '0' && c <= '9')||c==='.') {
      clean.push(c);
    }
  }
  var newClean = clean.join("");
  return parseFloat(newClean);
};

var unformatPercentageField = function (event) {
  // When user tabs into a field, unformat it
  var $this = $(this);

  // Get the value.
  var input = $this.val();
  if (input === '') {
    // leave it alone.  Get out of here
    return;
  }
  let unformattedPercentage = sanitizePercentageField(input);

  $this.val(unformattedPercentage);
};

var sanitizeCurrencyField = function (f) {
  var clean = [];
  for (var i = 0; i < f.length; i++) {
    var c = f[i];
    if (c >= '0' && c <= '9') {
      clean.push(c);
    } else if (c === '.') {
      break; // get out at the decimal point
    }
  }
  var newClean = clean.join("");
  return parseInt(newClean);
};
