  "use strict";
var myChart;
var ReasonableRound = function (n) {
  var divCount = 0;
  while (n > 100) {
    n = n / 10;
    divCount++;
  }
  n = Math.round(n); // round to nearest integer
  while (divCount > 0) {
    n *= 10;
    divCount--;
  }
  return n;
}
function FormatCurrencyKM(val) {
  val = ReasonableRound(val);
  let suffix = "";
  let sign = "";
  if (val < 0) {
    sign = "-";
    val = -val;
  }
  if (val < 1000) { // do nothing   
  } else if (val < 1000000) {
    val = Math.round(val / 1000); // if less than 1M, get rid of decimals
    suffix = "K";
  } else if (val < 1000000000) {
    val = val / 1000000;
    suffix = "M";
  }
  return "$" + sign + val + suffix;
}
var GetMax = function (seriesData) {
  var maxVal = 0;
  $.each(seriesData, function (index, value) {
    if (value > maxVal) {
      maxVal = value;
    }
  });
  return maxVal;
}  

Chart.plugins.register({
          afterDatasetsDraw: function (myChart) {
              if (myChart.tooltip._active && myChart.tooltip._active.length) {
                  var activePoint = myChart.tooltip._active[0],
                      ctx = myChart.ctx,
                      y_axis = myChart.scales['y-axis-0'],
                      x = activePoint.tooltipPosition().x,
                      topY,
                      bottomY;

                  if (y_axis) {
                      topY = y_axis.top,
                      bottomY = y_axis.bottom;
                  }

                  // draw line
                  ctx.save();
                  ctx.beginPath();
                  ctx.moveTo(x, topY);
                  ctx.lineTo(x, bottomY);
                  ctx.lineWidth = 3;
                  ctx.strokeStyle = '#000000';
        ctx.pointBackgroundColor = '#000000';
        ctx.pointRadius = 20;
        ctx.pointHighlightFill = '#000000';
        ctx.pointStrokeColor = '#000000';
        ctx.stroke();
                  ctx.restore();
              }
          }
    }); 

let TriggerMouseMove = function (c) {
var meta = c.getDatasetMeta(0);
var rect = c.canvas.getBoundingClientRect();
var retage = document.getElementById('retirementAge').value
c.data.labels.forEach(function (labelVal, idx) {
  if (labelVal == retage) {
    var point = meta.data[idx].getCenterPoint();
    var evt = new MouseEvent('mousemove', {
      clientX: rect.left + point.x,
      clientY: rect.top + point.y
    });
    var node = c.canvas;
    node.dispatchEvent(evt);
  }
});
};
$(document).ready(function () {
let retired = false;
let lifeExpectancy = document.getElementById('LifeExpectancy').value;      
if (retired) { // draw the inflation adjusted retirement income needed
  $(".factsToConsider").html("Here Are Some Facts to Consider");
  $(".projectedAssetsWorking").addClass("hidden"); // hide the Working Projected Assets
  $(".projectedAssetsRetired").removeClass("hidden");  // show the Retired Projected Assets
  $(".legendTextRetired").html("What you have"); // update text for Retired copy under main chart 
  $("#doughnutDiv").addClass("hidden"); // hide the doughnut
  $("#RetiredTimelineDiv").removeClass("hidden");  // show the chart
  var ctxx = document.getElementById("chartCanvasRetired").getContext("2d");
  window.myChart = new Chart(ctxx, {
    type: 'line',
    data: {
      labels: [40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95],
      datasets: [{
        label: 'Nominal Income Taken',
        data: [],
        backgroundColor: "#6C3B6F",
        borderColor: "#6C3B6F",
        pointRadius: 0,
        fill: 'none',
      },
      ],
    },
    options: {
      plugins: { datalabels: { display: false } },
      responsive: true,
      maintainAspectRatio: true,
      legend: { display: true, position: 'bottom', boxWidth: 20, },
      animation: { duration: 0 },


      scales: {
        yAxes: [{
          ticks: {
            min: 0,
            beginAtZero: true,
            maxTicksLimit: 5,
            padding: 5,
            callback: function (value, index, values) {
              return FormatCurrencyKM(value);
            }
          },
          gridLines: {
            display: true,
            borderDashOffset: { display: true },
            drawTicks: false
          },
        }],
        xAxes: [{
          gridLines: { display: false },
          ticks: {
            min: 0,
            max: lifeExpectancy,
            //beginAtZero: false,
            startValue: 45,
            step: 5,
            maxTicksLimit: 9
          },
          scaleLabel: {
            display: true,
            labelString: 'Age',
            fontSize: 16,
            fontStyle: 'bold'
          }
        }]
      },
      tooltips: {
        enabled: true,
        backgroundColor: '#fff',
        borderColor: '#000',
        borderWidth: '1',
        borderRadius: '1',
        titleFontColor: '#000',
        bodyFontColor: '#000',
        mode: 'nearest',
        axis: 'x',
        intersect: false,
        callbacks: {
          title: function (tooltipItems, data) {
            let tooltipItem = tooltipItems[0];
            return "Nominal Income: " + FormatCurrencyKM(tooltipItem.yLabel);;
          },
          label: function (tooltipItem, data) {
            return " At Age " + tooltipItem.xLabel;
          }
        }
      }
    }
  });
}  // end of if (!retired)
let whatYoullHaveLabelText = "What you'll have";
if (retired) {
  whatYoullHaveLabelText = "What you have";
}
var ctx = document.getElementById("chartCanvasWorking").getContext("2d");
window.myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: [40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95],
    datasets: [{
      label: "What you'll need",
      data: [500000,630219.85,794354.11,1001235.45,1261996.91,1590671.00,2004944.87,2527112.10,3185272.41,4014843.78,5060468.47,5098064.44,5133919.88,5167880.47,5199782.59,5229452.82,5256707.35,5281351.45,5303178.84,5321971.06,5337496.79,5349511.13,5357754.89,5361953.73,5361817.39,5357038.77,5347293.04,5332236.64,5311506.22,5284717.61,5251464.67,5211318.02,5163823.85,5108502.50,5044847.11,4972322.10,4890361.59,4798367.76,4695709.09,4581718.52,4455691.52,4316884.03,4164510.29,3997740.60,3815698.90,3617460.23,3402048.09,3168431.62,2915522.62,2642172.47,2347168.80,2029232.03,1687011.73,1319082.76,923941.18,500000],
      backgroundColor: "rgba(77,127,113,100)",
      pointRadius: 0,
      showLine: false,
    }, {
      label: whatYoullHaveLabelText,
      data: [500000,579700.00,664592.53,755020.97,851351.70,953975.67,1063310.03,1179800.00,1303920.65,1436179.00,1577116.21,1426611.15,1264208.11,1089204.27,900857.88,698386.17,480963.10,247717.01,-2271.86,-269973.98,-556413.28,-862670.08,-1189884.11,-1539257.77,-1912059.53,-2309627.50,-2733373.21,-3184785.60,-3665435.22,-4176978.65,-4721163.20,-5299831.75,-5914928.01,-6568501.96,-7262715.59,-7999848.99,-8782306.73,-9612624.65,-10493476.91,-11427683.53,-12418218.24,-13468216.86,-14580986.05,-15760012.53,-17008972.90,-18331743.85,-19732413.01,-21215290.38,-22784920.37,-24446094.44,-26203864.53,-28063557.10,-30030788.01,-32111478.17,-34311870.04,-36638545.02],
      backgroundColor: "rgba(255,176,67,100)",
      pointRadius: 0,
      fill: 'origin'
    }, {
      label: "What you'll need",
      data: [500000,630219.85,794354.11,1001235.45,1261996.91,1590671.00,2004944.87,2527112.10,3185272.41,4014843.78,5060468.47,5098064.44,5133919.88,5167880.47,5199782.59,5229452.82,5256707.35,5281351.45,5303178.84,5321971.06,5337496.79,5349511.13,5357754.89,5361953.73,5361817.39,5357038.77,5347293.04,5332236.64,5311506.22,5284717.61,5251464.67,5211318.02,5163823.85,5108502.50,5044847.11,4972322.10,4890361.59,4798367.76,4695709.09,4581718.52,4455691.52,4316884.03,4164510.29,3997740.60,3815698.90,3617460.23,3402048.09,3168431.62,2915522.62,2642172.47,2347168.80,2029232.03,1687011.73,1319082.76,923941.18,500000],
      backgroundColor: "rgba(77,127,113,100)",
      pointRadius: 0,
      fill: 'origin',
      showLine: true,
    },
    ],
  },
  options: {
    plugins: { datalabels: { display: false } },
    responsive: true,
    maintainAspectRatio: true,
    legend: { display: false },
    animation: { duration: 0 },


    scales: {
      yAxes: [{
        ticks: {
          min: 0,
          //max: 2500000,
          beginAtZero: true,
          maxTicksLimit: 5,
          padding: 5,
          callback: function (value, index, values) {
            return FormatCurrencyKM(value);
          }
        },
        gridLines: {
          display: true,
          borderDashOffset: { display: true },
          drawTicks: false
        },
      }],
      xAxes: [{
        gridLines: { display: false },
        ticks: {
          min: 0,
          max: lifeExpectancy,
          //beginAtZero: false,
          startValue: 45,
          step: 5,
          maxTicksLimit: 9
        },
        scaleLabel: {
          display: true,
          labelString: 'Age',
          fontSize: 16,
          fontStyle: 'bold'
        }
      }]
    },
    tooltips: {
      enabled: true,
      backgroundColor: '#fff',
      borderColor: '#000',
      borderWidth: '1',
      titleFontColor: '#000',
      bodyFontColor: '#000',
      mode: 'nearest',
      intersect: false,
      callbacks: {
        title: function (tooltipItems, data) {
          let tooltipItem = tooltipItems[0];
          return data.datasets[tooltipItem.datasetIndex].label + ": "+FormatCurrencyKM(tooltipItem.yLabel);
        },
        label: function (tooltipItem, data) {
          return "At Age " + tooltipItem.xLabel;
        }
      }
    }
  }
});
TriggerMouseMove(window.myChart);
});
  
$(document).ready(function () {
var ctx = document.getElementById("chartDoughnut").getContext("2d");
window.doughnutConfig = new Chart(ctx, {
  type: 'doughnut',
  plugins: 
    [{
      // MR - adding center text on doughnut
      beforeDraw: function (doughnutConfig) {
        let width = doughnutConfig.chart.width;
        let height = doughnutConfig.chart.height;
        let ctx = doughnutConfig.chart.ctx;

        ctx.restore();
          let fontSize = (height / 150).toFixed(2);
          ctx.font = fontSize + "em sans-serif";
          ctx.textBaseline = "middle";
          ctx.fillStyle = "#333";
          let text = FormatCurrencyKM(parseFloat("1577116.21"));
          let textX = Math.round((width - ctx.measureText(text).width) / 2);
          let textY = height / 2;

        ctx.fillText(text, textX, textY);
        ctx.save();
      }
    },
      {
      //doughnut custom cutout percentage for contributions
      //https://github.com/chartjs/Chart.js/issues/6195
      // Change thickness of line for the data section of the
      // doughnut chart that displays the amount that is left to be raised. Accessing data[1] gets us the
      // correct data section of the doughnut we want to manipulate.
      // Bill Ahlbrandt - make the radius of data[0] 0.93*data[1] so it scales properly
      beforeDraw: function (doughnutConfig) {
        if (doughnutConfig.config.options.elements.customCutout !== undefined) {
          //doughnutConfig.getDatasetMeta(0).data[1]._view.innerRadius = 94;
          doughnutConfig.getDatasetMeta(0).data[0]._view.outerRadius = doughnutConfig.getDatasetMeta(0).data[1]._view.outerRadius * 0.93;
          doughnutConfig.getDatasetMeta(0).data[2]._view.outerRadius = doughnutConfig.getDatasetMeta(0).data[1]._view.outerRadius * 0.93;
          //doughnutConfig.getDatasetMeta(0).data[0]._view.outerRadius = 60;
          //doughnutConfig.getDatasetMeta(0).data[2]._view.outerRadius = 70;
          doughnutConfig.update();
        }
      }
    }],
  data: {
    labels: ['Initial Balance', 'Contributions', 'Growth'],
    datasets: [{
      data: [
                      34,
                      34,
                      32
      ],
      backgroundColor: [
        '#FFA426',
        '#4D7F71',
        '#6C3B6F'
      ],
      borderWidth: [
        1,
        1,
        1
      ],
      borderAlign: "inner",
      borderColor: [
        '#FFA426',
        '#4D7F71',
        '#6C3B6F'
      ],
      weight: [
        '1',
        '1',
        '1'
      ],
    }]
  },
  options: {
    responsive: true,
    aspectRatio: 1,
    maintainAspectRatio: true,
    legend: {
      display: false
    },
    title: {
      display: false
    },
    tooltips: {
      enabled: false
    },
    animation: {
      animateScale: false,
      animateRotate: false
    },
    elements: {
      customCutout: true
    },
    //outerRadius: [80,100,80],
    //cutoutPercentage: 50,
    layout: {
      padding: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
      }
    },
    rotation: 5,
    plugins: {
      datalabels: {
        color: '#fff',
        textAlign: 'center',
        font: {
          lineHeight: 1.6,
          weight: 'bold'
        },
        formatter: function (value, ctx) {
          return value + '%';
        }
      },

    }
  }
});      
});
let showPopup = false;
function PopupForm1() {
if (showPopup) {
  $("#ContactFormModal").modal('show');
}
}

  function addCommas(x) {
      if (!x) {
          x = 0;  // default to 0
      }
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts[0]; // ignore the decimals
  }
//Portfolio Return Slider
  $(document).ready(function () {
  let sliderwNumb = wNumb({
    decimals: 2,
    suffix: '%'
  });
    var ror = document.getElementById('RateOfReturnPercent').value;
      // set the global slider field
      let ReturnRateValue = document.getElementById('ReturnRateValue');
      // set up the slider
      noUiSlider.create(
          ReturnRateValue, {
              start: 7,
              step: 0.25,
              connect: [true, false],
      tooltips: [true],
              range: {
                  min: 0,
                  max: 12
              },
              format: sliderwNumb,				  
          }
      );
      let valueDisplayReturnRate = document.getElementById("ReturnRateValue");
      ReturnRateValue.noUiSlider.on('update', function (values, handle) {
        valueDisplayReturnRate.value = values[handle];
        document.getElementById("RateOfReturnPercent").value = valueDisplayReturnRate.value;            
      });
      valueDisplayReturnRate.addEventListener('change', function () {
        ReturnRateValue.noUiSlider.set(this.value);
        document.getElementById("RateOfReturnPercent").value = valueDisplayReturnRate.value;            
      });       
      ReturnRateValue.noUiSlider.set(ror);  // set it to the value in the hidden field     
  });
//Inflation Return Slider
$(document).ready(function () {
    let sliderwNumb = wNumb({
        decimals: 2,
        suffix: '%'
    });
    var inflationToSet = document.getElementById('InflationRatePercent').value;
    // set the global slider field
    let InflationRateValue = document.getElementById('InflationRateValue');
    // set up the slider
    noUiSlider.create(
        InflationRateValue, {
        start: 2.0,
        step: 0.02,
        connect: [true, false],
        tooltips: [true],
        range: {
            min: 0,
            max: 6
        },
        format: sliderwNumb,
    }
    );
    let valueDisplayInflationRate = document.getElementById("InflationRateValue");
    InflationRateValue.noUiSlider.on('update', function (values, handle) {
      valueDisplayInflationRate.value = values[handle];
      document.getElementById("InflationRatePercent").value = valueDisplayInflationRate.value;
    });
    valueDisplayInflationRate.addEventListener('change', function () {
      InflationRateValue.noUiSlider.set(this.value);
      document.getElementById("InflationRatePercent").value = valueDisplayInflationRate.value;
    });
    InflationRateValue.noUiSlider.set(inflationToSet);  // set it to the value in the hidden field
});
    // Set up the Life Expectancy Slider
$(document).ready(function () {
    //var lifeExpectancyToSetHidden = document.getElementById('LifeExpectancy');
  if (showPopup) {
    var tenSeconds = 10000;
    window.setTimeout(PopupForm1, tenSeconds);
  }
    let lifeExpectancy = document.getElementById('LifeExpectancy').value;
    // set the global slider field
    let lifeExpectancyValue = document.getElementById('LifeExpectancyValue');  // LifeExpectancyValue is the div holding the slider
    // set up the slider
    noUiSlider.create(
      lifeExpectancyValue, {
        start: lifeExpectancy,
        step: 1,
        connect: [true, false],
        tooltips: [true],
        range: {
          min: 65,
          max: 120
        },
        format: wNumb({
          decimals: 0,
          thousand: ','
        })
      }
    );        
      let valueDisplayLifeExpectancy = document.getElementById("LifeExpectancyValue");
      LifeExpectancyValue.noUiSlider.on('update', function (values, handle) {
        valueDisplayLifeExpectancy.value = values[handle];
        document.getElementById("LifeExpectancy").value = valueDisplayLifeExpectancy.value;
      });
      valueDisplayLifeExpectancy.addEventListener('change', function () {
        LifeExpectancyValue.noUiSlider.set(this.value);
        document.getElementById("LifeExpectancy").value = valueDisplayLifeExpectancy.value;
      });
      LifeExpectancyValue.noUiSlider.set(lifeExpectancy);  // set it to the value in the hidden field
  });
  
  
  
  
//v.9/214/2020
/* Outputs Numbers like 1400000 as 1.4M */
function nFormatter(num) {
    if (num >= 1000000000) {
        return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
    }
    if (num >= 1000000) {
        return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
    }
    if (num >= 1000) {
        return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
    }
    return num;
}

/* Outputs Numbers like 1400000 as 1,400,000 */
function toCommas(value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

//Start Of Calculations

// Grab Cookie
// function getCookie(cname) {
//     var name = cname + "=";
//     var ca = document.cookie.split(';');
//     for(var i = 0; i < ca.length; i++) {
//       var c = ca[i];
//       while (c.charAt(0) == ' ') {
//         c = c.substring(1);
//       }
//       if (c.indexOf(name) == 0) {
//         return c.substring(name.length, c.length);
//       }
//     }
//     return "";
// }

// function setCookie(name,value,minutes) {
//     var expires = "";
//     if (minutes) {
//         var date = new Date();
//         var minutes = 30;
//         date.setTime(date.getTime() + (minutes * 60 * 1000));
//         expires = "; expires=" + date.toUTCString();
//     }
//     document.cookie = name + "=" + (value || "")  + expires + "; path=/";
// }

// //Set Cookie To Variable
// var cookie = getCookie('calculator_query_string');

// // Demo If Statement to split into array
// if(cookie) {
//     var cookieBase = cookie.split(/[ \( \&\=\,ica,r,f,p,s| \)]+/);
//     var cookieFinal = cookieBase.slice(1);
//     console.log(cookieFinal)
// } else {
//     console.log('no cookie found');
// }

//Variables need to be set to cookie values. Zaw
//Investment Situation and Goal Variables
var age = 55;
var retirementAge = 75;
var initialBalance = 1000000;
var contributionValue = 25000;
var annualWithdrawal = 80000;
var wantedTotalAmountAtLastAge =250000.00;
var estimatedTaxRate;

var investmentRoR = .07;
var inflation = 0.0224;
var lifeExpectancy = 95;

// Age-related Variables
var ageData, savingsData, valueAddsData;
var baseAgeValue = (1 + age) - age;
var baseAgeDifference = retirementAge - age;
baseAgeDifference = (baseAgeDifference <= 0) ? 0 : baseAgeDifference  


console.log('My initial cookie age is '+age+' I plan to cookie retire at '+retirementAge+' My initial cookie invest will be '+initialBalance+' My annual cookie contribution will be '+contributionValue);

function calculateSavings(rateOfReturn) {
    ageData = [];
    savingsData = [];
    valueAddsData = [];
    var paceData = [initialBalance];
    var yearsInvested = 0;
    var yearCounter = 1;
    //Daryl Change here V2
    var contributions = [contributionValue];
    var valueOfAdds_preRetire = []
    var finalInvestment
    var nominalIncomeTaken = [annualWithdrawal];
    var contributionsValueAtRetirement = 0;
    var maxInvestmentVal = 0;
    var riskContent = "Very Low"
    var ageBeforeUnderWantedAmount = 0;
    var neededMaxAmountAtRetirement  = 0;



    function calculateTaxRate(){
        if(!estimatedTaxRate){
            var totalEffectiveTax = 0;
            var currTaxVal;
            var totalEffectiveTaxRate;
            var brackets = [
                [9699,0,.1],
                [39474,9700,.12],
                [84199,39475,.22],
                [160724,84200,.24],
                [204099,160725,.32],
                [510299,204100,.35],
                [1000000,510300,.37]
            ]

            try{
                for(var p = 0; p < brackets.length;p++){
                    if(annualWithdrawal >= brackets[p][0]){
                        currTaxVal = (brackets[p][0] - brackets[p][1]) * brackets[p][2];
                        totalEffectiveTax += currTaxVal;
                    } else if(annualWithdrawal > brackets[p][1] && annualWithdrawal < brackets[p][0]){
                        currTaxVal = (annualWithdrawal - brackets[p][1]) * brackets[p][2]
                        totalEffectiveTax += currTaxVal
                    }
                }
            } catch(e){
                console.log("Calculate Tax Rate function", e)
            }
            totalEffectiveTaxRate = (totalEffectiveTax/annualWithdrawal)
            estimatedTaxRate = totalEffectiveTaxRate
        }
    }
    //calculate tax rate here
    calculateTaxRate()

    for(i = age; i <= lifeExpectancy; i++) {
        ageData.push(i);
        //Calculate Contribution
        var ageDifference = (i+1) - age;
        var inflationTotal = Math.pow(1+(investmentRoR - inflation),baseAgeDifference-(baseAgeDifference- yearCounter))
        var savingsIncrease = (contributions[0] * inflationTotal);
        savingsData.push(savingsIncrease);
        //Calculate Value Of Adds
        if(yearCounter <= baseAgeDifference) {
            //custom by daryl
            var currentContribution = contributions[0] * Math.pow((1+inflation),yearCounter)
            var current_preRetirementValueAdd;

            if(i === age){
                //For value add
                current_preRetirementValueAdd  = contributions[0] * inflationTotal
            } else {
                //For value add
                current_preRetirementValueAdd = contributions[contributions.length - 1] * inflationTotal
            }
            contributions.push(currentContribution)
            valueOfAdds_preRetire.push(current_preRetirementValueAdd)


            yearCounter++;
        }
        yearsInvested++;

        //for nominal income needed
        if(i <= lifeExpectancy - baseAgeDifference){
            currentNominalIncomeValue = nominalIncomeTaken[nominalIncomeTaken.length -1] * (1 + inflation)
            nominalIncomeTaken.push(currentNominalIncomeValue)
        }
    }
    nominalIncomeTaken = nominalIncomeTaken.map(function(val, index){
        return val = Math.round(val)
    })
    //remove unneeded last contribution
    contributions.pop()
    nominalIncomeTaken.pop()

    contributions.forEach(function(val, index){
        contributionsValueAtRetirement += val
    })
    recontributionValue = contributionValue

    // Calculate Pace


    function calculateActualRetirement() {
        var total = valueAddsData[0];

        var yearCounter = 1;
        var paceCalcArray = [];
        var yearsAfterRetirement = lifeExpectancy - retirementAge;
        var yearsInvestedPace = 0;
        var valueAddsTotal = 0;
        var currPacePreRet;


        //preretire
        for(var i = 0; i < baseAgeDifference; i++) {
            valueAddsTotal += valueOfAdds_preRetire[i]
            currPacePreRet = valueAddsTotal+initialBalance * Math.pow((1+investmentRoR - inflation), (i + 1));
            paceData.push(currPacePreRet)
        }
        maxInvestmentVal = paceData[paceData.length -1]

        // Caluclate Final Pace Values and Start Annual Withdrawal
        // (final investment - (withdrawl/1-estimated tax rate) *

        finalInvestment = paceData.slice(-1)[0];
        paceCalcArray.push(finalInvestment)
        //console.log("this is final investment:",paceCalcArray)
        for(var l = 1; l <= yearsAfterRetirement; l++) {
            //...............Used for final input in graph data.............//
            finalInvestment = (finalInvestment - ((annualWithdrawal/(1- estimatedTaxRate)) * Math.pow((1+inflation),((retirementAge + l) - Math.min(age, retirementAge))))) * (1 + investmentRoR -inflation)
            finalInvestmentFormatted = finalInvestment;

            paceCalcArray.push(finalInvestmentFormatted);
            paceData.push(finalInvestmentFormatted);

        }
        console.log("Years Before Retirement: " + baseAgeDifference)
        console.log("Years After Retiremnet: " + yearsAfterRetirement);

        return paceData;
    }

    //Daryl change here

    //Calculate minimum needed retirement account
    function calculateNeedsPreAndPostRetire(){

      //calculating post retire account value
      var postValueAccount = [wantedTotalAmountAtLastAge];
      var retirementAccountNeeds = [initialBalance];
      var annualizedRate;
      var preretireVal;
      for(var l = lifeExpectancy; l > retirementAge;l--){
          var currVal = (((annualWithdrawal /(1 - estimatedTaxRate)) * Math.pow((1 + inflation), (l - Math.min(age, retirementAge)))) + (postValueAccount[postValueAccount.length -1]/(1+ investmentRoR - inflation)))
          postValueAccount.push(currVal)
      }

      postValueAccount = postValueAccount.map(function(val, index){
        return Math.round(val)
      }).reverse()

      neededMaxAmountAtRetirement = postValueAccount[0]
      // calculating annualized tax rate at 95
      annualizedRate = (Math.pow((postValueAccount[0]/initialBalance),(1/(retirementAge - age)))) -1;

      // calculating needs post retirement;
      for(var o = age + 1; o < retirementAge;o++){
        preretireVal = retirementAccountNeeds[retirementAccountNeeds.length -1] * (1+annualizedRate)
        retirementAccountNeeds.push(preretireVal)
      }
      retirementAccountNeeds = retirementAccountNeeds.map(function(val, index){
        return Math.round(val)
      })
      retirementAccountNeeds = retirementAccountNeeds.concat(postValueAccount)

      return retirementAccountNeeds

    }


    var maxAgeRiskVal = ((ageData.length - baseAgeDifference + retirementAge) - lifeExpectancy)
    if(maxAgeRiskVal >= 20){
        riskContent = "Very Low"
    } else if(maxAgeRiskVal < 20 && maxAgeRiskVal >= 5){
        riskContent = "Low"
    } else if(maxAgeRiskVal  >=0 && maxAgeRiskVal < 5){
        riskContent = "Medium"
    } else if(maxAgeRiskVal>= -5 && maxAgeRiskVal < 0){
        riskContent = "High"
    } else if (maxAgeRiskVal < -5){
        riskContent = "Very High"
    } else {
        riskContent = "Very, Very Low"
    }


    //final calculations to format data for graphs object
    var actualPace = calculateActualRetirement();
    var minimumNeededRetirementAccount = calculateNeedsPreAndPostRetire()
    var growth = maxInvestmentVal - initialBalance - contributionsValueAtRetirement


    return {
        graph1:{
            "actualPace":actualPace,
            "minNeededAccount": minimumNeededRetirementAccount
        },
        graph2:{
            "initialBalance":[initialBalance, initialBalance/maxInvestmentVal],
             "contributions":[contributionsValueAtRetirement, contributionsValueAtRetirement/maxInvestmentVal],
             "growth":[growth, growth/maxInvestmentVal]
         },
        graph3:{
            "nominalIncomeTaken": nominalIncomeTaken,
            "realIncomeNeededEachYear":annualWithdrawal,
            "shortfall":nominalIncomeTaken[nominalIncomeTaken.length -1] - annualWithdrawal
        },
        "riskContent": riskContent,
        "AgeData": ageData,
        "retirementAge":retirementAge,
        "ActualInvestmentAtRetirement": maxInvestmentVal,
        "neededInvestmentAtRetirement": neededMaxAmountAtRetirement
    }

}

// Html Markup
var injectCalc = document.getElementById('calcOutput');
injectCalc.innerHTML = '<div class="container"> <div class="row wrapper"> <div class="col-lg-2 col-lg-offset-1 text-right inputs-wrapper"></div><div class="col-lg-12 data-wrapper"> <div class="row"> <div class="col-lg-12"> <canvas id="myChart"></canvas> </div><div class="col-lg-12 rates"> <div class="row"> <div class="col-lg-12 text-center"> <h3>Based on the details you provided:</h3> </div><div class="col-sm-10 col-sm-offset-1 rate"> <div class="row align-items-center"> <div class="col-lg-3 text-center rate-percent"> <span>At</span> <span>2%</span> <span>Rate of return</span> </div><div class="col-lg-8 text-breakdown"> <p>Your portfolio is estimated to last until you are: <span class="finalAge">X</span> years old or <span class="investmentDuration">X</span> years into retirement, <span class="estimatedText">with an estimated portfolio value at the time of retirement of</span> <span class="finalValue">$X,XXX,XXX</span></p></div></div></div><div class="col-sm-10 col-sm-offset-1 rate"> <div class="row align-items-center"> <div class="col-lg-3 text-center rate-percent"> <span>At</span> <span>5%</span> <span>Rate of return</span> </div><div class="col-lg-8 text-breakdown"> <p>Your portfolio is estimated to last until you are: <span class="finalAge">X</span> years old or <span class="investmentDuration">X</span> years into retirement, <span class="estimatedText">with an estimated portfolio value at the time of retirement of</span> <span class="finalValue">$X,XXX,XXX</span></p></div></div></div><div class="col-sm-10 col-sm-offset-1 rate"> <div class="row align-items-center"> <div class="col-lg-3 text-center rate-percent"> <span>At</span> <span>8%</span> <span>Rate of return</span> </div><div class="col-lg-8 text-breakdown"> <p>Your portfolio is estimated to last until you are: <span class="finalAge">X</span> years old or <span class="investmentDuration">X</span> years into retirement, <span class="estimatedText">with an estimated portfolio value at the time of retirement of</span> <span class="finalValue">$X,XXX,XXX</span></p></div></div></div></div></div></div></div></div></div>';
var myChart = document.getElementById('myChart').getContext('2d');

//Main calculation function gets declared here @zaw
var retData = calculateSavings();
console.log("data needed", retData)

// Globl Options
Chart.defaults.global.defaultFontFamily = 'Roboto';
Chart.defaults.global.defaultFontSize = 18;
Chart.defaults.global.defaultFontColor = '#4c4c4c';

var demoChart = new Chart(myChart, {

    type: 'line', // bar, horizontal bar, pie, line, doughnut, radar, polarArea
    data: {
        labels: retData["AgeData"],
        datasets:[{
            data: retData ["graph1"]["actualPace"],
            label: 'Actual',
            backgroundColor: '#B6CCC2',
            pointHoverBackgroundColor: '#B6CCC2',
            borderWidth: 0,
            borderColor: 'rgba(0,0,0,0)',
            hoverBorderWidth: 3,
            hoverBorderColor: 'black',
            pointBackgroundColor: 'rgba(0,0,0,0)',
            pointRadius: 2
        },{
            data: retData["graph1"]["minNeededAccount"],
            label: 'Need to Last Until 95',
            backgroundColor: '#769D91',
            pointHoverBackgroundColor: '#769D91',
            borderWidth: 0,
            borderColor: 'rgba(0,0,0,0)',
            hoverBorderWidth: 3,
            hoverBorderColor: 'black',
            pointBackgroundColor: 'rgba(0,0,0,0)',
            pointRadius: 2
        }]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        hover: {
            intersect: false
        },
        title: {
            display: true,
            text: ['ESTIMATED PORTFOLIO VALUE', 'Based on annual returns of:'],
            fontSize: 20,
            padding: 30
        },
        legend: {
            display: true,
            position: 'top',
            labels: {
                fontColor: 'black'
            }
        },
        layout: {
            padding: {
                left: 0,
                right: 0,
                bottom: 0,
                top: 0
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    callback: function(label, index, labels) {
                        return '$' + nFormatter(label);
                    },
                    beginAtZero: true,
                    min: initialBalance
                },
            }],
            xAxes: [{
            scaleLabel: {
                display: true,
                labelString: 'Age',
            },
            ticks: {
                autoSkip: true,
                maxTicksLimit: 6
            }
            }]
        },
        plugins: {
            datalabels: {
                // hide datalabels for all datasets
                display: false
            }
        },
        tooltips: {
            mode: 'point',
            displayColors: true,
            titleFontSize: 0,
            callbacks: {
                label: function(tooltipItem, data) {
                    var beforeLabel = data.labels[tooltipItem.index];
                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    return 'Age: ' + beforeLabel + ' Portfolio Value: $' +  toCommas(value);
                }
            }
        }
    }
});


