//v.9/214/2020

//Start Of Calculations

// Grab Cookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}

// Set Cookie To Variable
function setCookie(name,value,minutes) {
    var expires = "";
    if (minutes) {
        var date = new Date();
        var minutes = 30;
        date.setTime(date.getTime() + (minutes * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}


var cookie = getCookie('calculator_query_string');

// Demo If Statement to split into array
if(cookie) {
    cookie = cookie.split(/(?:=|&)/);
}

//Variables need to be set to cookie values. Zaw
// var age = 85;
// var retirementAge = 75;
// var initialBalance = 1000000;
// var contributionValue = 25000;
// var annualWithdrawal = 80000;
// var wantedTotalAmountAtLastAge =250000.00;
// var estimatedTaxRate;

var age = parseInt(cookie[1]);
var retirementAge = parseInt(cookie[3]);
var initialBalance = parseInt(cookie[5]);
var contributionValue = parseInt(cookie[7]);
var annualWithdrawal = parseInt(cookie[9]);
var wantedTotalAmountAtLastAge = parseInt(cookie[11]);

var investmentRoR = 0.07;
var inflation = 0.016;
var lifeExpectancy = 95;
//...................//

// console.log('My initial cookie age is '+age+' I plan to cookie retire at '+retirementAge+' My initial cookie invest will be '+initialBalance+' My annual cookie contribution will be '+contributionValue);



function calculateSavings(rateOfReturn) {
    // Value Of Adds Calculation Values
    var baseAgeDifference = retirementAge - age;


    if(baseAgeDifference <= 0) {
        baseAgeDifference = 0;
    }

    var baseAgeValue = (1 + age) - age;
  
  
    ageData = [];
    savingsData = [];
    valueAddsData = [];
    var paceData = [initialBalance];
    var yearsInvested = 0;
    var yearCounter = 1;
    //Daryl Change here V2
    var contributions = [contributionValue];
    var valueOfAdds_preRetire = [];
    var finalInvestment;
    var nominalIncomeTaken = [annualWithdrawal];
    var contributionsValueAtRetirement = 0;
    var maxInvestmentVal = 0;
    var riskText;
    var riskImage;
    var ageBeforeUnderWantedAmount = 0;
    var neededMaxAmountAtRetirement  = 0;


  var estimatedTaxRate;
    function calculateTaxRate(){
        //TODO:Check if Usewr has given tax rate
        if(!estimatedTaxRate){
            var totalEffectiveTax = 0;
            var currTaxVal;
            var totalEffectiveTaxRate;
            var brackets = [
                [9699,0,0.1],
                [39474,9700,0.12],
                [84199,39475,0.22],
                [160724,84200,0.24],
                [204099,160725,0.32],
                [510299,204100,0.35],
                [1000000,510300,0.37]
            ];

            try{
                for(var p = 0; p < brackets.length;p++){
                    if(annualWithdrawal >= brackets[p][0]){
                        currTaxVal = (brackets[p][0] - brackets[p][1]) * brackets[p][2];
                        totalEffectiveTax += currTaxVal;
                    } else if(annualWithdrawal > brackets[p][1] && annualWithdrawal < brackets[p][0]){
                        currTaxVal = (annualWithdrawal - brackets[p][1]) * brackets[p][2];
                        totalEffectiveTax += currTaxVal;
                    }
                }
            } catch(e){
                console.log("Calculate Tax Rate function", e);
            }
            totalEffectiveTaxRate = (totalEffectiveTax/annualWithdrawal);
            estimatedTaxRate = totalEffectiveTaxRate;
        }
    }
    //calculate tax rate here
    calculateTaxRate();

    for(i = age; i <= lifeExpectancy; i++) {
        ageData.push(i);
        //Calculate Contribution
        var ageDifference = (i+1) - age;
        var inflationTotal = Math.pow(1+(investmentRoR - inflation),baseAgeDifference-(baseAgeDifference- yearCounter));
        var savingsIncrease = (contributions[0] * inflationTotal);
        savingsData.push(savingsIncrease);
        //Calculate Value Of Adds
        if(yearCounter <= baseAgeDifference) {
            //custom by daryl
            var currentContribution = contributions[0] * Math.pow((1+inflation),yearCounter);
            var current_preRetirementValueAdd;

            if(i === age){
                //For value add
                current_preRetirementValueAdd  = contributions[0] * inflationTotal;
            } else {
                //For value add
                current_preRetirementValueAdd = contributions[contributions.length - 1] * inflationTotal;
            }
            contributions.push(currentContribution);
            valueOfAdds_preRetire.push(current_preRetirementValueAdd);


            yearCounter++;
        }
        yearsInvested++;

        //for nominal income needed
        if(i <= lifeExpectancy - baseAgeDifference){
            currentNominalIncomeValue = nominalIncomeTaken[nominalIncomeTaken.length -1] * (1 + inflation);
            nominalIncomeTaken.push(currentNominalIncomeValue);
        }
    }
    nominalIncomeTaken = nominalIncomeTaken.map(function(val, index){
        return Math.round(val);
    });
    //remove unneeded last contribution
    contributions.pop();
    nominalIncomeTaken.pop();

    contributions.forEach(function(val, index){
        contributionsValueAtRetirement += val;
    });
    recontributionValue = contributionValue;

    // Calculate Pace


    function calculateActualRetirement() {
        var total = valueAddsData[0];

        var yearCounter = 1;
        var paceCalcArray = [];
        var yearsAfterRetirement = lifeExpectancy - retirementAge;
        var yearsInvestedPace = 0;
        var valueAddsTotal = 0;
        var currPacePreRet;


        //preretire
        for(var i = 0; i < baseAgeDifference; i++) {
            valueAddsTotal += valueOfAdds_preRetire[i];
            currPacePreRet = valueAddsTotal+initialBalance * Math.pow((1+investmentRoR - inflation), (i + 1));
            paceData.push(currPacePreRet);
        }
        maxInvestmentVal = paceData[paceData.length -1];

        // Caluclate Final Pace Values and Start Annual Withdrawal
        // (final investment - (withdrawl/1-estimated tax rate) *

        finalInvestment = paceData.slice(-1)[0];
        paceCalcArray.push(finalInvestment);
        //console.log("this is final investment:",paceCalcArray)
        for(var l = 1; l <= yearsAfterRetirement; l++) {
            //...............Used for final input in graph data.............//
            finalInvestment = (finalInvestment - ((annualWithdrawal/(1- estimatedTaxRate)) * Math.pow((1+inflation),((retirementAge + l) - Math.min(age, retirementAge))))) * (1 + investmentRoR -inflation);
            finalInvestmentFormatted = finalInvestment;

            paceCalcArray.push(finalInvestmentFormatted);
            paceData.push(finalInvestmentFormatted);

        }
        console.log(baseAgeDifference, yearsAfterRetirement);

        return paceData;
    }

    //Daryl change here

    //Calculate minimum needed retirement account
    function calculateNeedsPreAndPostRetire(){

      //calculating post retire account value
      var postValueAccount = [wantedTotalAmountAtLastAge];
      var retirementAccountNeeds = [initialBalance];
      var annualizedRate;
      var preretireVal;
      retirementAge = retirementAge >= age ? retirementAge : age;
      for(var l = lifeExpectancy; l > retirementAge;l--){
          var currVal = (((annualWithdrawal /(1 - estimatedTaxRate)) * Math.pow((1 + inflation), (l - Math.min(age, retirementAge)))) + (postValueAccount[postValueAccount.length -1]/(1+ investmentRoR - inflation)));
          postValueAccount.push(currVal);
      }

      postValueAccount = postValueAccount.map(function(val, index){
        return Math.round(val);
      }).reverse();

      neededMaxAmountAtRetirement = postValueAccount[0];
      // calculating annualized tax rate at 95
      annualizedRate = (Math.pow((postValueAccount[0]/initialBalance),(1/(retirementAge - age)))) -1;

      // calculating needs post retirement;
      console.log("Post val account", postValueAccount);
      for(var o = age + 1; o < retirementAge;o++){
        preretireVal = retirementAccountNeeds[retirementAccountNeeds.length -1] * (1+annualizedRate);
        retirementAccountNeeds.push(preretireVal);
      }
      retirementAccountNeeds = retirementAccountNeeds.map(function(val, index){
        return Math.round(val);
      });
      retirementAccountNeeds = retirementAccountNeeds.concat(postValueAccount);

      return retirementAccountNeeds;

    }

    //final calculations to format data for graphs object
    var actualPace = calculateActualRetirement();
    var minimumNeededRetirementAccount = calculateNeedsPreAndPostRetire();
    var growth = maxInvestmentVal - initialBalance - contributionsValueAtRetirement;

    if(age >= retirementAge){
        minimumNeededRetirementAccount.shift();
    }

    // TODO: IF the portfolio is still equal or greater than expected amount at 95,
    // then keep running the loop. Max loop till age 115.
    // Are we tracking the Age when portfolio drops below expected amount at 95 years old?
    

    // var paceFromRetireToEnd = actualPace.slice(retirementAge - age).filter(function(pace) { return pace >= wantedTotalAmountAtLastAge });
    
    
    // Find index where current pace dips below expected
    // start pace from retirementAge
    var riskPace = actualPace.filter(function(pace) { return pace >= wantedTotalAmountAtLastAge });
    var ageBelowExpected;
    if (riskPace.length == actualPace.length) {
        // if pace hasn't dipped below expected ammount, keep iterating from retire Age until pace dips and before age 165
        var riskPace = JSON.parse(JSON.stringify(actualPace));
        var postLifePace = riskPace.slice(-1)[0];
        for(var i = 0; (i < (165 - retirementAge)) && (postLifePace > wantedTotalAmountAtLastAge) ; i++) {
            postLifePace = (postLifePace - ((annualWithdrawal/(1- estimatedTaxRate)) * Math.pow((1+inflation),((lifeExpectancy + i) - Math.min(age, retirementAge))))) * (1 + investmentRoR -inflation);
            riskPace.push(postLifePace);
        }
        ageBelowExpected = (riskPace.length - 1) + age;
    } else {
        ageBelowExpected = (riskPace.length - 1) + age;
    }
    
    var maxAgeRiskVal = ageBelowExpected - 95;
    if(maxAgeRiskVal >= 20){
        riskText = "Very Low";
        riskImage = "/-/media/Files/martech/lpf101/images/VeryLowProb2x.png";
    } else if(maxAgeRiskVal < 20 && maxAgeRiskVal >= 5){
        riskText = "Low";
        riskImage = "/-/media/Files/martech/lpf101/images/LowProb2x.png";
    } else if(maxAgeRiskVal  >=0 && maxAgeRiskVal < 5){
        riskText = "Medium";
        riskImage = "/-/media/Files/martech/lpf101/images/MediumProb2x.png";
    } else if(maxAgeRiskVal>= -5 && maxAgeRiskVal < 0){
        riskText = "High";
        riskImage = "/-/media/Files/martech/lpf101/images/HighProb2x.png";
    } else if (maxAgeRiskVal < -5){
        riskText = "Very High";
        riskImage = "/-/media/Files/martech/lpf101/images/VeryHighProb2x.png";
    } else {
        riskText = "Very, Very Low";
        riskImage = "/-/media/Files/martech/lpf101/images/VeryLowProb2x.png";
    }

    return {
        graph1:{
            "actualPace":actualPace,
            "minNeededAccount": minimumNeededRetirementAccount
        },
        graph2:{
            "initialBalance":[initialBalance, initialBalance/maxInvestmentVal],
             "contributions":[contributionsValueAtRetirement, contributionsValueAtRetirement/maxInvestmentVal],
             "growth":[growth, growth/maxInvestmentVal]
         },
        graph3:{
            "nominalIncomeTaken": nominalIncomeTaken,
            "realIncomeNeededEachYear":annualWithdrawal,
            "shortfall":nominalIncomeTaken[nominalIncomeTaken.length -1] - annualWithdrawal
        },
        "riskText": riskText,
        "riskImage": riskImage,
        "AgeData": ageData,
        "retirementAge":retirementAge,
        "ActualInvestmentAtRetirement": maxInvestmentVal,
        "neededInvestmentAtRetirement": neededMaxAmountAtRetirement,
        "actualDiffNeedAtRetire": maxInvestmentVal - neededMaxAmountAtRetirement,
        "isSurplus": ((maxInvestmentVal - neededMaxAmountAtRetirement) > 0 ? true : false),
        "effectiveTaxRate": estimatedTaxRate, 
    };

}

function calcPortfolioValAtRet(yearsIntoRetirement){
 if(yearsIntoRetirement < 0){
    yearsIntoRetirement = 0;
 }
   return Math.pow((initialBalance + (1+ (investmentRoR - inflation))),yearsIntoRetirement);
}


//Main calculation function gets declared here @zaw
// var paceMedium = calculateSavings();
// console.log("data needed", paceMedium);


// Run Array And Find Value 'f' to calculate years into retirement portfolio hits 0 or below
function calculateYearsIntoRetirement(paceArray) {

    paceArray = paceArray.graph1.actualPace;
    console.log(paceArray.length);
    for(f=0; f <= paceArray.length; f++) {
        if(paceArray[f] < wantedTotalAmountAtLastAge) {
            var yearsIntoRetirement = (f - 1) - (retirementAge - age) ;
            return yearsIntoRetirement;
        } else {
            // Do nothing
        }
    }
}

// Run array to find value to calculate age at which portfolio hits 0
function calculateMaxAge(paceArray) {

    paceArray = paceArray.graph1.actualPace;
    for(f=0; f <= paceArray.length; f++) {
        if(paceArray[f] < wantedTotalAmountAtLastAge) {
            //console.log('you are broke');
            var ageAtZero = retirementAge + (f -1) - (retirementAge - age);
            //console.log("retirement age",ageAtZero)
            return ageAtZero;
        } else {
            // Do nothing
        }
    }
}

function paceDurationMedium() {
    var yearsIntoRetirement = calculateYearsIntoRetirement(paceMedium);
    console.log("years into retirement:", yearsIntoRetirement);
    if(isNaN(retirementAge + yearsIntoRetirement)) {
        console.log("html will be set to OVER 100 years. Meaning value of portfolio doesn't reach 0 in array timeframe", `${calculateMaxAge(paceMedium)} or ${yearsIntoRetirement} years into retirement.`);
    } else {
        console.log(`The medium portfolio value will last until the person is ${calculateMaxAge(paceMedium)} or ${yearsIntoRetirement} years into retirement.`);
    }
}