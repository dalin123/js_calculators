/************************************************************************************************/
/******************************* Formatting and Helper Functions ********************************/
/************************************************************************************************/
function ReasonableRound(n) {
    var divCount = 0;
    while (n > 100) {
        n = n / 10;
        divCount++;
    }
    n = Math.round(n); // round to nearest integer
    while (divCount > 0) {
        n *= 10;
        divCount--;
    }
    return n;
  }
  
  function round(value, decimals) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
  }
  
  function createRounder(decimals) {
    return (function roundFn(value) {
      round(value, decimals);
    });
  }
  
  function FormatCurrencyKM(val, rounder) {
    var suffix = "";
    var sign = "";
    if (val < 0) {
        sign = "-";
        val = -val;
    }
    if (val < 1000) { // do nothing
    } else if (val < 1000000) {
        val = Math.round(val / 1000); // if less than 1M, get rid of decimals
        suffix = "K";
    } else if (val < 1000000000) {
        val = val / 1000000;
        suffix = "M";
    }
  
    if (rounder) {
      val = rounder(val);
    } else {
      val = round(val, 2);
    }
  
    return "$" + sign + val + suffix;
}

//Format Array Data To $ and , Versions For Chart
function formatArray(arrayName) {
  formattedArray = [];
  for(l = 0; l < arrayName.length; l++) {
      formattedArray.push('$'+toCommas(arrayName[l]));
  }

  return formattedArray;
}

/* Outputs Numbers like 1400000 as 1.4M */
function nFormatter(num) {
  if (num >= 1000000000) {
      return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
  }
  if (num >= 1000000) {
      return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
  }
  if (num >= 1000) {
      return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
  }
  return num;
}

/* Outputs Numbers like 1400000 as 1,400,000 */
function toCommas(value) {
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function setHtml(data) {
  // Create Money Format with wNumb library	
  var MONEYFormat = wNumb({	
    prefix: "$",
    negativeBefore: "-",
    thousand: ','
  });
  // Inject Risk Level and Gauge Meter Image for Desktop + Mobile	
  document.getElementById("RiskLevelDesktop").innerHTML = data.riskText;	
  document.getElementById("RiskLevelMobile").innerHTML = data.riskText;	
  document.getElementById("RiskGaugeDesktop").src = data.riskImage;	
  document.getElementById("RiskGaugeMobile").src = data.riskImage;	
  // Inject Dollar Values for "What you have" and "what you need" at retirement	
  // for desktop and mobile elements	
  document.getElementById("portfolioAtX").innerHTML = 
    FormatCurrencyKM(data.ActualInvestmentAtRetirement);	
  document.getElementById("portfolioAtXM").innerHTML = 
    FormatCurrencyKM(data.ActualInvestmentAtRetirement);	
  document.getElementById("portfolioNeededAtRetirement").innerHTML = 
    FormatCurrencyKM(data.neededInvestmentAtRetirement);	
  document.getElementById("portfolioNeededAtRetirementM").innerHTML = 
    FormatCurrencyKM(data.neededInvestmentAtRetirement);	
  // Inject Have, Need, and Shortfall/Surplus Values	
  document.getElementById("HaveValue").innerHTML = 
    FormatCurrencyKM(data.ActualInvestmentAtRetirement);
  document.getElementById("NeedValue").innerHTML = 	
    FormatCurrencyKM(data.neededInvestmentAtRetirement);	
  document.getElementById("DiffValue").innerHTML = 
    FormatCurrencyKM(data.actualDiffNeedAtRetire);
  document.getElementById("DiffText").innerHTML = data.isSurplus ? "Surplus" : "Shortfall";	
  // Inject Initial Balance, Contributions, and Growth for doughnut values
  document.getElementById("doughnutBalance").innerHTML = 
    MONEYFormat.to(Math.round(data.graph2.initialBalance[0]));
  document.getElementById("doughnutGrowth").innerHTML = 
    MONEYFormat.to(Math.round(data.graph2.growth[0]));
  document.getElementById("doughnutContribution").innerHTML = 
    MONEYFormat.to(Math.round(data.graph2.contributions[0]));
}

function hideIfTrue(isRetired, id) {
  var el = document.getElementById(id);
  if(isRetired) {
    el.style.display = "none";
  } else {
    el.style.display = "";
  }
}

// sort/order the two line chart array
function createLineChartDataObject(data) {
  var ageArray = data.AgeData;
  var actualPaceArray = data.graph1.actualPace;
  var minNeedPaceArray = data.graph1.minNeededAccount;
  
  var actualData = {
    label: "What you'll need",	
    data: minNeedPaceArray,	
    backgroundColor: "rgba(77,127,113,100)",	
    pointRadius: 0,	
    fill: 'origin',	
    showLine: true,	
  };
  var needData = {
    label: "What you'll have",	
    data: actualPaceArray,	
    backgroundColor: "rgba(255,176,67,100)",	
    pointRadius: 0,	
    fill: 'origin',	
    showLine: true,	
  };

  var dataArray = [];
  if(data.actualDiffNeedAtRetire > 0) {
    dataArray.push(actualData, needData);
  } else {
    dataArray.push(needData, actualData);
  }
  

  return {
    labels: ageArray,
    datasets: dataArray
  };


}

function addPieCenterTextFn(doughnutConfig) {	
    var width = doughnutConfig.chart.width;	
    var height = doughnutConfig.chart.height;	
    var ctx = doughnutConfig.chart.ctx;	
    ctx.restore();	
    var fontSize = (height / 150).toFixed(2);	
    ctx.font = fontSize + "em sans-serif";	
    ctx.textBaseline = "middle";	
    ctx.fillStyle = "#333";	
    var text = this.actualPortfolioAtRetire;	
    var textX = Math.round((width - ctx.measureText(text).width) / 2);	
    var textY = height / 2;	
    ctx.fillText(text, textX, textY);	
    ctx.save();	
}

function createLineChart(data, canvasId) {
  // Inject Line Chart	
  var lineContext = document.getElementById(canvasId).getContext('2d');	
  var lineChart = new Chart(lineContext, {	
    type: 'line',	
    data: createLineChartDataObject(data),	
    options: {	
      plugins: {	
        datalabels: {	
          display: false	
        }	
      },	
      responsive: true,	
      maintainAspectRatio: true,	
      legend: {	
        display: false	
      },	
      animation: {	
          duration: 0	
      },	
      scales: {	
        yAxes: [{	
          ticks: {	
            min: 0,
            beginAtZero: true,	
            maxTicksLimit: 5,	
            padding: 5,	
            callback: function(value, index, values) {	
              return FormatCurrencyKM(value);	
            }	
          },	
          gridLines: {	
            display: true,	
            borderDashOffset: {	
              display: true	
            },	
            drawTicks: false	
          },	
        }],	
        xAxes: [{	
          gridLines: {
            display: false
          },
          ticks: {	
            min: 0,	
            max: lifeExpectancy,	
            //beginAtZero: false,	
            startValue: 45,	
            step: 5,	
            maxTicksLimit: 9	
          },	
          scaleLabel: {	
            display: true,	
            labelString: 'Age',	
            fontSize: 16,	
            fontStyle: 'bold'	
          }	
        }]	
      },	
      tooltips: {	
        enabled: true,	
        backgroundColor: '#fff',	
        borderColor: '#000',	
        borderWidth: '1',	
        titleFontColor: '#000',	
        bodyFontColor: '#000',	
        mode: 'nearest',	
        intersect: false,	
        callbacks: {	
          title: function(tooltipItems, data) {	
            var tooltipItem = tooltipItems[0];	
            return data.datasets[tooltipItem.datasetIndex].label + ": " + FormatCurrencyKM(tooltipItem.yLabel);	
          },	
          label: function(tooltipItem, data) {	
            return "At Age " + tooltipItem.xLabel;	
          }	
        }	
      }	
    },	
    plugins: [{	
      afterDatasetsDraw: function (myChart) {	
        if (myChart.tooltip._active && myChart.tooltip._active.length) {	
          var activePoint = myChart.tooltip._active[0],	
            ctx = myChart.ctx,	
            y_axis = myChart.scales['y-axis-0'],	
            x = activePoint.tooltipPosition().x,	
            topY,	
            bottomY;	
          if (y_axis) {	
            topY = y_axis.top;
            bottomY = y_axis.bottom;	
          }	
          // draw line	
          ctx.save();	
          ctx.beginPath();	
          ctx.moveTo(x, topY);	
          ctx.lineTo(x, bottomY);	
          ctx.lineWidth = 3;	
          ctx.strokeStyle = '#000000';	
          ctx.pointBackgroundColor = '#000000';	
          ctx.pointRadius = 20;	
          ctx.pointHighlightFill = '#000000';	
          ctx.pointStrokeColor = '#000000';	
          ctx.stroke();	
          ctx.restore();	
        }	
      },	
    }],	
  });
  
  return lineChart;
}

function createDoughnutChart(data, canvasId) {
  // Format and Round Numbers	
  var roundBalancePercent = Math.round(data.graph2.initialBalance[1] * 100);	
  var roundContributePercent = Math.round(data.graph2.contributions[1] * 100);
  var roundGrowthPercent = Math.round(data.graph2.growth[1] * 100);
  var actualPortfolioAtRetire = FormatCurrencyKM(parseFloat(data.neededInvestmentAtRetirement));	
  
  // if person is already retired
  hideIfTrue(data.isRetired, 'doughnutDiv');
  
  // Inject Doughnut Chart	
  var doughnutContext = document.getElementById(canvasId).getContext("2d");	
  var doughnutChart = new Chart(doughnutContext, {	
    type: 'doughnut',	
    data: {	
      labels: ['Initial Balance', 'Contributions', 'Growth'],	
      datasets: [{	
        data: [	
          roundBalancePercent,	
          roundContributePercent,	
          roundGrowthPercent	
        ],	
        backgroundColor: [	
          '#6C3B6F',	
          '#4D7F71',	
          '#FFA426'	
        ],	
        borderWidth: [	
          1,	
          1,	
          1	
        ],	
        borderAlign: "inner",	
        borderColor: [	
          '#6C3B6F',	
          '#4D7F71',	
          '#FFA426'	
        ],	
      }],	
    },	
    options: {	
      responsive: true,	
      aspectRatio: 1,	
      maintainAspectRatio: true,	
      legend: {	
        display: false	
      },	
      title: {	
        display: false	
      },	
      tooltips: {	
        enabled: false	
      },	
      animation: {	
        animateScale: false,	
        animateRotate: false	
      },	
      elements: {	
        customCutout: true	
      },	
      layout: {	
        padding: {	
          left: 0,	
          right: 0,	
          top: 0,	
          bottom: 0	
        }	
      },	
      rotation: 5,	
      plugins: {	
        datalabels: {	
          // Requires 3rd party plugin chartjs-plugin-datalabels	
          color: '#fff',	
          textAlign: 'center',	
          font: {	
            lineHeight: 1.6,	
            weight: 'bold'	
          },	
          formatter: function(value, ctx) {	
            return value + '%';	
          }	
        },	
      }	
    },	
    plugins: [{	
      // MR - adding center text on doughnut	
      beforeDraw: addPieCenterTextFn.bind({actualPortfolioAtRetire})	
    }, {	
      //doughnut custom cutout percentage for contributions	
      //https://github.com/chartjs/Chart.js/issues/6195	
      // Change thickness of line for the data section of the	
      // doughnut chart that displays the amount that is left to be raised. Accessing data[1] gets us the	
      // correct data section of the doughnut we want to manipulate.	
      // Bill Ahlbrandt - make the radius of data[0] 0.93*data[1] so it scales properly	
      beforeDraw: function (doughnutConfig) {	
        if (doughnutConfig.config.options.elements.customCutout !== undefined) {	
          //doughnutConfig.getDatasetMeta(0).data[1]._view.innerRadius = 94;	
          doughnutConfig.getDatasetMeta(0).data[0]._view.outerRadius = doughnutConfig.getDatasetMeta(0).data[1]._view.outerRadius * 0.93;	
          doughnutConfig.getDatasetMeta(0).data[2]._view.outerRadius = doughnutConfig.getDatasetMeta(0).data[1]._view.outerRadius * 0.93;	
          //doughnutConfig.getDatasetMeta(0).data[0]._view.outerRadius = 60;	
          //doughnutConfig.getDatasetMeta(0).data[2]._view.outerRadius = 70;	
          doughnutConfig.update();	
        }	
      }	
    }],	
  });
  
  return doughnutChart;
}

function createNominalIncomeChart(data, canvasId) {
    // if person is already retired
    hideIfTrue(!data.isRetired, 'RetiredTimelineDiv');

    var ctxx = document.getElementById(canvasId).getContext("2d");
    var nominalChart = new Chart(ctxx, {
      type: 'line',
      data: {
        labels: data.AgeData,
        datasets: [{
          label: 'Nominal Income Taken',
          data: data.graph3.nominalIncomeTaken,
          backgroundColor: "#6C3B6F",
          borderColor: "#6C3B6F",
          pointRadius: 0,
          fill: 'none',
        },
        ],
      },
      options: {
        plugins: { datalabels: { display: false } },
        responsive: true,
        maintainAspectRatio: true,
        legend: { display: true, position: 'bottom', boxWidth: 20, },
        animation: { duration: 0 },


        scales: {
          yAxes: [{
            ticks: {
              min: 0,
              beginAtZero: true,
              maxTicksLimit: 5,
              padding: 5,
              callback: function (value, index, values) {
                return FormatCurrencyKM(value);
              }
            },
            gridLines: {
              display: true,
              borderDashOffset: { display: true },
              drawTicks: false
            },
          }],
          xAxes: [{
            gridLines: { display: false },
            ticks: {
              min: 0,
              max: lifeExpectancy,
              //beginAtZero: false,
              startValue: 45,
              step: 5,
              maxTicksLimit: 9
            },
            scaleLabel: {
              display: true,
              labelString: 'Age',
              fontSize: 16,
              fontStyle: 'bold'
            }
          }]
        },
        tooltips: {
          enabled: true,
          backgroundColor: '#fff',
          borderColor: '#000',
          borderWidth: '1',
          borderRadius: '1',
          titleFontColor: '#000',
          bodyFontColor: '#000',
          mode: 'nearest',
          axis: 'x',
          intersect: false,
          callbacks: {
            title: function (tooltipItems, data) {
              let tooltipItem = tooltipItems[0];
              return "Nominal Income: " + FormatCurrencyKM(tooltipItem.yLabel);;
            },
            label: function (tooltipItem, data) {
              return " At Age " + tooltipItem.xLabel;
            }
          }
        }
      }
    });

    return nominalChart;
}

function attachAnimationCallbacks() {
  // Popover features for question-mark tooltips	
  // Dependency: jquery-2.2.4, bootstrap, popover	
  $('[data-toggle="popover"]').popover({	
        placement: 'bottom',	
        boundary: 'viewport',	
        container: 'body',	
        html: true,	
        content: function () {	
            var content = $(this).attr("data-popover-content");	
            return $(content).html();	
        }	
  });	
  $('body').on('click', function(e) {	
    $('[data-toggle="popover"]').each(function() {	
      //$("div.overlay").toggleClass("on");	
      if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {	
        $(this).popover('hide');	
      //$("div.overlay").removeClass("on");	
      }	
    });	
  });
}

function updateLineChart(chart, data) {
  chart.data = createLineChartDataObject(data);
  chart.update({ duration: 0 });
}

function updatePieChart(chart, data) {
  // Format and Round Numbers	
  var roundBalancePercent = Math.round(data.graph2.initialBalance[1] * 100);	
  var roundContributePercent = Math.round(data.graph2.contributions[1] * 100);
  var roundGrowthPercent = Math.round(data.graph2.growth[1] * 100);
  
  chart.data.datasets[0].data = [
    roundBalancePercent,	
    roundContributePercent,	
    roundGrowthPercent	
  ];
  
  var actualPortfolioAtRetire = FormatCurrencyKM(parseFloat(data.neededInvestmentAtRetirement));
  chart.config.plugins[0].beforeDraw = addPieCenterTextFn.bind({actualPortfolioAtRetire});
  chart.update({ duration: 0 });
  
  // if person is already retired
  hideIfTrue(data.isRetired, 'doughnutDiv');
}

function updateNominalIncomeChart(chart, data) {
  // if person is already retired
  hideIfTrue(!data.isRetired, 'RetiredTimelineDiv');
  
  chart.data.labels = data.AgeData;
  chart.data.datasets[0].data = data.graph3.nominalIncomeTaken;
  chart.update({ duration: 0 });
}

function getHtmlInputs() { 
  // Really bad pattern: please do not replicate
  age = Number(document.querySelector('input[data-calculator-key="ica"]').value);
  retirementAge = Number(document.querySelector('input[data-calculator-key="ira"]').value);
  initialBalance = Number(document.querySelector('input[data-calculator-key="icp"]').value);
  annualWithdrawal = Number(document.querySelector('input[data-calculator-key="ircf"]').value);
  contributionValue = Number(document.querySelector('input[data-calculator-key="ias"]').value);
  wantedTotalAmountAtLastAge = Number(document.querySelector('input[data-calculator-key="iaale"]').value);;

  investmentRoR = 0.07;
  inflation = 0.016;
  lifeExpectancy = 95;
}

function attachRecalculateCallback(lineChart, pieChart, nominalChart) {
  var recalcBtnElement = document.querySelector("div[data-fb-submit-button] button");
  
  recalcBtnElement.addEventListener('click', function(event){
    
    // TODO: validate inputs
    // get html inputs
    getHtmlInputs();
    // Main Retirement Calculation Output	
    var retData = calculateSavings();
    retData.isRetired = age >= retirementAge;
    // get html inputs again due to side effect for retirementAge
    getHtmlInputs();	
    // update texts
    setHtml(retData);
    // redraw line chart
    updateLineChart(lineChart, retData);
    // redraw doughnut chart
    updatePieChart(pieChart, retData);
    // redraw nominal income line chart
    updateNominalIncomeChart(nominalChart, retData);
  });
}


/****************************************************************************************************/	
/*************************** Main Rendering Block *******************************/	
/****************************************************************************************************/	
window.addEventListener('DOMContentLoaded', function() {	
  
  // Really bad pattern: please do not replicate
  age = parseInt(cookie[1]);
  retirementAge = parseInt(cookie[3]);
  initialBalance = parseInt(cookie[5]);
  contributionValue = parseInt(cookie[7]);
  annualWithdrawal = parseInt(cookie[9]);
  wantedTotalAmountAtLastAge = parseInt(cookie[11]);

  investmentRoR = 0.07;
  inflation = 0.016;
  lifeExpectancy = 95;

  // Main Retirement Calculation Output	
  var retData = calculateSavings();	
  retData.isRetired = age >= retirementAge;

  // Front-End Rendering Texts
  setHtml(retData);
  // get html inputs again due to side effect for retirementAge
    getHtmlInputs();	
  // Front-End Rendering Line Chart
  var lineChart = createLineChart(retData, 'TimelineChart');
  // Front-End Rendering Doughnut Chart
  var doughnutChart = createDoughnutChart(retData, 'chartDoughnut');
  // Front-End Rendering Nominal Income Chart
  var nominalChart = createNominalIncomeChart(retData, 'chartCanvasRetired');
  // Front-End Callbacks for Animation/Transition
  attachAnimationCallbacks();
  // Front-End callback for recalcualte features
  attachRecalculateCallback(lineChart, doughnutChart, nominalChart);
});