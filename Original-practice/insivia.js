//v.3/27/2020

/* Outputs Numbers like 1400000 as 1.4M */
function nFormatter(num) {
    if (num >= 1000000000) {
        return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
    }
    if (num >= 1000000) {
        return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
    }
    if (num >= 1000) {
        return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
    }
    return num;
}

/* Outputs Numbers like 1400000 as 1,400,000 */
function toCommas(value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var injectCalc = document.getElementById('calcOutput');

injectCalc.innerHTML = '<div class="container"> <div class="row wrapper"> <div class="col-lg-2 col-lg-offset-1 text-right inputs-wrapper"></div><div class="col-lg-12 data-wrapper"> <div class="row"> <div class="col-lg-12"> <canvas id="myChart"></canvas> </div><div class="col-lg-12 rates"> <div class="row"> <div class="col-lg-12 text-center"> <h3>Based on the details you provided:</h3> </div><div class="col-sm-10 col-sm-offset-1 rate"> <div class="row align-items-center"> <div class="col-lg-3 text-center rate-percent"> <span>At</span> <span>2%</span> <span>Rate of return</span> </div><div class="col-lg-8 text-breakdown"> <p>Your portfolio is estimated to last until you are: <span class="finalAge">X</span> years old or <span class="investmentDuration">X</span> years into retirement, <span class="estimatedText">with an estimated portfolio value at the time of retirement of</span> <span class="finalValue">$X,XXX,XXX</span></p></div></div></div><div class="col-sm-10 col-sm-offset-1 rate"> <div class="row align-items-center"> <div class="col-lg-3 text-center rate-percent"> <span>At</span> <span>5%</span> <span>Rate of return</span> </div><div class="col-lg-8 text-breakdown"> <p>Your portfolio is estimated to last until you are: <span class="finalAge">X</span> years old or <span class="investmentDuration">X</span> years into retirement, <span class="estimatedText">with an estimated portfolio value at the time of retirement of</span> <span class="finalValue">$X,XXX,XXX</span></p></div></div></div><div class="col-sm-10 col-sm-offset-1 rate"> <div class="row align-items-center"> <div class="col-lg-3 text-center rate-percent"> <span>At</span> <span>8%</span> <span>Rate of return</span> </div><div class="col-lg-8 text-breakdown"> <p>Your portfolio is estimated to last until you are: <span class="finalAge">X</span> years old or <span class="investmentDuration">X</span> years into retirement, <span class="estimatedText">with an estimated portfolio value at the time of retirement of</span> <span class="finalValue">$X,XXX,XXX</span></p></div></div></div></div></div></div></div></div></div>';



//Start Of Calculations

// Grab Cookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}

function setCookie(name,value,minutes) {
    var expires = "";
    if (minutes) {
        var date = new Date();
        var minutes = 30;
        date.setTime(date.getTime() + (minutes * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

//Set Cookie To Variable
var cookie = getCookie('calculator_query_string');

// Demo If Statement to split into array
if(cookie) {
    var cookieBase = cookie.split(/[ \( \&\=\,ica,r,f,p,s| \)]+/);
    var cookieFinal = cookieBase.slice(1);
    console.log(cookieFinal)
} else {
    console.log('no cookie found');
}

//Setting Variables Based On Cookie
var age = parseInt(cookieFinal[0]);
var retirementAge = parseInt(cookieFinal[1]);
var initialBalance = parseInt(cookieFinal[2]);
var contributionValue = parseInt(cookieFinal[3]);
var annualWithdrawal = parseInt(cookieFinal[4]);

console.log('My initial cookie age is '+age+' I plan to cookie retire at '+retirementAge+' My initial cookie invest will be '+initialBalance+' My annual cookie contribution will be '+contributionValue);

//Global Elements/Values
var myChart = document.getElementById('myChart').getContext('2d');
var retirementHave = document.getElementsByClassName('retirement-have');
var retirementNeed = document.getElementsByClassName('retirement-need');
var updateChart = document.getElementById('update-chart');

//Investment Related Variables
var staticInterest = 0.031;

// Value Of Adds Calculation Values
var baseAgeDifference = retirementAge - age;

if(baseAgeDifference <= 0) {
    baseAgeDifference = 0;
}

var baseAgeValue = (1 + age) - age;


function calculateSavings(rateOfReturn) {

    ageData = [];
    savingsData = [];
    valueAddsData = [];
    paceData = [];
    var yearsInvested = 0;
    var yearCounter = 1;

    //console.log(initialBalance * Math.pow((1+rateOfReturn-staticInterest), baseAgeDifference));

    for(i = age; i <= 100; i++) {
        ageData.push(i);

        //Calculate Contribution
        var ageDifference = (i+1) - age;
        var inflationTotal = Math.pow((1+staticInterest), (ageDifference - 1));
        var savingsIncrease = (contributionValue * inflationTotal);
        savingsData.push(savingsIncrease);

        //Calculate Value Of Adds
        if(yearCounter <= baseAgeDifference) {
            var valueOfAdds = contributionValue * Math.pow((1+rateOfReturn-staticInterest), (yearsInvested + 1));
            valueAddsData.push(valueOfAdds);
            yearCounter++;
        } else {
            var finalValueAdd = valueAddsData.slice(-1)[0];
            valueAddsData.push(finalValueAdd);
        }

        yearsInvested++;
    }

    // Calculate Pace
    function postData() {
        var total = valueAddsData[0];
        var yearsInvestedPace = 1;
        var paceCalcArray = [initialBalance];
        var yearsAfterRetirement = 100 - retirementAge;
        console.log(rateOfReturn);


        // Calculate Pace Until Retirement Age Is Met
        for(var i = 0; i < baseAgeDifference; i++) {
            if(i === 0) {
                paceCalc = total + (initialBalance * Math.pow((1+rateOfReturn-staticInterest), yearsInvestedPace));
                paceCalcFormatted = parseInt(paceCalc.toFixed(0));
                paceCalcArray.push(paceCalcFormatted);
            } else {
                total += valueAddsData[i];
                paceCalc = total + (initialBalance * Math.pow((1+rateOfReturn-staticInterest), yearsInvestedPace));
                paceCalcFormatted = parseInt(paceCalc.toFixed(0));
                paceCalcArray.push(paceCalcFormatted);
                yearCounter++;
            }
            yearsInvestedPace++;
        }

        // Caluclate Final Pace Values and Start Annual Withdrawal
        //var finalInvestment = paceCalcArray.slice(-1)[0];
        var finalInvestment = paceCalcArray.slice(-1)[0];
        for(var l = 1; l <= yearsAfterRetirement; l++) {
            //finalInvestment += (finalInvestment * rateOfReturn) - annualWithdrawal;
            //finalInvestment = finalInvestment - (annualWithdrawal * Math.pow((1+staticInterest), l)) + (baseAgeDifference * (1+rateOfReturn - staticInterest));
            finalInvestment = (finalInvestment - (annualWithdrawal * Math.pow((1+staticInterest), l+baseAgeDifference))) * (1+rateOfReturn - staticInterest);
            finalInvestmentFormatted = parseInt(finalInvestment.toFixed(0));
            paceCalcArray.push(finalInvestmentFormatted);
        }


        //console.log(savingsData);
        //console.log(valueAddsData);
        console.log(paceCalcArray);

        return paceCalcArray;
    }

    var testArray = postData();
    return testArray;
}

// Populate Final Age Info
var paceTest = calculateSavings(0.07);
var paceLow = calculateSavings(0.02);
var paceMedium = calculateSavings(0.05);
var paceHigh = calculateSavings(0.08);

//Format Array Data To $ and , Versions For Chart
function formatArray(arrayName) {
    formattedArray = [];
    for(l = 0; l < arrayName.length; l++) {
        formattedArray.push('$'+toCommas(arrayName[l]));
    }

    return formattedArray;
}

var paceLowFormatted = formatArray(paceLow);
var paceMediumFormatted = formatArray(paceMedium);
var paceHighFormatted = formatArray(paceHigh);


// Run Array And Find Value 'f' to calculate years into retirement portfolio hits 0 or below
function calculateYearsIntoRetirement(paceArray) {
    for(f=0; f <= paceArray.length; f++) {
        if(paceArray[f] < 0) {
            var yearsIntoRetirement = f - baseAgeDifference - 1;
            return yearsIntoRetirement;
        } else {
            // Do nothing
        }
    }
}

// Run array to find value to calculate age at which portfolio hits 0
function calculateAgeAtRetirement(paceArray) {
    for(f=0; f <= paceArray.length; f++) {
        if(paceArray[f] < 0) {
            console.log('you are broke');
            var ageAtZero = age + f -1;
            return ageAtZero;
        } else {
            // Do nothing
        }
    }
}

var finalAge = document.getElementsByClassName('finalAge');
var investmentDuration = document.getElementsByClassName('investmentDuration');

function paceDurationLow() {
    var yearsIntoRetirement = calculateYearsIntoRetirement(paceLow);

    if(isNaN(retirementAge + yearsIntoRetirement)) {
        finalAge[0].innerHTML = '>= 100';
        investmentDuration[0].innerHTML = '>= 10';
    } else {
        finalAge[0].innerHTML = calculateAgeAtRetirement(paceLow);
        investmentDuration[0].innerHTML = yearsIntoRetirement;
    }
}

function paceDurationMedium() {
    var yearsIntoRetirement = calculateYearsIntoRetirement(paceMedium);
    if(isNaN(retirementAge + yearsIntoRetirement)) {
        finalAge[1].innerHTML = '>= 100';
        investmentDuration[1].innerHTML = '>= 10';
    } else {
            finalAge[1].innerHTML = calculateAgeAtRetirement(paceMedium);
            investmentDuration[1].innerHTML = yearsIntoRetirement;
    }
}

function paceDurationHigh() {
    var yearsIntoRetirement = calculateYearsIntoRetirement(paceHigh);
    if(isNaN(retirementAge + yearsIntoRetirement)) {
        finalAge[2].innerHTML = '>= 100';
        investmentDuration[2].innerHTML = '>= 10';
    } else {
            finalAge[2].innerHTML = calculateAgeAtRetirement(paceHigh);
            investmentDuration[2].innerHTML = yearsIntoRetirement;
    }
}

paceDurationLow();
paceDurationMedium();
paceDurationHigh();


//Grab Portfolio Value Based On Retirement Duration
var portfolioTarget = baseAgeDifference;
var portfolioValueLow = '$' + toCommas(paceLow[portfolioTarget].toFixed(0)); // 2%
var portfolioValueMedium = '$' + toCommas(paceMedium[portfolioTarget].toFixed(0)); //5%
var portfolioValueHigh = '$' + toCommas(paceHigh[portfolioTarget].toFixed(0)); //8%

var finalValue = document.getElementsByClassName('finalValue');
var estimatedText = document.getElementsByClassName('estimatedText');

function createDetails() {
    if(age >= retirementAge) {
        for(v = 0; v < estimatedText.length; v++) {
            estimatedText[v].innerHTML = "based on the current portfolio value of ";
        }

        for(v = 0; v < finalValue.length; v++) {
            finalValue[v].innerHTML = '$' + toCommas(initialBalance);
        }
    } else {
        for(v = 0; v < finalValue.length; v++) {
            if(v == 0) {
                finalValue[v].innerHTML = portfolioValueLow;
                console.log(portfolioValueLow);
            } else if (v == 1) {
                finalValue[v].innerHTML = portfolioValueMedium;
                console.log(portfolioValueMedium);
            } else {
                finalValue[v].innerHTML = portfolioValueHigh;
                console.log(portfolioValueHigh);
            }

            estimatedText[v].innerHTML = "with an estimated portfolio value at the time of retirement of ";
        }
    }
}

createDetails();

function checkZero() {
    if(initialBalance <= 0 && contributionValue <= 0 && annualWithdrawal <= 0) {
        paceData = [];
        savingsData = [];
        valueAddsData = [];

        for(v = 0; v < finalValue.length; v++) {
            if(v == 0) {
                finalValue[v].innerHTML = '0';
                finalAge[v].innerHTML = '0';
                investmentDuration[v].innerHTML = '0';
            } else if (v == 1) {
                finalValue[v].innerHTML = '0';
                finalAge[v].innerHTML = '0';
                investmentDuration[v].innerHTML = '0';
            } else {
                finalValue[v].innerHTML = '0';
                finalAge[v].innerHTML = '0';
                investmentDuration[v].innerHTML = '0';
            }
        }
    }
}

checkZero();

// Globl Options
Chart.defaults.global.defaultFontFamily = 'Roboto';
Chart.defaults.global.defaultFontSize = 18;
Chart.defaults.global.defaultFontColor = '#4c4c4c';

var demoChart = new Chart(myChart, {

    type: 'line', // bar, horizontal bar, pie, line, doughnut, radar, polarArea
    data: {
        labels: ageData,
        datasets:[{
            data: paceLow,
            label: '2%',
            backgroundColor: '#B6CCC2',
            pointHoverBackgroundColor: '#B6CCC2',
            borderWidth: 0,
            borderColor: 'rgba(0,0,0,0)',
            hoverBorderWidth: 3,
            hoverBorderColor: 'black',
            pointBackgroundColor: 'rgba(0,0,0,0)',
            pointRadius: 2
        },{
            data: paceMedium,
            label: '5%',
            backgroundColor: '#769D91',
            pointHoverBackgroundColor: '#769D91',
            borderWidth: 0,
            borderColor: 'rgba(0,0,0,0)',
            hoverBorderWidth: 3,
            hoverBorderColor: 'black',
            pointBackgroundColor: 'rgba(0,0,0,0)',
            pointRadius: 2
        },{
            data: paceHigh,
            label: '8%',
            backgroundColor: '#4D7F71',
            pointHoverBackgroundColor: '#4D7F71',
            borderWidth: 0,
            borderColor: 'rgba(0,0,0,0)',
            hoverBorderWidth: 3,
            hoverBorderColor: 'black',
            pointBackgroundColor: 'rgba(0,0,0,0)',
            pointRadius: 2
        }]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        hover: {
            intersect: false
        },
        title: {
            display: true,
            text: ['ESTIMATED PORTFOLIO VALUE', 'Based on annual returns of:'],
            fontSize: 20,
            padding: 30
        },
        legend: {
            display: true,
            position: 'top',
            labels: {
                fontColor: 'black'
            }
        },
        layout: {
            padding: {
                left: 0,
                right: 0,
                bottom: 0,
                top: 0
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    callback: function(label, index, labels) {
                        return '$' + nFormatter(label);
                    },
                    beginAtZero: true,
                    min: 0
                },
            }],
            xAxes: [{
            scaleLabel: {
                display: true,
                labelString: 'Age',
            },
            ticks: {
                autoSkip: true,
                maxTicksLimit: 6
            }
            }]
        },
        plugins: {
            datalabels: {
                // hide datalabels for all datasets
                display: false
            }
        },
        tooltips: {
            mode: 'point',
            displayColors: true,
            titleFontSize: 0,
            callbacks: {
                label: function(tooltipItem, data) {
                    var beforeLabel = data.labels[tooltipItem.index];
                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    return 'Age: ' + beforeLabel + ' Portfolio Value: $' +  toCommas(value);
                }
            }
        }
    }
});

//Append Custom Button To Form Test For Fisher
var formBlock = document.getElementsByClassName('form-builder')[0];
formBlock.innerHTML += '<button type="submit" id="update-chart">Recalculate</button>';
var updateChart = document.getElementById('update-chart');

updateChart.addEventListener('click', function(e) {
    e.preventDefault();

    checkZero();

    $('html').animate({
        scrollTop: $("#myChart").offset().top
    }, 2000);

    //Grab Initial Balance and values
    var valueAge = document.querySelector('input[data-calculator-key="ica"]');
    var valueRetire = document.querySelector('input[data-calculator-key="ira"]');
    var valueContribute = document.querySelector('input[data-calculator-key="ias"]');
    var valueInitialValue = document.querySelector('input[data-calculator-key="icp"]');
    var valueWithdrawal = document.querySelector('input[data-calculator-key="ircf"]');


    function grabValues() {
    var _age = parseInt(valueAge.value);
    var _retirementAge = parseInt(valueRetire.value);
    var _contributionValue = parseInt(valueContribute.value);
    var _initialBalance = parseInt(valueInitialValue.value);
    var valuesArray = [_age, _retirementAge, _contributionValue, _initialBalance];
    return valuesArray;
    }

    var formValues = grabValues();

    [valueAge, valueRetire, valueContribute, valueInitialValue].forEach(function(value) {
        value.addEventListener("input", function() {
            grabValues();
            formValues = grabValues();
            console.log(formValues);
            age = formValues[0];
            retirementAge = formValues[1];
            contributionValue = formValues[2];
            initialBalance = formValues[3];
        });
    });

    /* Overwirte Cookie Values */
    age = parseInt(valueAge.value);
    retirementAge = parseInt(valueRetire.value);
    initialBalance = parseInt(valueInitialValue.value);
    contributionValue = parseInt(valueContribute.value);
    annualWithdrawal = parseInt(valueWithdrawal.value);
    baseAgeDifference = retirementAge - age;

    if(baseAgeDifference <= 0) {
        baseAgeDifference = 0;
    }

    paceLow = calculateSavings(0.02);
    paceMedium = calculateSavings(0.05);
    paceHigh = calculateSavings(0.08);

    portfolioTarget = baseAgeDifference; // - 1 since arrays start at 0;
    portfolioValueLow = '$' + toCommas(paceLow[portfolioTarget].toFixed(0)); // 2%
    portfolioValueMedium = '$' + toCommas(paceMedium[portfolioTarget].toFixed(0)); //5%
    portfolioValueHigh = '$' + toCommas(paceHigh[portfolioTarget].toFixed(0)); //8%

    paceDurationLow();
    paceDurationMedium();
    paceDurationHigh();

    createDetails();

    setCookie('calculator_query_string','ica='+age+'&ira='+retirementAge+'&icp='+initialBalance+'&ias='+contributionValue+'&ircf='+annualWithdrawal,30);

    demoChart.data.datasets[0].data = paceLow;
    demoChart.data.datasets[1].data = paceMedium;
    demoChart.data.datasets[2].data = paceHigh;
    demoChart.data.labels = ageData;
    demoChart.update();
    console.log("Everything has been recalculated");
});