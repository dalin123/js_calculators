//Global variable to hold everything
window.fiCalc = window.fiCalc || {};
//Wrapping in IIFE to avoid function name collision in global window context
(function () {
    /************************************************************************************************/
    /****************************** Setup: Get/Set Cookie Functions *********************************/
    /************************************************************************************************/
    // get cookie value based on name
    function getCookieValue(key) {
        var val = document.cookie.match('(?:^|;?\s?)(' + key + ')=([^;]*);?');
        return val ? val[2] : null;
    }

    function setCookie(name, value) {
        document.cookie = name + "=" + value + ";path=/";
    }

    function handleCookieUpdate(cookieKey, newKey, newValue) {
        var oldValue = getCookie(cookieKey);
        var hasSpouseIndex = oldValue.split("&").indexOf("hasSpouse=");
        // doesn't exist or it's value is empty
        if (hasSpouseIndex > -1) {
            var newValue = oldValue + "&" + newKey + "=" + newValue;
            setCookie(cookieKey, newValue);
            console.log("Updated Cookie");
        }
    }

    // Live: Retirement/HSA User Inputs from DOM
    function parseCookieValueStringToArray(cookie) {
        // calculator_query_string cookie structure explained
        // Prefix: i = primary and s = spouse
        // Suffix: g = gender, ca = current age, ra = retirement age, le = life expectange/end age
        // Suffix: cp = current portfolio balance, hs = current health saving (HSA) balance
        // Suffix: as = annual portfolio contribution, ac = annual HSA contribution
        // ig=M&ica=50&ira=65&ile=75&icp=500000&ihs=19500&ias=20000&ihac=6000&sg=F&sca=45&sra=65&sle=85&scp=500000&shs=10000&sas=15000&shac=6000

        // get only values
        // cookie = cookie ? cookie.split(/(?:&?\w+=)/).slice(1) : null;
        // get key and value
        cookie = cookie.split(/(?:=|&)/);
        return cookie;
    }

    function buildObjectFromCookieArray(arr) {
        // Primary and Spouse each have 8 properties
        // cookie should have 16 properties for both
        // if non-empty spouseData exist in cookie
        // assumed the valid input values
        var hasSpouse = arr.length >= 32 && arr.slice(16).indexOf('') == -1;
        var keyToNameTable = {
            'g': 'gender',
            'ca': 'currentAge',
            'ra': 'retirementAge',
            'le': 'lifeExpectancy',
            'cp': 'currentInvestibleAssets',
            'as': 'additionsTillRetirement',
            'hs': 'currentHealthSavings',
            'hac': 'HSAadditionsTillRetirement'
        };

        // initialize default values
        var primaryData = {
            "gender": 'U',
            "currentAge": 0,
            "retirementAge": 0,
            "lifeExpectancy": 0,
            "currentInvestibleAssets": 0,
            "additionsTillRetirement": 0,
            "currentHealthSavings": 0,
            "HSAadditionsTillRetirement": 0,
        };
        var spouseData = {
            "gender": 'U',
            "currentAge": 0,
            "retirementAge": 0,
            "lifeExpectancy": 0,
            "currentInvestibleAssets": 0,
            "additionsTillRetirement": 0,
            "currentHealthSavings": 0,
            "HSAadditionsTillRetirement": 0,
            "is_spouse": true
        };
        // set values
        for (var i = 0; i < arr.length && arr.length % 2 == 0; i += 2) {
            var prefix = arr[i][0];
            var key = arr[i].slice(1);
            var name = keyToNameTable[key];
            var value = arr[i + 1];
            value = isNaN(value) ? value : parseInt(value);

            if (prefix == 'i') {
                primaryData[name] = value;
            } else if (prefix == 's') {
                spouseData[name] = value;
            } else {
                console.log('CalcError: buildObjectFromCookieArray fn');
            }
        }
        var data = {
            "primary": primaryData,
            "spouse": spouseData,
            "hasSpouse": hasSpouse,
        };
        //Update window object with the new inputs
        window.fiCalc.inputs = data;
        return data;
    }

    function getCalculatorQueryString() {
        var val = getCookieValue('calculator_query_string');
        val = parseCookieValueStringToArray(val);
        val = buildObjectFromCookieArray(val);
        return val;
    }

    // Convert to cookie value format for primary and spouse data
    function convertToCookieValue(primaryData, spouseData, hasSpouse) {
        var keyTable = {
            'gender': 'g',
            'currentAge': 'ca',
            'retirementAge': 'ra',
            'lifeExpectancy': 'le',
            'currentInvestibleAssets': 'cp',
            'additionsTillRetirement': 'as',
            'currentHealthSavings': 'hs',
            'HSAadditionsTillRetirement': 'hac'
        };
        var cookie = '';
        Object.keys(keyTable).forEach(function (key, index, arr) {
            cookie += 'i' + keyTable[key] + '=' + primaryData[key] + '&';
        });
        //Add the spouse values to cookie
        if (hasSpouse) {
            //Add the spouse flag
            cookie += 'hasSpouse=true&';
            //Add the spouse inputs
            Object.keys(keyTable).forEach(function (key, index, arr) {
                cookie += 's' + keyTable[key] + '=' + spouseData[key] + '&';
            });
        } else {
            //Scenarios where hasSpouse already exists or needs to be added
            if (cookie.indexOf('hasSpouse=true' > -1)) {
                //update spouse flag to false
                cookie.replace('hasSpouse=true', 'hasSpouse=false');
            } else if (cookie.indexOf('hasSpouse=false' > -1)) {
                //spouse flag is set correctly
            } else {
                //set spouse flag to false
                cookie += 'hasSpouse=false&';
            }
        }
        return cookie.slice(0, -1);
    }

    function writeToCookie(key, data) {
        var cookieValStr = convertToCookieValue(data.primary, data.spouse, data.hasSpouse);
        document.cookie = key + '=' + cookieValStr + ';path=/';
    }

    /************************************************************************************************/
    /****************************** Setup: Get/Set Html Input Functions *********************************/
    /************************************************************************************************/
    // Set Html input elements with cookie data for primary and spouse
    function populateHtmlInputs(primary, spouse, hasSpouse) {
        document.getElementById('genderP').value = fiCalc.inputs.primary.gender;
        document.getElementById("startAgeP").value = primary.currentAge;
        document.getElementById("retireAgeP").value = primary.retirementAge;
        document.getElementById("endAgeP").value = primary.lifeExpectancy;
        document.getElementById("startBalanceP").value = toCommas(primary.currentInvestibleAssets);
        document.getElementById("startHsaP").value = toCommas(primary.currentHealthSavings);
        document.getElementById("annualContributeP").value = toCommas(primary.additionsTillRetirement);
        document.getElementById("annualHsaP").value = toCommas(primary.HSAadditionsTillRetirement);
        if (hasSpouse) {
            document.getElementById('genderS').value = fiCalc.inputs.spouse.gender;
            document.getElementById("startAgeS").value = spouse.currentAge;
            document.getElementById("retireAgeS").value = spouse.retirementAge;
            document.getElementById("endAgeS").value = spouse.lifeExpectancy;
            document.getElementById("startBalanceS").value = toCommas(spouse.currentInvestibleAssets);
            document.getElementById("startHsaS").value = toCommas(spouse.currentHealthSavings);
            document.getElementById("annualContributeS").value = toCommas(spouse.additionsTillRetirement);
            document.getElementById("annualHsaS").value = toCommas(spouse.HSAadditionsTillRetirement);
        }
        //Add commas to form inputs
        document.querySelectorAll('input[type="text"]').forEach(function (el) {
            el.addEventListener('change', function () {
                var currentInput = this.value.replace(/\W/g, '');
                var fixedInput = toCommas(currentInput);
                this.value = fixedInput;
            });
        });
    }

    // Get html input elements for recalculate purposes
    function getHtmlInputs() {
        var genderArr = [
            'n/a',
            'F',
            'M',
            'Intersex',
            'OptOut'
        ];

        var primaryData = {
            "gender": genderArr[document.getElementById('genderP').selectedIndex],
            "currentAge": parseInt(document.getElementById("startAgeP").value),
            "retirementAge": parseInt(document.getElementById("retireAgeP").value),
            "lifeExpectancy": parseInt(document.getElementById("endAgeP").value),
            "currentInvestibleAssets": parseInt(document.getElementById("startBalanceP").value.replace(/\W/g, '')),
            "additionsTillRetirement": parseInt(document.getElementById("annualContributeP").value.replace(/\W/g, '')),
            "currentHealthSavings": parseInt(document.getElementById("startHsaP").value.replace(/\W/g, '')),
            "HSAadditionsTillRetirement": parseInt(document.getElementById("annualHsaP").value.replace(/\W/g, ''))
        };
        var spouseData = {
            "gender": genderArr[document.getElementById('genderS').selectedIndex],
            "currentAge": parseInt(document.getElementById("startAgeS").value),
            "retirementAge": parseInt(document.getElementById("retireAgeS").value),
            "lifeExpectancy": parseInt(document.getElementById("endAgeS").value),
            "currentInvestibleAssets": parseInt(document.getElementById("startBalanceS").value.replace(/\W/g, '')),
            "additionsTillRetirement": parseInt(document.getElementById("annualContributeS").value.replace(/\W/g, '')),
            "currentHealthSavings": parseInt(document.getElementById("startHsaS").value.replace(/\W/g, '')),
            "HSAadditionsTillRetirement": parseInt(document.getElementById("annualHsaS").value.replace(/\W/g, '')),
            "is_spouse": true
        };
        //Use checkSpouseInputs() to handle. This function checks if the spouse section is expanded and that all inputs have a valid input
        var hasSpouse = checkSpouseInputs();
        //If no spouse then set all the spouse inputs to 0
        if (!hasSpouse) {
            spouseData = {
                "gender": 'U',
                "currentAge": 0,
                "retirementAge": 0,
                "lifeExpectancy": 0,
                "currentInvestibleAssets": 0,
                "additionsTillRetirement": 0,
                "currentHealthSavings": 0,
                "HSAadditionsTillRetirement": 0,
                "is_spouse": true
            };
        }
        var data = {
            "primary": primaryData,
            "spouse": spouseData,
            "hasSpouse": hasSpouse
        };
        //Update window object with the new inputs
        window.fiCalc.inputs = data;
        return data;
    }

    // Test/DEMO: Retirement/HSA User Inputs from DOM
    function getTestInput() {
        // **************** Test Data A: fixed data dummy ****************
        // var hasSpouse = false;
        // var primaryData = {
        // "currentAge": 50,
        // "retirementAge": 60,
        // "gender": 'M',
        // "lifeExpectancy": 90,
        // "currentInvestibleAssets": 500000,
        // "additionsTillRetirement": 30000,
        // "currentHealthSavings": 50000,
        // "HSAadditionsTillRetirement": 1000
        // };
        // var spouseData = {
        // "currentAge": 0,
        // "retirementAge": 0,
        // "gender": 'F',
        // "lifeExpectancy": 0,
        // "currentInvestibleAssets": 0,
        // "additionsTillRetirement": 0,
        // "currentHealthSavings": 0,
        // "HSAadditionsTillRetirement": 0,
        // "is_spouse": true
        // };
        // **************** Test Data B: cookie data dummy ****************
        //calculator_query_string=ig=M&ica=50&ira=65&ile=75&icp=500000&ihs=20000&ias=19500&ihac=6000&sg=F&sca=45&sra=65&sle=85&scp=500000&shs=15000&sas=10000&shac=6000
        // calculator_query test lpf116.js'ig=F&ica=70&ira=65&ile=91&icp=1000000&ihs=50000&ias=0&ihac=0&sg=M&sca=60&sra=70&sle=95&scp=500000&shs=0&sas=0&shac=0';

        var cookie_test_both = 'ig=F&ica=70&ira=65&ile=91&icp=1000000&ihs=50000&ias=0&ihac=0&sg=M&sca=60&sra=70&sle=95&scp=50000&shs=0&sas=0&shac=0';
        var cookie_test_primary = 'ig=F&ica=70&ira=65&ile=91&icp=500000&ihs=30000&ias=0&ihac=0';
        var cookie_test_NaN = 'ig=F&ica=58&ira=68&ile=78&icp=&ihs=0&ias=0&ihac=0';
        var cookie = cookie_test_primary;
        cookie = parseCookieValueStringToArray(cookie);
        var inputObject = buildObjectFromCookieArray(cookie);

        return inputObject;
    }

    /************************************************************************************************/
    /*********************************** Setup: get Initial Render Input *************************************/
    /************************************************************************************************/
    /**
     * get initialRenderInputs() grabs values from the cookie
     * based on incoming preprocessed data
     * Input: 
     * Output: 
     */
    function initialRenderInputs() {
        var cookie = getCookieValue("calculator_query_string");
        cookie = parseCookieValueStringToArray(cookie);
        var inputObject = buildObjectFromCookieArray(cookie);

        return inputObject;
        // var genderArr = [
        //     'n/a',
        //     'F',
        //     'M',
        //     'Intersex',
        //     'OptOut'
        // ];

        // var primaryData = {
        //     "gender": genderArr[document.getElementById('genderP').selectedIndex],
        //     "currentAge": parseInt(document.getElementById("startAgeP").value),
        //     "retirementAge": parseInt(document.getElementById("retireAgeP").value),
        //     "lifeExpectancy": parseInt(document.getElementById("endAgeP").value),
        //     "currentInvestibleAssets": parseInt(document.getElementById("startBalanceP").value),
        //     "additionsTillRetirement": parseInt(document.getElementById("annualContributeP").value),
        //     "currentHealthSavings": parseInt(document.getElementById("startHsaP").value),
        //     "HSAadditionsTillRetirement": parseInt(document.getElementById("annualHsaP").value)
        // };
        // var spouseData = {
        //     "gender": genderArr[document.getElementById('genderS').selectedIndex],
        //     "currentAge": parseInt(document.getElementById("startAgeS").value),
        //     "retirementAge": parseInt(document.getElementById("retireAgeS").value),
        //     "lifeExpectancy": parseInt(document.getElementById("endAgeS").value),
        //     "currentInvestibleAssets": parseInt(document.getElementById("startBalanceS").value),
        //     "additionsTillRetirement": parseInt(document.getElementById("annualContributeS").value),
        //     "currentHealthSavings": parseInt(document.getElementById("startHsaS").value),
        //     "HSAadditionsTillRetirement": parseInt(document.getElementById("annualHsaS").value),
        //     "is_spouse": true
        // };

        // var hasSpouse = spouseData.gender && (spouseData.currentAge > 0);
        // hasSpouse = hasSpouse && (spouseData.retirementAge > 0) && (spouseData.lifeExpectancy > 0);

        // if (!hasSpouse) {
        //     spouseData = {
        //         "gender": 'U',
        //         "currentAge": 0,
        //         "retirementAge": 0,
        //         "lifeExpectancy": 0,
        //         "currentInvestibleAssets": 0,
        //         "additionsTillRetirement": 0,
        //         "currentHealthSavings": 0,
        //         "HSAadditionsTillRetirement": 0,
        //         "is_spouse": true
        //     };
        // }

        // return {
        //     "primary": primaryData,
        //     "spouse": spouseData,
        //     "hasSpouse": hasSpouse
        // };
    }

    /************************************************************************************************/
    /*************************** Setup: Validate Html Input Functions *******************************/
    /************************************************************************************************/
    function isFormValid(formId) {
        var f = document.getElementById(formId);
        return f;
    }

    /************************************************************************************************/
    /*********************************** Setup: Show Secondary Tabs *************************************/
    /************************************************************************************************/
    /**
     * ?showSecondaryTabs() inserts values into dynamic breakdown
     * ?based on incoming preprocessed data
     * ?Input: 
     * ?Output:
     * Show/hide the spouse and combined tabs for the cost breakdown 
     * @param person {string} primary or combined
     * @returns show/hides the yearly costs tabs
     */
    function showSecondaryTabs(person) {
        //Hide the spouse and combined tabs
        if (person === 'primary') {
            //Don't need to querySelector and querySelectorAll here. Combined into a single statement
            document.querySelectorAll('#spouseYearly, #combinedYearly').forEach(function (node, index) {
                //Hide the tabs if they aren't hidden already
                if (!node.classList.contains('hide')) {
                    node.classList.add("hide");
                }
            });
        }
        if (person === 'combined') {
            document.querySelectorAll('#spouseYearly, #combinedYearly').forEach(function (node, index) {
                //Show the spouse and combined tabs if they're hidden
                if (node.classList.contains('hide')) {
                    node.classList.remove("hide");
                }
            });
        }
        //Make the primary tab the active one in case the user switched tabs. This makes sure tab and breakdown data are in sync 
        if (!document.querySelector('#primaryYearly').classList.contains('active')) {
            //Add the active class to the tab and activate the correct panel
            document.querySelectorAll('#primaryYearly, #i-estmcosts-tab').forEach(function (node, index) {
                node.classList.add('active');
            });
            //Remove the active class from whatever tab and panel has it to avoid two active tabs and panel conflict 
            document.querySelectorAll('#spouseYearly, #combinedYearly, #s-estmcosts-tab, #c-estmcosts-tab').forEach(function (node, index) {
                if (node.classList.contains('active')) {
                    node.classList.remove('active');
                }
            });

        }
    }
    /************************************************************************************************/
    /*********************************** Setup: Dynamic Breakdown ***********************************/
    /************************************************************************************************/
    /**
     * Updates the year estimated cost breakdown chart
     * @param {object} data the data we need to parse out 
     * @param {string} who the person(s) who need to breakdown for
     */
    function insertBreakDown(data, who) {
        var targetClassNames;
        var breakdownClassNames = [".oopc-ltf-amt", ".oopc-snf-amt", ".oopc-pd-amt", ".oopc-ps-amt", ".oopc-den-amt", ".oopc-os-amt", ".oopc-is-amt", ".oopc-oth-amt"];
        var spouseBreakdownClassNames = [".oopc-ltf-amt-s", ".oopc-snf-amt-s", ".oopc-pd-amt-s", ".oopc-ps-amt-s", ".oopc-den-amt-s", ".oopc-os-amt-s", ".oopc-is-amt-s", ".oopc-oth-amt-s"];
        var combinedBreakdownClassNames = [".oopc-ltf-amt-c", ".oopc-snf-amt-c", ".oopc-pd-amt-c", ".oopc-ps-amt-c", ".oopc-den-amt-c", ".oopc-os-amt-c", ".oopc-is-amt-c", ".oopc-oth-amt-c"];
        //Process corect order due to src table not being neat 
        var orderedTitle = [["Premiums - $", data[3]], ["Out of Pocket Costs - $", data[4]], ["Yearly Total - $", data[2]]];
        var startIndex = 5;
        var breakDownMap = {
            "primary": [],
            "spouse": [],
            "combined": []
        };
        //For overall title
        $("h3.h-m").each(function (index, val) {
            if (index < 3) {
                breakDownMap.primary.push(val);
            } else if (index >= 3 && index < 6) {
                breakDownMap.spouse.push(val);
            } else {
                breakDownMap.combined.push(val);
            }
        });

        if (who === "primary") {
            breakDownMap.primary.forEach(function (element, index) {
                $(element).html(orderedTitle[index][0] + Math.round(orderedTitle[index][1]).toLocaleString("en-US"));
            });
            targetClassNames = breakdownClassNames;
        } else if (who === "spouse") {
            breakDownMap.spouse.forEach(function (element, index) {
                $(element).html(orderedTitle[index][0] + Math.round(orderedTitle[index][1]).toLocaleString("en-US"));
            });
            targetClassNames = spouseBreakdownClassNames;
        } else if (who === "combined") {
            breakDownMap.combined.forEach(function (element, index) {
                $(element).html(orderedTitle[index][0] + Math.round(orderedTitle[index][1]).toLocaleString("en-US"));
            });
            targetClassNames = combinedBreakdownClassNames;
        }
        //TODO: Why do we use breakdownClassNames.length, but loop through targetClassNames?
        // upgrade data array to 2021 inflation
        for (var i = 0; i < breakdownClassNames.length; i++) {
            $(targetClassNames[i]).text("$" + Math.round(data[startIndex]).toLocaleString("en-US"));
            startIndex++;
            //updatedOther = "long term care" + "skilled nursing" + "other". We lump these categories together on the breakdown.
            var updatedOther = data[5] + data[6] + data[12];
            //Update "other" estimate. It's the last thing in the array
            $(targetClassNames[targetClassNames.length - 1]).text("$" + Math.round(updatedOther).toLocaleString("en-us"));
        }
    }

    /************************************************************************************************/
    /*********************************** Setup: Dynamic Text Inserter *************************************/
    /************************************************************************************************/
    /**
     * insertDynamicText() inserts values into dynamic breakdown
     * based on incoming preprocessed data
     * Input: 
     * Output: 
     */
    function addDynamicTexts(data, who) {
        var dynamicSpanIds = ["#tot-hc-cost", "#hsa-sav-tot", "#oop-cost", "#est-cost-intro", "#i-age", "#i-ret"];
        var spanLib;
        var hasHsaFlag = window.fiCalc.all.hasHsaFlag;
        var hasOopCosts = window.fiCalc.all.lifetimeOop > 0 ? true : false;
        var oopCostHtml = document.querySelector('[data-calc="oopCosts"]');
        var noOopHtml = document.querySelector('[data-calc="noOop"]');
        var qualHtml = document.querySelector('[data-calc="q"]');
        var before65Html = document.querySelector('[data-calc="ret64"]');
        var after65Html = document.querySelector('[data-calc="ret65"]');
        var hasHsaHtml = document.querySelectorAll('.hasHsa, [data-calc="noHsa"]');
        var noHsaHtml = document.querySelectorAll('.noHsa, [data-calc="noHsa"]');

        //?? Todo: Support adding dynamic tests for combined users
        if (who === "primary") {
            spanLib = {
                "tot-hc-cost": data.lifetimeHcCosts.toLocaleString("en-us"),
                "hsa-sav-tot": data.lifetimePercentCovered,
                "oop-cost": data.lifetimeOop.toLocaleString("en-us"),
                "est-cost-intro": data.input.primary.currentAge >= data.input.primary.retirementAge ? "We see that you're currently " + data.input.primary.currentAge + ". " : "We see you've chosen to retire at age " + data.input.primary.retirementAge + ". ",
                "i-age": data.input.primary.currentAge >= data.input.primary.retirementAge ? data.input.primary.currentAge : data.input.primary.retirementAge,
                "i-ret": data.input.primary.currentAge >= data.input.primary.retirementAge ? " retired" : " not retired"
            };
        } else if (who === "combined") {
            spanLib = {
                "tot-hc-cost": data.lifetimeHcCosts.toLocaleString("en-us"),
                "hsa-sav-tot": data.lifetimePercentCovered,
                "oop-cost": data.lifetimeOop.toLocaleString("en-us"),
                "est-cost-intro": data.input.primary.currentAge >= data.input.primary.retirementAge ? "We see that you're currently " + data.input.primary.currentAge + ". " : "We see you've chosen to retire at age " + data.input.primary.retirementAge + ". ",
                "i-age": data.input.primary.currentAge >= data.input.primary.retirementAge ? data.input.primary.currentAge : data.input.primary.retirementAge,
                "i-ret": data.input.primary.currentAge >= data.input.primary.retirementAge ? " retired" : " not retired"
            };
        } else {
            console.log('Error processing addDynamicTexts()');
        }
        document.querySelectorAll(dynamicSpanIds).forEach(function (val, index) {
            val.textContent = spanLib[val.id];
        });
        //Show the OOP costs content
        if (hasOopCosts) {
            //Remove the hide class to display
            oopCostHtml.classList.remove('hide');
            //Hide the no OOP content if it's not hidden already
            if (!noOopHtml.classList.contains('hide')) {
                noOopHtml.classList.add('hide');
            }
        }
        //Show the NO OOP costs content
        else {
            //Show the NO OOP content
            noOopHtml.classList.remove('hide');
            //Hide the OOP content if it's not hidden alrady
            if (!oopCostHtml.classList.contains('hide')) {
                oopCostHtml.classList.add('hide');
            }
        }
        //Show the qualified content
        if (response_quality == "Qualified") {
            qualHtml.classList.remove('hide');
        }
        //Show the retire before 65 content
        if (fiCalc.inputs.primary.retirementAge < 65) {
            before65Html.classList.toggle('hide');
        }
        //Show the retire after 65 content 
        if (fiCalc.inputs.primary.retirementAge >= 65) {
            after65Html.classList.toggle('hide');
        }
        //Show the dynamic HSA content if the user actually has one
        if (hasHsaFlag && fiCalc.inputs.primary.currentAge < 65) {
            //Remove the hide class to display
            hasHsaHtml.forEach(function (el) {
                el.classList.remove('hide');
            });
            //Hide the NO HSA content if it's not hidden
            if (!noHsaHtml.classList.contains('hide')) {
                noHsaHtml.classList.add('hide');
            }
        }
        //If NO HSA show the NO HSA content and hide the HSA content
        else {
            //Show the NO HSA content
            noHsaHtml.forEach(function (el) {
                if (el.classList.contains('hide')) {
                    el.classList.remove('hide');
                }
            });
            //Hide the HSA content if it's not hidden
            hasHsaHtml.forEach(function (el) {
                if (!el.classList.contains('hide')) {
                    el.classList.add('hide');
                }
            });
        }
    }

    /************************************************************************************************/
    /*********************************** Setup: Chart Functions *************************************/
    /************************************************************************************************/
    function genderForCalc(val) {
        switch (val) {
            case 'Intersex':
            case 'OptOut':
            case 'non-binary':
            case 'n/a':
            case 'I':
            case '0':
                val = 'U';
                break;
            default:
                val = val;
        }
        return val;
    }

    function getHealthCareData(inputFn, assumeFn, calcFn) {
        // Assumption data
        var assume = assumeFn();
        // get html input values on recalculate
        // Get Primary and Spouse Retirement/HSA Inputs From cookie
        // var input = parseCookieInput("calculator_query_string");
        // var input = getTestInput();
        var input = inputFn();
        // convert gender for calculation
        var genderP = input.primary.gender;
        var genderS = input.spouse.gender;
        input.primary.gender = genderForCalc(genderP);
        input.spouse.gender = genderForCalc(genderS);
        // calculate healthcare cost data
        var data = calcFn(
            assume.portfolioReturnRate, assume.healthcareCostInflationRate, assume.hsaReturnRate,
            assume.ageGenderCostTable, input.primary, input.spouse, input.hasSpouse);
        // change gender back
        input.primary.gender = genderP;
        input.spouse.gender = genderS;
        // insert input and assume data to the data
        data.input = input;
        data.assume = assume;
        return data;
    }

    function getDummyArray(length) {
        var i;
        var arr = [];
        for (i = 0; i < length; i++) {
            arr.push('VALUE HERE');
        }
        return arr;
    }

    function getXAxisTitle(curAge, retAge) {
        // Get x-axis title
        // primary is not retired yetgi
        var xAxisTitle = 'Years After Retirement';
        // if primary is already retired,
        if (curAge > retAge) {
            xAxisTitle = 'Years From Today';
        }

        return xAxisTitle;
    }

    function getFontSize() {
        // calc fontSize based on viewport
        var vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
        var vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);
        var fontSize = Math.min(vw, vh) * 0.015;
        fontSize = Math.max(fontSize, 12);
        return fontSize;
    }

    function xAxisTicksCallback(input) {
        return function (val, index, arr) {
            var currentAge = input.primary.currentAge;
            var retireAge = input.primary.retirementAge;
            var startAge = currentAge > retireAge ? currentAge : retireAge;
            // Primary life exepectancy year
            var endYear = input.primary.lifeExpectancy - startAge;
            if (val == endYear) {
                return val + "*";
            } else {
                return val;
            }
        };
    }

    // Create Chart from Config and age, out-of-pocket, and HSA coverage data
    function createChart(htmlElementId, ageData, oopData,
        coveredByHsaData, hcPercentData, input) {
        // Set global defaults for Chart
        Chart.defaults.global.defaultFontSize = getFontSize();
        Chart.defaults.global.defaultFontFamily = 'Roboto';
        Chart.defaults.global.defaultFontColor = '#4c4c4c';
        // et X-Axis label
        var xAxisTitle = getXAxisTitle(input.primary.currentAge, input.primary.retirementAge);

        // initial chart config object for bar graph
        var chartConfig = {
            type: 'bar', // bar, horizontal bar, pie, line, doughnut, radar, polarArea
            data: {
                labels: ageData,
                datasets: [{
                    data: coveredByHsaData,
                    backgroundColor: '#769d91',
                    label: 'Covered by HSA',
                    pointHoverBackgroundColor: '#769d91',
                    borderWidth: 0,
                    borderColor: 'rgba(0,0,0,0)',
                    hoverBorderWidth: 3,
                    hoverBorderColor: 'black',
                    pointBackgroundColor: 'rgba(0,0,0,0)',
                    pointRadius: 2
                }, {
                    data: oopData,
                    backgroundColor: '#7C2529',
                    label: 'Out of Pocket',
                    pointHoverBackgroundColor: '#7C2529',
                    borderWidth: 0,
                    borderColor: 'rgba(0,0,0,0)',
                    hoverBorderWidth: 3,
                    hoverBorderColor: 'black',
                    pointBackgroundColor: 'rgba(0,0,0,0)',
                    pointRadius: 2
                }, {
                    data: hcPercentData,
                    hidden: true,
                    label: 'NoLabel',
                }]
            },
            plugins: [{
                resize: function (chart, options) {
                    // make variable font size
                    var fontSize = getFontSize();
                    Chart.defaults.global.defaultFontSize = fontSize;
                }
            },],
            options: {
                responsive: true,
                maintainAspectRatio: false,
                hover: {
                    intersect: false
                },
                title: {
                    display: true,
                    text: ['ESTIMATED YEARLY HEALTHCARE COSTS IN RETIREMENT'],
                    padding: 30
                },
                legend: {
                    display: true,
                    position: 'bottom',
                    labels: {
                        fontColor: 'black',
                        boxWidth: 50,
                        boxHeight: 50,
                        filter: function (item, Chart) {
                            // Logic to remove a particular legend item goes here
                            return (item.text.indexOf('NoLabel') == -1);

                        },
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        bottom: 0,
                        top: 0
                    }
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        scaleLabel: {
                            display: true,
                            labelString: xAxisTitle
                        },
                        ticks: {
                            // for tick labels format
                            callback: xAxisTicksCallback(input)
                        }
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            // for tick labels format
                            callback: function (val, index, arr) {
                                return '$' + toCommas(val);
                            }
                        },
                        maxTicksLimit: 13,
                    }],

                },
                plugins: {
                    datalabels: {
                        // hide datalabels for all datasets
                        display: false
                    },
                },
                tooltips: {
                    mode: 'point',
                    displayColors: true,
                    titleFontSize: 0,
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var ageLabel = String(data.labels[tooltipItem.index]).split(";")[0];
                            ageLabel = "Year: " + ageLabel;
                            var costLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            costLabel = "Cost: $" + toCommas(costLabel);
                            var HcPercentLabel = data.datasets[2].data[tooltipItem.index];
                            if (HcPercentLabel.indexOf && HcPercentLabel.indexOf('NaN') > -1) {
                                HcPercentLabel = 'n/a';
                            }
                            HcPercentLabel = 'Cost as % of Portfolio Income: ' + HcPercentLabel;
                            return [ageLabel, costLabel, HcPercentLabel];
                        }
                    }
                }
            }
        };
        //Get canvas element as context
        var ctx = document.getElementById(htmlElementId).getContext('2d');
        // Create chart with canvas context and config objects
        var healthcareCostChart = new Chart(ctx, chartConfig);
        return healthcareCostChart;
    }

    function updateChart(chart, ageData, oopData, coveredByHsaData, hcPercentData, input) {
        var xAxisTitle = getXAxisTitle(input.primary.currentAge, input.primary.retirementAge);
        chart.options.scales.xAxes[0].scaleLabel.labelString = xAxisTitle;
        chart.options.scales.xAxes[0].ticks.callback = xAxisTicksCallback(input);
        chart.data.labels = ageData;
        chart.data.datasets[0].data = oopData;
        chart.data.datasets[1].data = coveredByHsaData;
        chart.data.datasets[2].data = hcPercentData;
        chart.update({ duration: 0 });
    }

    /************************************************************************************************/
    /************************* Helper: Check spouse fields if they exist *****************************/
    /************************************************************************************************/
    /** 
    * Check the cookie for hasSpouse parameter 
    * @return {boolean}
    */
    var checkCookieForSpouse = function () {
        //Get the main cookie
        var calcCookie = getCookie('calculator_query_string');
        //Check if it hasSpouse in it
        if (calcCookie.indexOf('hasSpouse') > -1) {
            //Get the value by slicing until the next delimitter. This is dangerous because if the
            //cookie string contains hasSpouse at all we'll have a conflict
            //Todo: we should check the "calculator_query_string" cookie for hasSpouse instead 
            var hasSpouseCookieVal = getCookieValue('hasSpouse').slice(0, getCookieValue('hasSpouse').indexOf('&'));
            //if the parameter is true then it must be so
            return hasSpouseCookieVal === 'true' ? true : false;
        } else {
            return false;
        }
    };
    /**
    * Check if the spouse edit details section is expanded and that all input fields are filled out on TY
    * @return {boolean} true or false 
    */
    var checkSpouseInputs = function () {
        //All the spouse related HTML inputs - used in checkForSpouse and spouseErrorMessageHandling()
        var spouseSection = document.querySelector("#secondaryInfoBlock");
        var spouseGender = document.querySelector("#genderS");
        var spouseAge = document.querySelector("#startAgeS");
        var spouseRetirementAge = document.querySelector("#retireAgeS");
        var spouseLifeExpectancy = document.querySelector("#endAgeS");
        var spouseRetirementSavings = document.querySelector("#startBalanceS");
        var spouseHsaAccount = document.querySelector("#startHsaS");
        var spouseAnnualContributions = document.querySelector("#annualContributeS");
        var spouseHsaContributions = document.querySelector("#annualHsaS");
        //Combine all the above into an array to use in the functions
        var spouseInputsArray = [spouseGender, spouseAge, spouseRetirementAge, spouseLifeExpectancy, spouseRetirementSavings, spouseHsaAccount, spouseAnnualContributions, spouseHsaContributions];
        //Check that all the spouse input fields have been answered
        var isValid;
        //Check that the spouse section is expanded (ariaExpanded for Chrome. Classlist for Firefox)
        var isSpouseExpanded = spouseSection.ariaExpanded === "true" || spouseSection.classList.contains("in") ? true : false;
        spouseInputsArray.forEach(function (el) {
            if (el.value === null || el.value === '') {
                isValid = false;
                return;
            } else {
                isValid = true;
            }
        });
        //Check that the spouse section is expanded and that all the spouse inputs have a value
        if (isSpouseExpanded && isValid) {
            window.fiCalc.inputs.hasSpouse = true;
            return true;
        } else {
            window.fiCalc.inputs.hasSpouse = false;
            return false;
        }
    };
    /************************************************************************************************/
    /************************* TY Page: Error Message Handling **************************************/
    /************************************************************************************************/
    /**
     * Takes advantage of HTML5 built-in input error message by adding the input required attribute 
     * to the spouse inputs when addSpouse section is expanded on TY page recalculate. 
     * Removes the required attribute when the user clicks remove spouse and the section is not expanded
     * so when the user clicks Recalculate for only primary we don't have a conflict
     */
    function spouseErrorMessageHandling() {
        //All the spouse related HTML inputs - used in checkForSpouse and spouseErrorMessageHandling()
        var spouseSection = document.querySelector("#secondaryInfoBlock");
        var spouseGender = document.querySelector("#genderS");
        var spouseAge = document.querySelector("#startAgeS");
        var spouseRetirementAge = document.querySelector("#retireAgeS");
        var spouseLifeExpectancy = document.querySelector("#endAgeS");
        var spouseRetirementSavings = document.querySelector("#startBalanceS");
        var spouseHsaAccount = document.querySelector("#startHsaS");
        var spouseAnnualContributions = document.querySelector("#annualContributeS");
        var spouseHsaContributions = document.querySelector("#annualHsaS");
        //Combine all the above into an array to use in the functions
        var spouseInputsArray = [spouseGender, spouseAge, spouseRetirementAge, spouseLifeExpectancy, spouseRetirementSavings, spouseHsaAccount, spouseAnnualContributions, spouseHsaContributions];
        //Add the required attribute when Add Spouse is clicked
        document.querySelector("#addSpouseBtn").addEventListener('mousedown', function () {
            spouseInputsArray.forEach(function (el) {
                el.setAttribute("required", "");
            });
        });
        //Remove the required attribute when the spouse section is removed
        document.querySelector("#removeSpouseBtn").addEventListener('mousedown', function () {
            spouseInputsArray.forEach(function (el) {
                //We only need to move required if it's on the input
                if (el.required === true) {
                    el.removeAttribute("required");
                }
            });
        });
    }

    /************************************************************************************************/
    /************************* Main: Calculation and Render Handlers *****************************/
    /************************************************************************************************/

    /** 
     * Handle the initial pageload calculation and render.
     * This handles how we check for spouse differently than validateAndUpdate() below
     * TODO: see if we can combine initialCalcAndRender() and validateAndUpdate()
     * */
    function initialCalcAndRender() {
        // Calculate healthcare cost data and getCalculatorQueryString
        var data = getHealthCareData(
            initialRenderInputs,
            getAssumption,
            calculateSavings
        );
        //Default to the primary. We update this to "combined" if hasSpouse is true;
        var person = "primary";
        //Populate dynamic breakdown here
        insertBreakDown(data.primaryCostBreakdown, "primary");
        //Check the cookie to see if hasSpouse is included from the input page. We handle checking for hasSpouse differently on the intial pageload vs recalculate. 
        var initialHasSpouse = checkCookieForSpouse();
        if (initialHasSpouse) {
            console.log('Include spouse');
            //?show tabs here
            //I think this combines the data we need
            var combinedData = data.primaryCostBreakdown.map(function (ele, index) {
                return ele + data.spouseCostBreakDown[index];
            });
            insertBreakDown(data.spouseCostBreakDown, "spouse");
            insertBreakDown(combinedData, "combined");
            //Person is now combined
            person = "combined";
        }
        // Populate Primary and Spouse input elements in modal form
        populateHtmlInputs(data.input.primary, data.input.spouse, data.input.hasSpouse);
        //Add the dynamic text
        addDynamicTexts(data, person);
        //Show/hide the primary, spouse, and combined tabs in the yearly cost table
        showSecondaryTabs(person);
        // Render chart
        var barChart = createChart(
            'myChart',
            data.graph1.ageData,
            data.graph1.OOP,
            data.graph1.covered_By_Hsa,
            data.graph2.hcPortfolioRatio,
            data.input
        );
        toggleChartDisclosure(data.input.hasSpouse);
        //Attach form validation and updateChart callback for recalculation 'submit' event
        validateAndUpdate(barChart, 'hcTyModalForm', 'inputModal');
    }

    /**
     * Handles recalculate on TY page. A little redudant with the above but that was the approach we took for some reason
     * @param {string} chart the chart we want to update
     * @param {string} formId the form the submit button is attached to
     * @param {string} modalId the div we need to update 
     */
    function validateAndUpdate(chart, formId, modalId) {
        document.getElementById(formId).addEventListener('submit', function (event) {
            event.preventDefault();
            if (isFormValid(formId)) {
                // Recalculate with new inputs and render
                var data = getHealthCareData(
                    getHtmlInputs,
                    getAssumption,
                    calculateSavings
                );
                //Default to the primary. We update this to "combined" if hasSpouse is true;
                var person = "primary";
                //TODO make a spouse handler that wraps hasSpouse functionality
                //Check if the spouse section is expanded and all the inputs are filled out
                var hasSpouseCheck = checkSpouseInputs();
                if (hasSpouseCheck) {
                    //?show tabs here
                    //I think this combines the data we need
                    var combinedData = data.primaryCostBreakdown.map(function (ele, index) {
                        return ele + data.spouseCostBreakDown[index];
                    });
                    insertBreakDown(data.spouseCostBreakDown, "spouse");
                    insertBreakDown(combinedData, "combined");
                    person = "combined";
                }
                insertBreakDown(data.primaryCostBreakdown, "primary");
                addDynamicTexts(data, person);
                //Show/hide the primary, spouse, and combined tabs in the yearly cost table
                showSecondaryTabs(person);
                // update chart with new calculated heathcare cost data
                updateChart(
                    chart,
                    data.graph1.ageData,
                    data.graph1.covered_By_Hsa,
                    data.graph1.OOP,
                    data.graph2.hcPortfolioRatio,
                    data.input
                );
                //Update the chart disclosure
                toggleChartDisclosure(data.input.hasSpouse);
                // write html inputs into cookie
                writeToCookie('calculator_query_string', data.input);
                // close modal
                document.querySelector('#' + modalId + ' button[aria-label="Close"]').click();
            }
            return false;
        });
    }

    function toggleChartDisclosure(hasSpouse) {
        var element = document.getElementById('chartDisclosure');
        if (hasSpouse) {
            element.classList.remove('hide');
        } else {
            element.classList.remove('add');
        }
    }

    /************************************************************************************************/
    /******************************* Main: Run everything once DOM has loaded************************/
    /************************************************************************************************/
    document.addEventListener('DOMContentLoaded', function () {
        //Run the calculations and render on page load
        initialCalcAndRender();
        /************************************************************************************************/
        /**************************** Main: Make Modal Input Form Responsive ****************************/
        /************************************************************************************************/
        //TODO: consider consolidating all these event listners into a single function above then call the function here
        // lifeExpectancy Calc Close Button for Primary
        document.querySelector('#endAgeCalculatorP .close').addEventListener('click', function (event) {
            document.querySelector('#ageCalcExpandBtnP .glyphicon-plus').classList.toggle('hide');
            document.querySelector('#ageCalcExpandBtnP .glyphicon-minus').classList.toggle('hide');
        });

        // lifeExpectancy Calc Close Button for Spouse
        document.querySelector('#endAgeCalculatorS .close').addEventListener('click', function (event) {
            document.querySelector('#ageCalcExpandBtnS .glyphicon-plus').classList.toggle('hide');
            document.querySelector('#ageCalcExpandBtnS .glyphicon-minus').classList.toggle('hide');
        });

        // Add/Remove/Edit Spouse Button
        document.getElementById('toggleSpouseBtn').addEventListener('click', function (event) {
            event.preventDefault();
            document.querySelector('#addSpouseBtn').classList.toggle('hide');
            document.querySelector('#removeSpouseBtn').classList.toggle('hide');
            document.querySelector('#calcResultBtn').classList.toggle('spouse');
        });

        // Life Expectancy Calc Expand Button for Primary
        document.getElementById('ageCalcExpandBtnP').addEventListener('click', function () {
            document.querySelector('#ageCalcExpandBtnP .glyphicon-plus').classList.toggle('hide');
            document.querySelector('#ageCalcExpandBtnP .glyphicon-minus').classList.toggle('hide');
        });

        // Life Expectancy Calc Result Btn for Primary
        document.getElementById('calcLeBtn').addEventListener('click', function (event) {
            event.preventDefault();
            document.querySelector('#ageCalcExpandBtnP .glyphicon-plus').classList.toggle('hide');
            document.querySelector('#ageCalcExpandBtnP .glyphicon-minus').classList.toggle('hide');
        });

        // Life Expectancy Calc Expand Button for Spouse
        document.getElementById('ageCalcExpandBtnS').addEventListener('click', function () {
            document.querySelector('#ageCalcExpandBtnS .glyphicon-plus').classList.toggle('hide');
            document.querySelector('#ageCalcExpandBtnS .glyphicon-minus').classList.toggle('hide');
        });

        // Life Expectancy Calc Result Btn for Spouse
        document.getElementById('s_calcLeBtn').addEventListener('click', function (event) {
            event.preventDefault();
            document.querySelector('#ageCalcExpandBtnS .glyphicon-plus').classList.toggle('hide');
            document.querySelector('#ageCalcExpandBtnS .glyphicon-minus').classList.toggle('hide');
        });

        // Edit Details Button Click Handler - show/hides spouse input section
        document.querySelectorAll("#primaryInfoBtn, #mobileModalBtn").forEach(function (el) {
            el.addEventListener('click', function () {
                //Expand the spouse inputs if they have a spouse
                if (window.fiCalc.inputs.hasSpouse) {
                    //display spouse section
                    $("#secondaryInfoBlock").collapse("show");
                    //change the icon
                    document.querySelector('#addSpouseBtn').classList.add('hide');
                    document.querySelector('#removeSpouseBtn').classList.remove('hide');
                    document.querySelector('#calcResultBtn').classList.add('spouse');
                }
            });
        });
        //Add the spouse error messages. See function above for more specific description
        spouseErrorMessageHandling();
    });
})();