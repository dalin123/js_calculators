document.addEventListener('DOMContentLoaded', function (event) {
    var lifeData = [{ "Table 1": "Age", "field2": "US/CA", "field3": "UK(m)", "field4": "UK(f)", "field5": "FIE(m)", "field6": "FIE(f)", "field7": "", "field8": "", "field9": "" }, { "Table 1": "-", "field2": "78.8268", "field3": "79.3800", "field4": "83.0600", "field5": "73.2000", "field6": "80.2000", "field7": "", "field8": "Sources:", "field9": "https://fisherprojects.fi.com/sites/SWRewrite/Project Documents/Project Prep Documents/LE_Table_SourceData.xlsx" }, { "Table 1": "1", "field2": "79.3002", "field3": "79.7200", "field4": "83.3500", "field5": "73.9000", "field6": "80.8750", "field7": "", "field8": "", "field9": "" }, { "Table 1": "2", "field2": "79.3320", "field3": "79.7400", "field4": "83.3800", "field5": "74.6000", "field6": "81.5500", "field7": "", "field8": "", "field9": "" }, { "Table 1": "3", "field2": "79.3527", "field3": "79.7600", "field4": "83.3900", "field5": "75.3000", "field6": "82.2250", "field7": "", "field8": "", "field9": "" }, { "Table 1": "4", "field2": "79.3682", "field3": "79.7700", "field4": "83.4000", "field5": "76.0000", "field6": "82.9000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "5", "field2": "79.3810", "field3": "79.7700", "field4": "83.4000", "field5": "76.2200", "field6": "83.1200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "6", "field2": "79.3918", "field3": "79.7800", "field4": "83.4100", "field5": "76.4400", "field6": "83.3400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "7", "field2": "79.4010", "field3": "79.7900", "field4": "83.4100", "field5": "76.6600", "field6": "83.5600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "8", "field2": "79.4091", "field3": "79.7900", "field4": "83.4200", "field5": "76.8800", "field6": "83.7800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "9", "field2": "79.4160", "field3": "79.8000", "field4": "83.4300", "field5": "77.1000", "field6": "84.0000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "10", "field2": "79.4221", "field3": "79.8100", "field4": "83.4300", "field5": "77.1400", "field6": "84.0200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "11", "field2": "79.4278", "field3": "79.8100", "field4": "83.4400", "field5": "77.1800", "field6": "84.0400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "12", "field2": "79.4339", "field3": "79.8200", "field4": "83.4400", "field5": "77.2200", "field6": "84.0600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "13", "field2": "79.4417", "field3": "79.8300", "field4": "83.4500", "field5": "77.2600", "field6": "84.0800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "14", "field2": "79.4527", "field3": "79.8300", "field4": "83.4500", "field5": "77.3000", "field6": "84.1000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "15", "field2": "79.4681", "field3": "79.8400", "field4": "83.4600", "field5": "77.3200", "field6": "84.1200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "16", "field2": "79.4879", "field3": "79.8500", "field4": "83.4700", "field5": "77.3400", "field6": "84.1400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "17", "field2": "79.5121", "field3": "79.8600", "field4": "83.4800", "field5": "77.3600", "field6": "84.1600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "18", "field2": "79.5410", "field3": "79.8800", "field4": "83.4900", "field5": "77.3800", "field6": "84.1800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "19", "field2": "79.5746", "field3": "79.9100", "field4": "83.5000", "field5": "77.4000", "field6": "84.2000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "20", "field2": "79.6130", "field3": "79.9300", "field4": "83.5200", "field5": "77.4200", "field6": "84.2200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "21", "field2": "79.6559", "field3": "79.9600", "field4": "83.5300", "field5": "77.4400", "field6": "84.2400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "22", "field2": "79.7028", "field3": "79.9900", "field4": "83.5400", "field5": "77.4600", "field6": "84.2600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "23", "field2": "79.7525", "field3": "80.0100", "field4": "83.5500", "field5": "77.4800", "field6": "84.2800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "24", "field2": "79.8034", "field3": "80.0400", "field4": "83.5700", "field5": "77.5000", "field6": "84.3000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "25", "field2": "79.8546", "field3": "80.0700", "field4": "83.5800", "field5": "77.5600", "field6": "84.3200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "26", "field2": "79.9056", "field3": "80.1000", "field4": "83.5900", "field5": "77.6200", "field6": "84.3400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "27", "field2": "79.9568", "field3": "80.1300", "field4": "83.6100", "field5": "77.6800", "field6": "84.3600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "28", "field2": "80.0081", "field3": "80.1600", "field4": "83.6200", "field5": "77.7400", "field6": "84.3800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "29", "field2": "80.0598", "field3": "80.2000", "field4": "83.6400", "field5": "77.8000", "field6": "84.4000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "30", "field2": "80.1119", "field3": "80.2300", "field4": "83.6600", "field5": "77.8800", "field6": "84.4400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "31", "field2": "80.1646", "field3": "80.2600", "field4": "83.6800", "field5": "77.9600", "field6": "84.4800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "32", "field2": "80.2179", "field3": "80.3000", "field4": "83.7000", "field5": "78.0400", "field6": "84.5200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "33", "field2": "80.2717", "field3": "80.3300", "field4": "83.7200", "field5": "78.1200", "field6": "84.5600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "34", "field2": "80.3261", "field3": "80.3700", "field4": "83.7400", "field5": "78.2000", "field6": "84.6000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "35", "field2": "80.3812", "field3": "80.4100", "field4": "83.7700", "field5": "78.3000", "field6": "84.6400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "36", "field2": "80.4374", "field3": "80.4600", "field4": "83.7900", "field5": "78.4000", "field6": "84.6800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "37", "field2": "80.4953", "field3": "80.5000", "field4": "83.8200", "field5": "78.5000", "field6": "84.7200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "38", "field2": "80.5554", "field3": "80.5500", "field4": "83.8500", "field5": "78.6000", "field6": "84.7600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "39", "field2": "80.6177", "field3": "80.6000", "field4": "83.8900", "field5": "78.7000", "field6": "84.8000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "40", "field2": "80.6826", "field3": "80.6600", "field4": "83.9200", "field5": "78.8200", "field6": "84.8400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "41", "field2": "80.7502", "field3": "80.7200", "field4": "83.9600", "field5": "78.9400", "field6": "84.8800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "42", "field2": "80.8211", "field3": "80.7800", "field4": "84.0000", "field5": "79.0600", "field6": "84.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "43", "field2": "80.8959", "field3": "80.8400", "field4": "84.0400", "field5": "79.1800", "field6": "84.9600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "44", "field2": "80.9758", "field3": "80.9100", "field4": "84.0900", "field5": "79.3000", "field6": "85.0000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "45", "field2": "81.0615", "field3": "80.9800", "field4": "84.1400", "field5": "79.4200", "field6": "85.0600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "46", "field2": "81.1533", "field3": "81.0600", "field4": "84.1900", "field5": "79.5400", "field6": "85.1200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "47", "field2": "81.2514", "field3": "81.1300", "field4": "84.2400", "field5": "79.6600", "field6": "85.1800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "48", "field2": "81.3568", "field3": "81.2200", "field4": "84.3000", "field5": "79.7800", "field6": "85.2400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "49", "field2": "81.4701", "field3": "81.3000", "field4": "84.3600", "field5": "79.9000", "field6": "85.3000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "50", "field2": "81.5916", "field3": "81.3900", "field4": "84.4200", "field5": "80.0400", "field6": "85.3800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "51", "field2": "81.7211", "field3": "81.4900", "field4": "84.4900", "field5": "80.1800", "field6": "85.4600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "52", "field2": "81.8579", "field3": "81.5900", "field4": "84.5700", "field5": "80.3200", "field6": "85.5400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "53", "field2": "82.0020", "field3": "81.6900", "field4": "84.6500", "field5": "80.4600", "field6": "85.6200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "54", "field2": "82.1534", "field3": "81.8100", "field4": "84.7300", "field5": "80.6000", "field6": "85.7000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "55", "field2": "82.3121", "field3": "81.9200", "field4": "84.8200", "field5": "80.8000", "field6": "85.8000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "56", "field2": "82.4785", "field3": "82.0500", "field4": "84.9100", "field5": "81.0000", "field6": "85.9000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "57", "field2": "82.6525", "field3": "82.1800", "field4": "85.0100", "field5": "81.2000", "field6": "86.0000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "58", "field2": "82.8337", "field3": "82.3300", "field4": "85.1200", "field5": "81.4000", "field6": "86.1000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "59", "field2": "83.0219", "field3": "82.4800", "field4": "85.2300", "field5": "81.6000", "field6": "86.2000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "60", "field2": "83.2166", "field3": "82.6400", "field4": "85.3400", "field5": "81.8600", "field6": "86.3400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "61", "field2": "83.4179", "field3": "82.8200", "field4": "85.4700", "field5": "82.1200", "field6": "86.4800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "62", "field2": "83.6261", "field3": "83.0000", "field4": "85.6000", "field5": "82.3800", "field6": "86.6200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "63", "field2": "83.8413", "field3": "83.1900", "field4": "85.7400", "field5": "82.6400", "field6": "86.7600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "64", "field2": "84.0638", "field3": "83.4000", "field4": "85.8900", "field5": "82.9000", "field6": "86.9000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "65", "field2": "84.2939", "field3": "83.6100", "field4": "86.0400", "field5": "83.2200", "field6": "87.0800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "66", "field2": "84.5324", "field3": "83.8300", "field4": "86.2000", "field5": "83.5400", "field6": "87.2600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "67", "field2": "84.7798", "field3": "84.0500", "field4": "86.3600", "field5": "83.8600", "field6": "87.4400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "68", "field2": "85.0369", "field3": "84.2900", "field4": "86.5400", "field5": "84.1800", "field6": "87.6200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "69", "field2": "85.3038", "field3": "84.5300", "field4": "86.7300", "field5": "84.5000", "field6": "87.8000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "70", "field2": "85.5810", "field3": "84.8000", "field4": "86.9200", "field5": "84.8400", "field6": "88.0000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "71", "field2": "85.8704", "field3": "85.0700", "field4": "87.1300", "field5": "85.1800", "field6": "88.2000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "72", "field2": "86.1741", "field3": "85.3600", "field4": "87.3500", "field5": "85.5200", "field6": "88.4000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "73", "field2": "86.4920", "field3": "85.6800", "field4": "87.5900", "field5": "85.8600", "field6": "88.6000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "74", "field2": "86.8236", "field3": "86.0000", "field4": "87.8400", "field5": "86.2000", "field6": "88.8000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "75", "field2": "87.1684", "field3": "86.3500", "field4": "88.1100", "field5": "86.6000", "field6": "89.0600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "76", "field2": "87.5270", "field3": "86.7200", "field4": "88.3900", "field5": "87.0000", "field6": "89.3200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "77", "field2": "87.9019", "field3": "87.0900", "field4": "88.6900", "field5": "87.4000", "field6": "89.5800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "78", "field2": "88.2940", "field3": "87.4800", "field4": "88.9900", "field5": "87.8000", "field6": "89.8400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "79", "field2": "88.7037", "field3": "87.8900", "field4": "89.3200", "field5": "88.2000", "field6": "90.1000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "80", "field2": "89.1344", "field3": "88.3200", "field4": "89.6700", "field5": "88.7000", "field6": "90.4800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "81", "field2": "89.5833", "field3": "88.7800", "field4": "90.0500", "field5": "89.2000", "field6": "90.8600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "82", "field2": "90.0495", "field3": "89.2500", "field4": "90.4500", "field5": "89.7000", "field6": "91.2400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "83", "field2": "90.5343", "field3": "89.7600", "field4": "90.8700", "field5": "90.2000", "field6": "91.6200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "84", "field2": "91.0407", "field3": "90.3000", "field4": "91.3200", "field5": "90.7000", "field6": "92.0000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "85", "field2": "91.5694", "field3": "90.8500", "field4": "91.8100", "field5": "91.2800", "field6": "92.4800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "86", "field2": "92.1229", "field3": "91.4400", "field4": "92.3200", "field5": "91.8600", "field6": "92.9600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "87", "field2": "92.7017", "field3": "92.0500", "field4": "92.8500", "field5": "92.4400", "field6": "93.4400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "88", "field2": "93.3054", "field3": "92.6800", "field4": "93.4200", "field5": "93.0200", "field6": "93.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "89", "field2": "93.9338", "field3": "93.3400", "field4": "94.0100", "field5": "93.6000", "field6": "94.4000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "90", "field2": "94.5863", "field3": "94.0300", "field4": "94.6300", "field5": "94.3000", "field6": "95.0200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "91", "field2": "95.2626", "field3": "94.7300", "field4": "95.2900", "field5": "95.0000", "field6": "95.6400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "92", "field2": "95.9617", "field3": "95.4500", "field4": "95.9600", "field5": "95.7000", "field6": "96.2600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "93", "field2": "96.6831", "field3": "96.2100", "field4": "96.6600", "field5": "96.4000", "field6": "96.8800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "94", "field2": "97.4259", "field3": "97.0100", "field4": "97.4100", "field5": "97.1000", "field6": "97.5000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "95", "field2": "98.1890", "field3": "97.8000", "field4": "98.1700", "field5": "97.8600", "field6": "98.2200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "96", "field2": "98.9716", "field3": "98.6200", "field4": "98.9400", "field5": "98.6200", "field6": "98.9400", "field7": "", "field8": "", "field9": "" }, { "Table 1": "97", "field2": "99.7727", "field3": "99.4200", "field4": "99.7200", "field5": "99.3800", "field6": "99.6600", "field7": "", "field8": "", "field9": "" }, { "Table 1": "98", "field2": "100.5911", "field3": "100.2700", "field4": "100.5300", "field5": "100.1400", "field6": "100.3800", "field7": "", "field8": "", "field9": "" }, { "Table 1": "99", "field2": "101.4259", "field3": "101.1400", "field4": "101.3700", "field5": "100.9000", "field6": "101.1000", "field7": "", "field8": "", "field9": "" }, { "Table 1": "100", "field2": "102.2758", "field3": "102.0100", "field4": "102.2400", "field5": "101.7600", "field6": "101.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "101", "field2": "103.2758", "field3": "103.0100", "field4": "103.2400", "field5": "102.7600", "field6": "102.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "102", "field2": "104.2758", "field3": "104.0100", "field4": "104.2400", "field5": "103.7600", "field6": "103.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "103", "field2": "105.2758", "field3": "105.0100", "field4": "105.2400", "field5": "104.7600", "field6": "104.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "104", "field2": "106.2758", "field3": "106.0100", "field4": "106.2400", "field5": "105.7600", "field6": "105.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "105", "field2": "107.2758", "field3": "107.0100", "field4": "107.2400", "field5": "106.7600", "field6": "106.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "106", "field2": "108.2758", "field3": "108.0100", "field4": "108.2400", "field5": "107.7600", "field6": "107.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "107", "field2": "109.2758", "field3": "109.0100", "field4": "109.2400", "field5": "108.7600", "field6": "108.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "108", "field2": "110.2758", "field3": "110.0100", "field4": "110.2400", "field5": "109.7600", "field6": "109.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "109", "field2": "111.2758", "field3": "111.0100", "field4": "111.2400", "field5": "110.7600", "field6": "110.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "110", "field2": "112.2758", "field3": "112.0100", "field4": "112.2400", "field5": "111.7600", "field6": "111.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "111", "field2": "113.2758", "field3": "113.0100", "field4": "113.2400", "field5": "112.7600", "field6": "112.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "112", "field2": "114.2758", "field3": "114.0100", "field4": "114.2400", "field5": "113.7600", "field6": "113.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "113", "field2": "115.2758", "field3": "115.0100", "field4": "115.2400", "field5": "114.7600", "field6": "114.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "114", "field2": "116.2758", "field3": "116.0100", "field4": "116.2400", "field5": "115.7600", "field6": "115.9200", "field7": "", "field8": "", "field9": "" }, { "Table 1": "115", "field2": "117.2758", "field3": "117.0100", "field4": "117.2400", "field5": "116.7600", "field6": "116.9200", "field7": "", "field8": "", "field9": "" }];

    //Current Age
    var currAge = 1;
    var localization = "US";
    var gender = "m";
    var currHealth;
    var motherAge;
    var fatherAge;
    var motherStatus;
    var fatherStatus;

    var hasSpouse = false;

    var spouse_currAge;
    var spouse_localization = "US";
    var spouse_gender = "f";
    var spouse_currHealth;
    var spouse_motherAge;
    var spouse_fatherAge;
    var spouse_motherStatus;
    var spouse_fatherStatus;


    var healthTable = {
        Excellent: 5,
        Good: 3,
        Fair: 0,
        Poor: -3,
        Failing: currAge + 1
    };
    var statusTable = {
        "Client Unable to Provide": 0,
        "Age At Death - Exact": 1,
        "Age At Death - Client Estimate": 2,
        "Still Living": 3,
        "Passed Due to Unnatural Causes": 4
    };

    function getCookieValue(key) {
        var val = document.cookie.match('(?:^|;?\s?)(' + key + ')=([^;]*);?');
        return val ? val[2].split("&")[0] : null;
    }


    function driver() {

        /* Core Engine
        * @params {integers}  -  MetaData concerning parents biology
        * @return {Life Expectancy Results} returns life expectancy results 
        */
        function calculateLifeExpectancy(age, motherAge, motherStatus, fatherAge, fatherStatus, currHealth, lateParents, lateParentsArr, lateParentsAvgAge) {

            var scenarios = {
                hereditary: 0,
                irs_table_value: 0,
                scenario1: 0,
                scenario2: 0,
                scenario3: 0,
                scenario1PlusHealth: 0,
                scenario2PlusHealth: 0,
                scenario3PlusHealth: 0,
                scenario1PlusHealthPlusAgeCorr: 0,
                scenario2PlusHealthPlusAgeCorr: 0,
                scenario3PlusHealthPlusAgeCorr: 0,
                final1: 0,
                final2: 0,
                final3: 0,
                LE: 0,
                FinalLE: 0
            };

            var localizationDerivative = {
                US: "field2",
                UKm: "field3",
                UKf: "field4",
                FIEm: "field5",
                FIEf: "field6"
            };

            //-- Set number of late parents

            function statusDerivative(status, value1, value2) {
                if (status === undefined) return;
                return status.indexOf(value1) || status.indexOf(value2);
            }

            if (motherAge && (motherStatus == "Deceased" || motherAge >= 90) && lateParentsArr.length < 2) {
                lateParents++;
                lateParentsArr[0] = (motherAge);
                if (lateParentsAvgAge) {
                    lateParentsAvgAge += Number(motherAge);
                } else {
                    lateParentsAvgAge = Number(motherAge);
                }
            }
            if (fatherAge && (fatherStatus == "Deceased" || fatherAge >= 90) && lateParentsArr.length < 2) {
                lateParents++;
                lateParentsArr[1] = (fatherAge);
                if (lateParentsAvgAge) {
                    lateParentsAvgAge += Number(fatherAge);
                } else {
                    lateParentsAvgAge = Number(fatherAge);
                }
            }

            //---Average Age of late parents
            var actualParentsNum = 0;
            for (var p = 0; p < lateParentsArr.length; p++) {
                if (lateParentsArr[p]) {
                    actualParentsNum++;
                } else if (!lateParentsArr[p]) {
                    lateParents--;
                }
            }

            if (lateParentsArr.length <= 2 && motherAge || fatherAge) {
                lateParentsAvgAge = lateParentsAvgAge / actualParentsNum;
            }


            //---------------Set IRS table value
            if (currAge > 0 && currAge < 25) {
                scenarios.irs_table_value = 85;
            } else {
                scenarios.irs_table_value =
                    lifeData[Number(currAge) + 1][localizationDerivative[localization]];
            }
            scenarios.lateParentsAvgAge = lateParentsAvgAge;
            scenarios.lateParentsArr = lateParentsArr;

            //-------hereditary values

            if (lateParents <= 1) {
                scenarios.hereditary = Number(scenarios.irs_table_value);
            } else {
                scenarios.hereditary = Number(lateParentsAvgAge);
            }



            //scenario1
            scenarios.scenario1 = Math.max(
                scenarios.hereditary,
                scenarios.irs_table_value
            );
            //scenario2
            if (Math.abs(scenarios.hereditary - scenarios.irs_table_value) > 20) {

                scenarios.scenario2 =
                    (Number(scenarios.hereditary) + Number(scenarios.irs_table_value)) / 2;
            } else {
                scenarios.scenario2 = Math.max(
                    scenarios.hereditary,
                    scenarios.irs_table_value
                );
            }

            //scenario 3
            scenarios.scenario3 =
                Math.max(scenarios.hereditary, scenarios.irs_table_value) * 0.67 +
                Math.min(scenarios.hereditary, scenarios.irs_table_value) * 0.33;

            //scenario1 + health
            if (currHealth === "Excellent") {
                scenarios.scenario1PlusHealth =
                    scenarios.scenario1 + healthTable[currHealth];
            } else if (currHealth === "Good") {
                scenarios.scenario1PlusHealth =
                    scenarios.scenario1 + healthTable[currHealth];
            } else if (currHealth === "Fair") {
                scenarios.scenario1PlusHealth =
                    scenarios.scenario1 + healthTable[currHealth];
            } else if (currHealth === "Poor") {
                scenarios.scenario1PlusHealth =
                    scenarios.scenario1 + healthTable[currHealth];
            } else if (currHealth === "Failing") {
                scenarios.scenario1PlusHealth =
                    scenarios.scenario1 + healthTable[currHealth];
            } else {
                scenarios.scenario1PlusHealth = 0;
            }

            //scenario2 + health
            if (currHealth === "Excellent") {
                scenarios.scenario2PlusHealth =
                    scenarios.scenario2 + healthTable[currHealth];
            } else if (currHealth === "Good") {
                scenarios.scenario2PlusHealth =
                    scenarios.scenario2 + healthTable[currHealth];
            } else if (currHealth === "Fair") {
                scenarios.scenario2PlusHealth =
                    scenarios.scenario2 + healthTable[currHealth];
            } else if (currHealth === "Poor") {
                scenarios.scenario2PlusHealth =
                    scenarios.scenario2 + healthTable[currHealth];
            } else if (currHealth === "Failing") {
                scenarios.scenario2PlusHealth =
                    scenarios.scenario2 + healthTable[currHealth];
            } else {
                scenarios.scenario2PlusHealth = 0;
            }

            //scenario3 + health
            if (currHealth === "Excellent") {
                scenarios.scenario3PlusHealth =
                    scenarios.scenario3 + healthTable[currHealth];
            } else if (currHealth === "Good") {
                scenarios.scenario3PlusHealth =
                    scenarios.scenario3 + healthTable[currHealth];
            } else if (currHealth === "Fair") {
                scenarios.scenario3PlusHealth =
                    scenarios.scenario3 + healthTable[currHealth];
            } else if (currHealth === "Poor") {
                scenarios.scenario3PlusHealth =
                    scenarios.scenario3 + healthTable[currHealth];
            } else if (currHealth === "Failing") {
                scenarios.scenario3PlusHealth =
                    scenarios.scenario3 + healthTable[currHealth];
            } else {
                scenarios.scenario3PlusHealth = 0;
            }

            //scenario 1 + health + age correlation
            if (currAge < 80) {
                scenarios.scenario1PlusHealthPlusAgeCorr =
                    scenarios.scenario1PlusHealth + 5;
            } else if (currAge >= 80 && currAge < 90) {
                scenarios.scenario1PlusHealthPlusAgeCorr =
                    scenarios.scenario1PlusHealth + 3;
            } else {
                scenarios.scenario1PlusHealthPlusAgeCorr =
                    scenarios.scenario1PlusHealth + 1;
            }
            //scenario 2 + health + age correlation
            if (currAge < 80) {
                scenarios.scenario2PlusHealthPlusAgeCorr =
                    scenarios.scenario2PlusHealth + 5;
            } else if (currAge >= 80 && currAge < 90) {
                scenarios.scenario2PlusHealthPlusAgeCorr =
                    scenarios.scenario2PlusHealth + 3;
            } else {
                scenarios.scenario2PlusHealthPlusAgeCorr =
                    scenarios.scenario2PlusHealth + 1;
            }
            //scenario 3 + health + age correlation
            if (currAge < 80) {
                scenarios.scenario3PlusHealthPlusAgeCorr =
                    scenarios.scenario3PlusHealth + 5;
            } else if (currAge >= 80 && currAge < 90) {
                scenarios.scenario3PlusHealthPlusAgeCorr =
                    scenarios.scenario3PlusHealth + 3;
            } else {
                scenarios.scenario3PlusHealthPlusAgeCorr =
                    scenarios.scenario3PlusHealth + 1;
            }
            //final 1
            if (scenarios.scenario1PlusHealthPlusAgeCorr <= currAge) {
                scenarios.final1 = currAge + 2;
            } else {
                scenarios.final1 = scenarios.scenario1PlusHealthPlusAgeCorr;
            }
            //final 2
            if (scenarios.scenario2PlusHealthPlusAgeCorr <= currAge) {
                scenarios.final2 = currAge + 2;
            } else {
                scenarios.final2 = scenarios.scenario2PlusHealthPlusAgeCorr;
            }
            //final 3
            if (scenarios.scenario3PlusHealthPlusAgeCorr <= currAge) {
                scenarios.final3 = currAge + 2;
            } else {
                scenarios.final3 = scenarios.scenario3PlusHealthPlusAgeCorr;
            }

            //LE
            scenarios.LE =
                (scenarios.final1 +
                    scenarios.final2 +
                    scenarios.final3 -
                    Math.min(scenarios.final1, scenarios.final2, scenarios.final3)) /
                2;

            //Final Life expectancy
            scenarios.FinalLE = Math.round(scenarios.LE);
            // console.log(scenarios);
            return scenarios;
        }

        //-----------------------------------------------Start of HTML Input 

        // doesn't need to be inside gatherInputData FX because it's not an HTML/Cookie input
        var lateParents = 0;
        var lateParentsArr = [];
        var lateParentsAvgAge;
        var spouse_lateParents = 0;
        var spouse_lateParentsArr = [];
        var spouse_lateParentsAvgAge = 0;
        /* GatherInputData
        * @params {string}  -  What page we're pulling from and who is pulling it.
        */

        function gatherInputData(scenario, who) {

            var inputObject = {};


            if (scenario === "form_seven") {

                if (who === "primary") {
                    inputObject.currAge = getCookieValue("ica");
                    inputObject.motherStatus = document.querySelectorAll('#lec-i-mh .active input')[0].value;
                    inputObject.motherAge = document.getElementById('lec-i-mage').value;
                    inputObject.fatherStatus = document.querySelectorAll('#lec-i-fh .active input')[0].value;
                    inputObject.fatherAge = document.getElementById('lec-i-fage').value;
                    inputObject.currHealth = getCookieValue("ich");
                } else if (who === "spouse") {
                    inputObject.spouse_currAge = getCookieValue("sca");
                    inputObject.spouse_motherStatus = document.querySelectorAll('#lec-s-mh .active input')[0].value;
                    inputObject.spouse_motherAge = document.getElementById('lec-s-mage').value;
                    inputObject.spouse_fatherStatus = document.querySelectorAll('#lec-s-fh .active input')[0].value;
                    inputObject.spouse_fatherAge = document.getElementById('lec-s-fage').value;
                    inputObject.spouse_currHealth = getCookieValue("sch");
                }
            } else if (scenario === "TY") {
                if (who === "primary") {
                    inputObject.currAge = document.querySelector('*[data-name="age"]').value;
                    inputObject.motherStatus = document.querySelectorAll('#lec-i-mh .active input')[0].value;
                    inputObject.motherAge = document.getElementById('lec-i-mage').value;
                    inputObject.fatherStatus = document.querySelectorAll('#lec-i-fh .active input')[0].value;
                    inputObject.fatherAge = document.getElementById('lec-i-fage').value;
                    inputObject.currHealth = document.querySelectorAll('#lec-i-h .active input')[0].value;
                } else if (who === "spouse") {
                    inputObject.spouse_currAge = document.querySelector('*[data-name="age-2"]').value;
                    inputObject.spouse_motherStatus = document.querySelectorAll('#lec-s-mh .active input')[0].value;
                    inputObject.spouse_motherAge = document.getElementById('lec-s-mage').value;
                    inputObject.spouse_fatherStatus = document.querySelectorAll('#lec-s-fh .active input')[0].value;
                    inputObject.spouse_fatherAge = document.getElementById('lec-s-fage').value;
                    inputObject.spouse_currHealth = document.querySelectorAll('#lec-s-h .active input')[0].value;
                }
            }

            return inputObject;
        }

        var spouseCheck;
        var primaryCheck;
        var primaryInputData;
        var spouseInputData;



        var scenario;
        //used to check for has spouse between TY and form_7

        scenario = utag_data.page_step === "Thank You" ? "TY" : "form_seven";
        if (scenario === "form_seven") {
            hasSpouse = document.querySelector(".life-exp-calc.spouse");
        } else if (scenario === "TY") {
            hasSpouse = document.getElementById("secondaryInfoBlock").ariaExpanded === "true" ? true : false;
        }



        if (handleWhoClicked("primary", scenario)) {
            primaryInputData = gatherInputData(scenario, "primary");
            var primaryLEData = calculateLifeExpectancy(
                primaryInputData.currAge,
                primaryInputData.motherAge,
                primaryInputData.motherStatus,
                primaryInputData.fatherAge,
                primaryInputData.fatherStatus,
                primaryInputData.currHealth,
                lateParents,
                lateParentsArr,
                lateParentsAvgAge
            );
            document.querySelectorAll('[data-name="life-exp"]').forEach(function (item) {
                item.innerText = primaryLEData.FinalLE;
                item.value = primaryLEData.FinalLE;
                item.dataset.val = primaryLEData.FinalLE;
            });
        }

        if (hasSpouse && handleWhoClicked("spouse", scenario)) {
            spouseInputData = hasSpouse ? gatherInputData(scenario, "spouse") : false;
            var spouseLEData = calculateLifeExpectancy(
                spouseInputData.spouse_currAge,
                spouseInputData.spouse_motherAge,
                spouseInputData.spouse_motherStatus,
                spouseInputData.spouse_fatherAge,
                spouseInputData.spouse_fatherStatus,
                spouseInputData.spouse_currHealth,
                spouse_lateParents,
                spouse_lateParentsArr,
                spouse_lateParentsAvgAge
            );
            // make a fix to split apart html / and core engine here
            document.querySelectorAll('[data-name="life-exp-2"]').forEach(function (item) {
                item.innerText = spouseLEData.FinalLE;
                item.value = spouseLEData.FinalLE;
                item.dataset.val = spouseLEData.FinalLE;
            });
        }


    }

    // jquery handler here to target onlicks for all inputs to target all clicks and to
    /* 
    * @param {string} who - used to pass a parameter to callback function
    * @param{function} callback - the function we want to call
    * @return {function} if true runs the callback, else calls displayErrorMsg which adds an error message to the page
    */
    var checkInputs = function (who, callback, scenario) {
        if (who === "primary") {
            //All the inputs we want to check
            var imh = document.querySelector('#lec-i-mh .active input');
            var ima = document.querySelector('#lec-i-mage');
            var ifh = document.querySelector('#lec-i-fh .active input');
            var ifa = document.querySelector('#lec-i-fage');
            var iyh = scenario == "form_seven" ? getCookieValue("ich") : document.querySelector('#lec-i-h .active input');
            //Check that all inputs have a value
            if (imh && ima.value !== "" && ifh && ifa.value !== "" && iyh) {
                //All inputs have values. Run the function 
                callback(who, [imh, ima, ifh, ifa, iyh]);
            } else {
                //Add validation error message to page
                displayErrorMsg([imh, ima, ifh, ifa, iyh], "calcLeBtn");
            }
        } else if (who === "spouse") {
            //All the inputs we want to check
            var smh = document.querySelector('#lec-s-mh .active input');
            var sma = document.querySelector('#lec-s-mage');
            var sfh = document.querySelector('#lec-s-fh .active input');
            var sfa = document.querySelector('#lec-s-fage');
            var syh = scenario == "form_seven" ? getCookieValue("sch") : document.querySelector('#lec-s-h .active input');
            //Check that all inputs have a value
            if (smh && sma.value !== "" && sfh && sfa.value !== "" && syh) {
                //All inputs have values. Run the function 
                callback(who, [smh, sma, sfh, sfa, syh]);
            } else {
                //Add validation error message to page
                //UNCOMMENT HERE
                displayErrorMsg([smh, sma, sfh, sfa, syh], "s_calcLeBtn");
            }
        } else {
            console.log("Check parameters. Received: " + who);
        }
    };

    /* 
 * @param {string} who - used to pass a parameter to callback function
 * @param{function} callback - the function we want to call
 * @return {function} true or false
 */
    var handleWhoClicked = function (who, scenario) {
        if (who === "primary") {
            //All the inputs we want to check
            var imh = document.querySelector('#lec-i-mh .active input');
            var ima = document.querySelector('#lec-i-mage');
            var ifh = document.querySelector('#lec-i-fh .active input');
            var ifa = document.querySelector('#lec-i-fage');
            var iyh = scenario == "form_seven" ? getCookieValue("ich") : document.querySelector('#lec-i-h .active input');
            //Check that all inputs have a value
            if (imh && ima.value !== "" && ifh && ifa.value !== "" && iyh) {
                //All inputs have values. Run the function 
                return true;
            } else {
                //Add validation error message to page
                return false;
            }
        } else if (who === "spouse") {
            //All the inputs we want to check
            var smh = document.querySelector('#lec-s-mh .active input');
            var sma = document.querySelector('#lec-s-mage');
            var sfh = document.querySelector('#lec-s-fh .active input');
            var sfa = document.querySelector('#lec-s-fage');
            var syh = scenario == "form_seven" ? getCookieValue("sch") : document.querySelector('#lec-s-h .active input');
            //Check that all inputs have a value
            if (smh && sma.value !== "" && sfh && sfa.value !== "" && syh) {
                //All inputs have values. Run the function 
                return true;
            } else {
                //Add validation error message to page
                //UNCOMMENT HERE
                return false;
            }
        } else {
            console.log("Check parameters. Received: " + who);
        }
    };


    /* 
 * @ Checks to see if spouse tab is populated
 * @return {function} true or false
 */


    var checkHasSpouse = function () {

        var spouseGender = document.querySelector("#genderS").value;
        var spouseAge = document.querySelector("#startAgeS").value;
        var spouseRetirementAge = document.querySelector("#retireAgeS").value;
        var spouseLifeExpectancy = document.querySelector("#endAgeS").value;
        var spouseRetirementSavings = document.querySelector("#startBalanceS").value;
        var spouseHsaAccount = document.querySelector("#startHsaS").value;
        var spouseAnnualContributions = document.querySelector("#annualContributeS").value;
        var spouseHsaContributions = document.querySelector("#annualHsaS").value;

        var tyInputFields = (spouseGender && spouseAge && spouseRetirementAge && spouseLifeExpectancy && spouseRetirementSavings && spouseHsaAccount && spouseAnnualContributions && spouseHsaContributions);
        //Check that all inputs have a value
        if (tyInputFields) {
            return true;
        } else {
            return false;
        }
    };

    /************************************************************************************************/
    /*********************************** Setup: handleCookieUpdate FX's *************************************/
    /************************************************************************************************/


    /**
     * Cookie Helper Functions
     */

    function setCookie(name, value) {
        var expires = "";
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }
    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }


    /* 
    * @param {array} array - the HTML elements we want to check
    * @param {string} button - the ID of the button we want to display the error message above
    * @return {HTML Element} displays an error message 
    */
    var displayErrorMsg = function (array, button) {
        var hasHelpBlock = document.getElementById(button).nextSibling.classList;
        hasHelpBlock = hasHelpBlock ? hasHelpBlock.contains('help-block') : false;
        var errorMsgDiv = document.createElement('span');
        errorMsgDiv.classList.add("help-block");
        errorMsgDiv.innerHTML = "*Please fill out all the fields";
        for (var i = 0; i < array.length; i++) {
            if ((!array[i] || !array[i].value) && !hasHelpBlock) {
                console.log('Please fill out all fields');
                document.getElementById(button).insertAdjacentElement('afterend', errorMsgDiv);
                break;
            }
        }
    };

    var outsideScenario = utag_data.page_step === "Thank You" ? "TY" : "form_seven";

    if (outsideScenario === "form_seven") {
        hasSpouse = getCookieValue("hasSpouse").length > 1 ? true : false;
    } else if (outsideScenario === "TY") {
        hasSpouse = checkHasSpouse();
    }

    //Store functions as variable so we can use them in the event handlers below

    primaryCheck = function (event) {
        checkInputs("primary", driver, outsideScenario);
    };

    if (document.getElementById("s_calcLeBtn")) {
        spouseCheck = function (event) {
            checkInputs("spouse", driver, outsideScenario);
        };
    }
    //Add click handlers to calculate buttons
    document.getElementById("calcLeBtn").addEventListener("click", primaryCheck);
    //UNCOMMENT HERE

    if (document.getElementById("s_calcLeBtn")) {
        document.getElementById("s_calcLeBtn").addEventListener("click", spouseCheck);
    }


}
);