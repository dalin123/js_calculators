//Global window variable to hold data on page
window.fiCalc = window.fiCalc || {};
//Globals shared by various functions
//Maps all the iterations of M/F/Undefined. Add default for empty or NaN values when user doesn't anything.
var genderMap = {
  "m": "Male",
  "M": "Male",
  "f": "Female",
  "F": "Female",
  "u": "Undefined",
  "U": "Undefined",
  "Male": "Male",
  "male": "Male",
  "Female": "Female",
  "female": "Female",
  "undefined": "Undefined",
  "Undefined": "Undefined",
  "": "Undefined",
  NaN: "Undefined"
};
/************************************************************************************************/
/************************ Setup: Heatlhcare Cost Calculate Functions ****************************/
/************************************************************************************************/
//Moved from outside of calculateSavings()
//Helper fx for decimal places
function fixArrayValues(arr, decimal) {
  return arr.map(function (val, index) {
    return Number(Math.round(val + 'e' + decimal) + 'e-' + decimal);
  });
}

/* Outputs Numbers like 1400000 as 1,400,000 */
function toCommas(value) {
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

// Investment Assumptions
function getAssumption() {
  // Out-of-pocket (OOP) cost by gender and age
  // Gender: Undefined, Male, Female
  // Age Groups:
  var _ageGenderCostTable = {
    "Undefined": [2834, 5021, 5829, 10307],
    "Male": [2876, 4799, 5737, 9802],
    "Female": [2790, 5205, 5900, 10579]
  };
  return {
    "portfolioReturnRate": 0.0199,
    "hsaReturnRate": -0.0201,
    "healthcareCostInflationRate": 0.0199,
    "ageGenderCostTable": _ageGenderCostTable
  };
}

function calculateSavings(portfolioReturns, healthCareInflation, HSAreturns, ageGenderCostTable, primaryData, spouseData, hasSpouse) {
  //helper function for future value function
  function conv_number(expr, decplaces) {
    var str = "" + Math.round(eval(expr) * Math.pow(10, decplaces));
    while (str.length <= decplaces) {
      str = "0" + str;
    }
    var decpoint = str.length - decplaces;
    return (str.substring(0, decpoint) + "." + str.substring(decpoint, str.length));
  }

  /**
 * calculateBreakDownCosts() returns an array with proper health care breakdown
 * based on the retirement age. 
 * Input: Gender[Male_,Female_,Undefined_] Retirement Age
 * Output: Array
 */

  function calculateBreakDownCosts(gender, retirementAge, currentAge, assumptions) {
    //arrayvalue[NumberofBeneficiaries	,Medianshareofindividualincomespentonout-ofpockethealthcarecosts,AverateTotalOOPSpending,AverageOOPSpendingonPremiums,AverageOOPspendingonServices,	LongTermCareFacility	SkilledNursingFacility,	PerscriptionDrugs,	Providers/Suppliers	Dental,	OutpatientServices	,InpatientServices,	Other]
    var processedAge;
    var resultsTable;
    var lookupTable = {
      "Male": {
        "Under_65": [2979000, 0.08, 2876, 982, 1894, 607, 60, 389, 426, 269, 81, 23, 40],
        "Between_65_and_74": [6688000, 0.1, 4799, 2594, 2204, 706, 70, 453, 496, 313, 94, 26, 47],
        "Between_75_and_84": [3560000, 0.13, 5737, 2661, 3076, 985, 97, 632, 692, 436, 131, 37, 65],
        "Greater_than_85": [1366000, 0.15, 9802, 2493, 7309, 2341, 231, 1503, 1644, 1037, 312, 88, 155]
      },
      "Female": {
        "Under_65": [2892000, 0.07, 2790, 819, 1971, 631, 62, 405, 443, 280, 84, 24, 42],
        "Between_65_and_74": [8103000, 0.13, 5205, 2705, 2501, 801, 79, 514, 562, 355, 107, 30, 53],
        "Between_75_and_84": [4603000, 0.14, 5900, 2703, 3197, 1024, 101, 657, 719, 453, 136, 38, 68],
        "Greater_than_85": [2535000, 0.17, 10579, 2052, 8527, 2731, 269, 1753, 1918, 1209, 364, 102, 180]
      },
      "Undefined": {
        "Under_65": [5871000, 0.07, 2834, 901, 1932, 619, 61, 397, 434, 274, 82, 23, 41],
        "Between_65_and_74": [14791000, 0.12, 5021, 2655, 2367, 758, 75, 487, 532, 336, 101, 28, 50],
        "Between_75_and_84": [8163000, 0.14, 5829, 2685, 3144, 1007, 99, 646, 707, 446, 134, 38, 67],
        "Greater_than_85": [3901000, 0.16, 10307, 2206, 8101, 2595, 256, 1666, 1822, 1149, 345, 97, 171]
      }
    };
    gender = genderMap[gender];

    if (retirementAge < 65) {
      processedAge = "Under_65";
    } else if (retirementAge >= 65 && retirementAge <= 74) {
      processedAge = "Between_65_and_74";
    } else if (retirementAge >= 75 && retirementAge <= 84) {
      processedAge = "Between_75_and_84";
    } else {
      processedAge = "Greater_than_85";
    }

    //For people who are not retired
    var dynamicYearDiff = currentAge >= retirementAge ? 0 : retirementAge - currentAge;
    resultsTable = lookupTable[gender][processedAge].map(function (value, index) {
      if (index <= 1) {
        return value;
      }
      return value * Math.pow((1 + assumptions), dynamicYearDiff + (new Date().getFullYear() - 2016));
    });

    return resultsTable;
  }

  // Helper function for calculating future value
  function FV(rate, nper, pmt, pv) {
    rate = parseFloat(rate);
    nper = parseFloat(nper);
    pmt = parseFloat(pmt);
    pv = parseFloat(pv);
    if (nper == 0) {
      return (0);
    }
    if (rate == 0) { // Interest rate is 0
      fv_value = -(pv + (pmt * nper));
    } else {
      x = Math.pow(1 + rate, nper);
      fv_value = -(-pmt + x * pmt + rate * x * pv) / rate;
    }
    fv_value = conv_number(fv_value, 2);
    return Number(fv_value);
  }
  //helper fx for calculating secondary age health care cost
  function calculateSecondaryAge(hasSpouse, primary_currentAge, primary_retirementAge, spouse_currentAge) {
    if (hasSpouse === true) {
      if (primary_currentAge > primary_retirementAge) {
        return spouse_currentAge;
      } else {
        return spouse_currentAge + (primary_retirementAge - primary_currentAge);
      }
    }
    return undefined;
  }
  // helper fx: combinging primary and spouse data
  // helper fx: combinging primary and spouse data
  function combineGraphData(primary, spouse) {
    //Variables need throughout
    var longestLived = Math.max(primaryData.lifeExpectancy, (primary.spouseData.currentAge - spouse.spouseData.currentAge) + spouse.spouseData.lifeExpectancy);
    var indexVal = 0;
    var combinedOOPTotal = [];
    var combinedOOPYears = [];
    var covered_By_Hsa = [];
    var OOP = [];
    var combinedAges = [];
    var combinedOOPVal = 0;
    var combinedOOPyearVal = 0;
    var combinedHSAval = 0;
    var longer_spouse;
    var shorter_spouse;
    var lifetimeHcCosts;
    var lifetimeHsa;
    var lifetimePercentCovered;
    var lifetimeOop;
    var hasHsaFlag;
    //Find which spouse lives longer
    if (primary.costs.length >= spouse.costs.length) {
      longer_spouse = primary;
      shorter_spouse = spouse;
    } else {
      longer_spouse = spouse;
      shorter_spouse = primary;
    }

    var longer_spouse_port_length = longer_spouse.portfolioValueAtRetirement.length;
    var shorter_spouse_port_length = shorter_spouse.portfolioValueAtRetirement.length;
    //Trying to fix array lengths before combining the data
    //??Find out what this is doing
    var normalizedData = [shorter_spouse.portfolioValueAtRetirement[shorter_spouse.portfolioValueAtRetirement.length - 1]];
    var normDataVal;
    //??Find out what this is doign
    for (var h = shorter_spouse_port_length; h < longer_spouse_port_length; h++) {
      normDataVal = ((normalizedData[normalizedData.length - 1] * 0.95) * (1 + portfolioReturns));
      normalizedData.push(normDataVal);
    }
    //removing unneeded duplicate
    normalizedData.shift();
    shorter_spouse.portfolioValueAtRetirement = shorter_spouse.portfolioValueAtRetirement.concat(normalizedData);
    //Get the combined costs by iterating over the longer spouse first and add the shorter living spouses data if 
    var combinedCosts = longer_spouse.costs.map(function (value, index, arr) {
      //Set value to 0 if NaN, Undefined, Null, etc
      value = value || 0;
      shorter_spouse.costs[index] = shorter_spouse.costs[index] || 0;
      //Combine primary and spouse 
      return value + shorter_spouse.costs[index];
    });

    var combinedHSAValueEOY = longer_spouse.HSAvalueEOY.map(function (value, index, arr) {
      //Set value to 0 if NaN, Undefined, Null, etc
      value = value || 0;
      shorter_spouse.HSAvalueEOY[index] = shorter_spouse.HSAvalueEOY[index] || 0;
      //Combine primary and spouse 
      return value + shorter_spouse.HSAvalueEOY[index];
    });

    var combinedPortfolio = longer_spouse.portfolioValueAtRetirement.map(function (value, index, arr) {
      //if someone dies add it to the mix
      if (shorter_spouse.portfolioValueAtRetirement[index]) {
        return value + shorter_spouse.portfolioValueAtRetirement[index];
      } else {
        return value;
      }
    });

    //RESOLVES BUG FOR SMALL HSA (LESS THAN 1%) IN FIRST YEAR
    //Checks if the starting HSAvalueEOY is less than 0 then combines the small primary + spouse for year 1
    //When the combinedHSAValueEOY is greater than 0 in the first year then we calculate in the for loop below
    if (combinedHSAValueEOY[0] < 0) {
      var combinedHsa;
      //Combine the primary and spouse hSA
      primary.graph1.covered_By_Hsa.map(function (el, i) {
        //Set value to 0 if NaN, Undefined, Null, etc
        el = el || 0;
        spouse.graph1.covered_By_Hsa[i] = spouse.graph1.covered_By_Hsa[i] || 0;
        //Combine the primary and spouse amounts
        combinedHsa = el + spouse.graph1.covered_By_Hsa[i];
        //push the combined values to covered by HSA for graph
        covered_By_Hsa.push(combinedHsa);
      });
    }

    for (var y = Math.max(primaryData.retirementAge, primaryData.currentAge); y <= longestLived; y++) {
      //Fully OOP total start
      if (combinedHSAValueEOY[indexVal] > 0) {
        combinedOOPVal = 0;
      } else if (combinedHSAValueEOY[indexVal] <= 0 && combinedHSAValueEOY[indexVal - 1] && combinedHSAValueEOY[indexVal - 1] > 0) {
        combinedOOPVal = (-combinedHSAValueEOY[indexVal]);
      } else {
        combinedOOPVal = combinedCosts[indexVal];
      }
      combinedOOPTotal.push(combinedOOPVal);

      //-------- Fully OOP years;
      if (Math.round(combinedCosts[0] === Math.round(combinedOOPVal))) {
        combinedOOPyearVal++;
      }
      combinedOOPYears.push(combinedOOPyearVal);

      //Handles the amount covered by HSA when value in first year is greater than 0. We only overwrite the value if the covered_by_Hsa array index is not a number
      if (combinedHSAValueEOY[indexVal] > 0) {
        combinedHSAval = combinedCosts[indexVal];
      }
      // Commenting out because this leads to wonky scenario when primary dies
      // } else if (Math.abs(combinedHSAValueEOY[indexVal]) === combinedOOPVal) {
      //   combinedHSAval = (combinedCosts[indexVal] - combinedOOPVal);
      // }
      else {
        combinedHSAval = 0;
      }
      //Only update the array if it's undefined or null i.e. isn't a number. Basically check that something isn't there already.
      if (typeof covered_By_Hsa[indexVal] !== "number") {
        covered_By_Hsa[indexVal] = combinedHSAval;
      }
      //OOP start
      //OOP.push(combinedCosts[indexVal] - combinedHSAval);
      indexVal++;
      combinedAges.push(y);
    }

    //NEW OUT OF POCKET CALCULATIONS THAT HANDLES BOTH SMALL HSA and LARGE HSA AMOUNTS
    //Takes combinedCosts then subtracts the HSA amount per year
    combinedCosts.forEach(function (el, i) {
      //Set value to 0 if NaN, Undefined, Null, etc
      el = el || 0;
      covered_By_Hsa[i] = covered_By_Hsa[i] || 0;
      //get the OOP result
      var result = el - covered_By_Hsa[i];
      //Figure out what's covered by HSA vs what's OOP
      if (covered_By_Hsa[i] > el) {
        //Fully covered by HSA so no OOP
        OOP.push(0);
      } else {
        //Have to pay something OOP
        OOP.push(result);
      }
    });

    var portfolioRatio = covered_By_Hsa.map(function (value, index, arr) {
      return (value + OOP[index]) / (combinedPortfolio[index] * 0.05);
    });

    var adjustedCombinedHcCosts = combinedCosts.slice(0, combinedAges.length);
    var adjustedLiftetimeHcCosts = Math.round(adjustedCombinedHcCosts.reduce(function (a, b) { return a + b; }));

    //Add the lifetime healthcare costs
    lifetimeHcCosts = Math.round(combinedCosts.reduce(function (a, b) { return a + b; }));
    //Add the total HSA savings
    lifetimeHsa = Math.round(covered_By_Hsa.reduce(function (a, b) { return a + b; }));
    //Get the expected lifetime total
    //lifetimeOop = Math.round(OOP.reduce(function (a, b) { return a + b; }));
    lifetimeOop = lifetimeHcCosts - lifetimeHsa;
    //Get the percent covered by HSA
    lifetimePercentCovered = Math.round((lifetimeHsa / lifetimeHcCosts) * 100);
    //Do they have an HSA?
    hasHsaFlag = lifetimeHsa === 0 ? false : true;
    return {
      "graph1": {
        "covered_By_Hsa": covered_By_Hsa,
        "OOP": OOP,
        "ageData": combinedAges
      },
      "graph2": {
        "hcPortfolioRatio": portfolioRatio
      },
      "combinedCosts": combinedCosts,
      "combinedHSAValueEOY": combinedHSAValueEOY,
      "combinedPortfolio": combinedPortfolio,
      "combinedOOPYears": combinedOOPYears,
      "combinedOOPTotal": combinedOOPTotal,
      "lifetimeHcCosts": lifetimeHcCosts,
      "lifetimeHsa": lifetimeHsa,
      "lifetimeOop": lifetimeOop,
      "lifetimePercentCovered": lifetimePercentCovered,
      "hasHsaFlag": hasHsaFlag,
      "adjustedCombinedHcCosts": adjustedCombinedHcCosts,
      "adjustedLiftetimeHcCosts": adjustedLiftetimeHcCosts
    };
  }

  //Calculates raw data from inputs
  function graphCalculations(data) {
    //edge cases
    if (data.currentAge == 0 && data.retirementAge == 0 && lifeExpectancy == 0) {
      return {
        "graph1": {
          "covered_By_Hsa": [],
          "OOP": [],
        },
        "graph2": {
          "hcPortfolioRatio": []
        },
        "costs": [],
        "HSAvalueEOY": [],
        "OOPTotal": [],
        "fullyOOP": [],
        "portfolioValueAtRetirement": [],
        "lifetimeHcCosts": 0,
        "lifetimeHsa": 0,
        "lifetimeOop": 0,
        "lifetimePercentCovered": 0,
      };

    }
    var graph1A_coveredByHSA = [];
    var graph1B_OOP = [];
    var graph2 = [];

    var healthCareCosts = [];
    var HSAvalueEOY = [];
    var OOPTotal = [];
    var portfolioValueAtRetirement = [];
    var ageDataRetirementToLifeExpectancy = [];
    var fullyOutOfPocket = 0;
    var fullyOutOfPocketArr = [];
    var yearOverYearInflation = Math.pow((1 + healthCareInflation), new Date().getFullYear() - 2016).toFixed(6);
    var HSAcurrentValueEOY;
    var oopSpend;
    var cost;
    var costInflationRate;
    var currPortfolioVal;
    var currHSAcoverageVal;
    var oopCurrVal;
    var actualOopVal;
    var graph2currVal;
    // Desplitting out dataobject
    var retirementAge = data.retirementAge;
    var lifeExpectancy = data.lifeExpectancy;
    var currentAge = data.currentAge;
    var gender = data.gender;

    var currentHealthSavings = data.currentHealthSavings;
    var currentInvestibleAssets = data.currentInvestibleAssets;
    var additionsTillRetirement = data.additionsTillRetirement;
    var HSAadditionsTillRetirement = data.HSAadditionsTillRetirement;
    var secondaryAge = calculateSecondaryAge(hasSpouse, primaryData.currentAge, primaryData.retirementAge, spouseData.currentAge);
    var data_age;
    var lifetimeHcCosts;
    var lifetimeHsa;
    var lifetimePercentCovered;
    var lifetimeOop;
    var hasHsaFlag;
    gender = genderMap[gender];

    //Making sure retirement accounts for if current age is higher than retirement age
    if (!data.is_spouse) {
      retirementAge = currentAge >= retirementAge ? currentAge : retirementAge;
    }

    // life expectancy should now equal max secondary age
    var primaryStartAge = Math.max(primaryData.retirementAge, primaryData.currentAge);
    lifeExpectancy = data.is_spouse ? Math.max(secondaryAge, lifeExpectancy) : lifeExpectancy;
    if (data.is_spouse) {
      lifeExpectancy = spouseData.lifeExpectancy + (primaryStartAge - spouseData.currentAge);
    }
    for (var i = primaryStartAge; i <= lifeExpectancy; i++) {
      // Using secondary age if calculating spouse
      data_age = data.is_spouse ? secondaryAge : i;
      //1) Build cost based on values
      costInflationRate = Math.pow((1 + healthCareInflation), (data_age - currentAge));
      if (data_age < 65) {
        oopSpend = ageGenderCostTable[gender][0];
      } else if (data_age < 75) {
        oopSpend = ageGenderCostTable[gender][1];
      } else if (data_age < 85) {
        oopSpend = ageGenderCostTable[gender][2];
      } else {
        oopSpend = ageGenderCostTable[gender][3];
      }
      //------------- COST RELATED FUNCTIONS ----------
      oopSpend = oopSpend.toFixed(6);
      // getting 2016 base values to 2020 standards
      cost = oopSpend * yearOverYearInflation;
      // multiplying by actual year over year rate
      cost *= costInflationRate;
      // filling cost array
      healthCareCosts.push(cost);
      //-------------End cost related functions -------------
      //---------- HSA value EOY Start---------------------------
      //first value formula
      //Bugs: Off by 1 cent in HSAvalueEOY

      if (i === primaryStartAge && data_age > currentAge) {
        HSAcurrentValueEOY = ((currentHealthSavings * (Math.pow((1 + HSAreturns), data_age - currentAge))) + FV(HSAreturns, primaryStartAge - currentAge, -HSAadditionsTillRetirement, 0) - cost).toFixed(2);
      } else if (i === primaryStartAge && data_age <= currentAge) {
        HSAcurrentValueEOY = ((currentHealthSavings * (Math.pow((1 + HSAreturns), data_age - currentAge))) - cost);
      } else {
        HSAcurrentValueEOY = (HSAvalueEOY[HSAvalueEOY.length - 1] * (1 + HSAreturns)) - cost;
      }
      HSAcurrentValueEOY = Number(HSAcurrentValueEOY);
      HSAvalueEOY.push(Number(HSAcurrentValueEOY.toFixed(2)));

      //---------- HSA value EOY End ---------------------------
      //----------OOP total exluding HSA start/ Out of pocket years -----------------
      if (HSAcurrentValueEOY > 0) {
        oopCurrVal = 0;
      } else if (HSAcurrentValueEOY <= 0 && (OOPTotal[OOPTotal.length - 1] === 0) || OOPTotal.length === 0) {
        oopCurrVal = Number(-HSAcurrentValueEOY.toFixed(2));
      } else {
        oopCurrVal = Number(cost.toFixed(2));
      }

      OOPTotal.push(oopCurrVal);
      //----------OOP total exluding HSA End ------------------
      //-----------Fully oop start
      if (Math.round(cost) === Math.round(oopCurrVal)) {
        fullyOutOfPocket++;
      }
      fullyOutOfPocketArr.push(fullyOutOfPocket);

      //-----------Fully oop End
      //------------ Portfolio Value in Retirement EOY-----

      if (data_age > currentAge && !portfolioValueAtRetirement.length) {
        var dynamicAge = data.is_spouse ? data_age : retirementAge;
        currPortfolioVal = ((currentInvestibleAssets * Math.pow(1 + portfolioReturns, dynamicAge - currentAge)) + Number(FV(portfolioReturns, dynamicAge - currentAge, -additionsTillRetirement, 0))) * 0.95;
      } else if (data_age <= currentAge && !portfolioValueAtRetirement.length) {
        currPortfolioVal = currentInvestibleAssets * 0.95;
      } else {
        currPortfolioVal = (portfolioValueAtRetirement[portfolioValueAtRetirement.length - 1] * 0.95) * (1 + portfolioReturns);
      }
      portfolioValueAtRetirement.push(currPortfolioVal);
      //------------ Portfolio Value in Retirement EOY End-----
      //-------------------Graph1A calculations


      if (HSAcurrentValueEOY > 0) {
        currHSAcoverageVal = cost;
      } else if (Math.abs(HSAcurrentValueEOY).toFixed(2) == oopCurrVal) {
        currHSAcoverageVal = cost - oopCurrVal;
      } else {
        currHSAcoverageVal = 0;
      }
      graph1A_coveredByHSA.push(currHSAcoverageVal);
      //-------------------Graph1A calculationsEnd
      //-----------Graph1B start
      actualOopVal = cost - currHSAcoverageVal;
      graph1B_OOP.push(actualOopVal);
      //-----------Graph1B End
      //---------Graph1b Start
      graph2currVal = (actualOopVal + currHSAcoverageVal) / (currPortfolioVal * 0.05);
      graph2.push(graph2currVal);
      //---------Graph1b End
      ageDataRetirementToLifeExpectancy.push(i);
      secondaryAge++;
      //Add the lifetime healthcare costs over time
      lifetimeHcCosts = Math.round(healthCareCosts.reduce(function (a, b) { return a + b; }));
      //Add the total HSA savings
      lifetimeHsa = Math.round(graph1A_coveredByHSA.reduce(function (a, b) { return a + b; }));
      //Get the expected lifetime total
      lifetimeOop = Math.round(graph1B_OOP.reduce(function (a, b) { return a + b; }));
      //Get the percent covered by HSA
      lifetimePercentCovered = Math.round((lifetimeHsa / lifetimeHcCosts) * 100);
      //Do they have an HSA?
      hasHsaFlag = lifetimeHsa === 0 ? false : true;
    }
    //Remove unneeded last fully out of pocket
    //fullyOutOfPocketArr.pop();

    return {
      "graph1": {
        "ageData": ageDataRetirementToLifeExpectancy,
        "covered_By_Hsa": graph1A_coveredByHSA,
        "OOP": graph1B_OOP,
      },
      "graph2": {
        "hcPortfolioRatio": graph2
      },
      "costs": healthCareCosts,
      "HSAvalueEOY": HSAvalueEOY,
      "OOPTotal": OOPTotal,
      "fullyOOP": fullyOutOfPocketArr,
      "portfolioValueAtRetirement": portfolioValueAtRetirement,
      "spouseData": data,
      "lifetimeHcCosts": lifetimeHcCosts,
      "lifetimeHsa": lifetimeHsa,
      "lifetimeOop": lifetimeOop,
      "lifetimePercentCovered": lifetimePercentCovered,
      "hasHsaFlag": hasHsaFlag
    };
  }
  // Calculating primary and graph data
  //run break down here

  var assumptions = getAssumption();
  var primary_graphData = graphCalculations(primaryData);
  console.log("primary", primary_graphData);
  //Add primary data to global var
  window.fiCalc.primary = primary_graphData;
  var spouse_graphData = hasSpouse ? graphCalculations(spouseData) : undefined;
  //combining primary and spouse data
  if (hasSpouse) {
    console.log("spouse", spouse_graphData);
    //Add spouse data to global var
    window.fiCalc.spouse = spouse_graphData;
    primary_graphData = combineGraphData(primary_graphData, spouse_graphData);
    console.log('Combined', primary_graphData);
    //Add combined data to global var
    window.fiCalc.combined = primary_graphData;
  }
  // set age array to zero-based index
  var baseAge;
  var ageArr = primary_graphData.graph1.ageData;
  primary_graphData.graph1.ageData = ageArr.map(function (value, index, arr) {
    if (index == 0) baseAge = value;
    return value - baseAge;
  });

  // format data
  // round to 2 decimal places
  var coveredByHsa = primary_graphData.graph1.covered_By_Hsa;
  primary_graphData.graph1.covered_By_Hsa = fixArrayValues(coveredByHsa, 0);
  var OOP = primary_graphData.graph1.OOP;
  primary_graphData.graph1.OOP = fixArrayValues(OOP, 0);
  var hcPercent = primary_graphData.graph2.hcPortfolioRatio;
  hcPercent = fixArrayValues(hcPercent, 2).map(function (val, index) {
    return Number(Math.round(val + 'e' + 2)) + '%';
  });
  primary_graphData.graph2.hcPortfolioRatio = hcPercent;
  //breakdown call here
  var primarySpouseBreakdown = calculateBreakDownCosts(primaryData.gender, primaryData.retirementAge, primaryData.currentAge, assumptions.healthcareCostInflationRate);
  var secondarySpouseBreakdown = hasSpouse ? calculateBreakDownCosts(spouseData.gender, spouseData.retirementAge, spouseData.currentAge, assumptions.healthcareCostInflationRate) : undefined;

  primary_graphData.primaryCostBreakdown = primarySpouseBreakdown;
  primary_graphData.spouseCostBreakDown = secondarySpouseBreakdown;

  //Add all data to global var
  window.fiCalc.all = primary_graphData;
  //Question: What is this returning? It looks identical to combined.
  return primary_graphData;
}